/**
 * The ReplayLoader class definition.
 *
 * The ReplayLoader provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ReplayLoader');
goog.provide('JaSMIn.ReplayLoaderEvents');

goog.require('JaSMIn');
goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.EventDispatcher');
goog.require('JaSMIn.GameStateChange');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.ReplayDataIterator');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');



/**
 * @enum {!string}
 */
JaSMIn.ReplayLoaderEvents = {
  START: 'start',
  NEW_REPLAY: 'newReplay',
  PROGRESS: 'progress',
  FINISHED: 'finished',
  ERROR: 'error'
};






/**
 * [ReplayLoader description]
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.ReplayLoader = function() {

  /**
   * The replay data iterator instance
   * @type {?JaSMIn.ReplayDataIterator}
   */
  this.iterator = null;

  /**
   * The currently parsed replay instance
   * @type {?JaSMIn.Replay}
   */
  this.replay = null;

  /**
   * The currently loaded replay url
   * @type {?string}
   */
  this.replayURL = null;

  /**
   * The XMLHttpRequest object used to load the replay file
   * @type {?XMLHttpRequest}
   */
  this.xhr = null;



  /** @type {!Function} */
  this.xhrOnLoadListener = this.xhrOnLoad.bind(this);
  /** @type {!Function} */
  this.xhrOnProgressListener = this.xhrOnProgress.bind(this);
  /** @type {!Function} */
  this.xhrOnErrorListener = this.xhrOnError.bind(this);
};



/**
 * [load description]
 *
 * @param  {!string} replayURL the URL to the replay file
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.load = function(replayURL) {
  // Clear loader instance
  this.clear();

  this.replayURL = replayURL;

  // Publish start event
  this.dispatchEvent({
    type: JaSMIn.ReplayLoaderEvents.START,
    url: replayURL
  });

  // Create Request
  this.xhr = new XMLHttpRequest();
  this.xhr.open('GET', replayURL, true);

  // Add event listeners
  this.xhr.addEventListener('load', this.xhrOnLoadListener, false);
  this.xhr.addEventListener('progress', this.xhrOnProgressListener, false);
  this.xhr.addEventListener('error', this.xhrOnErrorListener, false);

  // Set mime type
  if (this.xhr.overrideMimeType) {
    this.xhr.overrideMimeType('text/plain');
  }

  // Send request
  this.xhr.send(null);
};



/**
 * Clear the loader instance.
 *
 * @param {!boolean=} keepIteratorAlive indicator if iterator should not be disposed
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.clear = function(keepIteratorAlive) {
  if (this.xhr !== null) {
    // Remove event listeners
    this.xhr.removeEventListener('load', this.xhrOnLoadListener);
    this.xhr.removeEventListener('progress', this.xhrOnProgressListener);
    this.xhr.removeEventListener('error', this.xhrOnErrorListener);

    // Abort active request
    this.xhr.abort();
    this.xhr = null;
  }

  if (!keepIteratorAlive && this.iterator !== null) {
    this.iterator.dispose();
  }

  this.iterator = null;
  this.replay = null;
  this.replayURL = null;
};



/**
 * The XHR onLoad callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.xhrOnLoad = function(event) {
  var success = false;

  if (event.target.status === 200 || event.target.status === 0) {
    // Successfully loaded
    success = true;

    // Parse remaining response
    this.parse(event.target.response, true);

    this.dispatchEvent({
        type: JaSMIn.ReplayLoaderEvents.FINISHED
      });
  } else {
    // Error during loading
    this.dispatchEvent({
        type: JaSMIn.ReplayLoaderEvents.ERROR,
        msg: event.target.statusText
      });
  }

  // Clear loader instance
  this.clear(success);
};



/**
 * The XHR onProgress callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.xhrOnProgress = function(event) {
  this.parse(event.target.response);

  this.dispatchEvent({
      type: JaSMIn.ReplayLoaderEvents.PROGRESS,
      total: event.total,
      loaded: event.loaded
    });
};



/**
 * The XHR onError callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.xhrOnError = function(event) {
  this.dispatchEvent({
      type: JaSMIn.ReplayLoaderEvents.ERROR,
      msg: event.target.statusText
    });

  // Clear loader instance
  this.clear();
};



/**
 * Try parsing a replay or continue parsing a replay.
 *
 * @param  {?string=} data the current data
 * @param  {!boolean=} fullyLoaded indicator if the replay file is fully loaded
 * @return {void}
 */
JaSMIn.ReplayLoader.prototype.parse = function(data, fullyLoaded) {
  if (!data) {
    // Nothing to parse
    return;
  }

  if (fullyLoaded === undefined) {
    fullyLoaded = false;
  }

  if (this.replay === null) {
    if (data.split('\n').length > 10) {
      // Start parsing after the first 10 lines of the replay file arrived
      this.iterator = new JaSMIn.ReplayDataIterator(data, !fullyLoaded);

      try {
        this.replay = JaSMIn.ReplayParser.parseReplay(this.iterator);
        this.replay.url = this.replayURL;
      } catch (ex) {
        this.dispatchEvent({
            type: JaSMIn.ReplayLoaderEvents.ERROR,
            msg: ex.message
          });

        // Clear loader instance
        this.clear();
        return;
      }

      this.dispatchEvent({
          type: JaSMIn.ReplayLoaderEvents.NEW_REPLAY,
          replay: this.replay
        });
    }
  } else {
    // Update iterator data
    this.iterator.data = data;
    this.iterator.fullyLoaded = fullyLoaded;

    // Check if we need to restart parsing
    if (this.iterator.line === null) {
      JaSMIn.ReplayParser.parseReplayBody(this.iterator, this.replay, this.replay.type == JaSMIn.ReplayTypes.TWOD ? 100 : 50);
    }
  }
};




// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.ReplayLoader.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.ReplayLoader.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.ReplayLoader.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
