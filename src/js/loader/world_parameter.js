goog.provide('JaSMIn.WorldParameter');


goog.require('JaSMIn');




/**
 * [WorldParameter description]
 *
 * @constructor
 * @struct
 * @param {!number} lineWidth the width of field lines
 * @param {!number} ballRadius the radius of the ball
 * @param {!THREE.Vector2} fieldDimensions the field dimensions
 * @param {!number} centerRadius the radius of the center cycle
 * @param {!THREE.Vector3} goalDimensions the dimensions of the goals
 * @param {!THREE.Vector2} goalAreaDimensions the dimensions of the goal area
 * @param {?THREE.Vector3} penaltyAreaDimensions the dimensions of the penalty area + penalty kick spot
 * @param {?number} fieldTextureRepeat the number of bright and dark green stripes per field half (null for 1m stipes)
 * @param {!number=} leftGoalColor the color value of the left goal
 * @param {!number=} rightGoalColor the color value of the right goal
 */
JaSMIn.WorldParameter = function(lineWidth,
                                 ballRadius,
                                 fieldDimensions,
                                 centerRadius,
                                 goalDimensions,
                                 goalAreaDimensions,
                                 penaltyAreaDimensions,
                                 fieldTextureRepeat,
                                 leftGoalColor,
                                 rightGoalColor) {
  /**
   * The width of field lines
   * @type {!number}
   */
  this.lineWidth = lineWidth;

  /**
   * The ball radius
   * @type {!number}
   */
  this.ballRadius = ballRadius;

  /**
   * The field dimensions
   * @type {!THREE.Vector2}
   */
  this.fieldDimensions = fieldDimensions;

  /**
   * The radius of the center circle
   * @type {!number}
   */
  this.centerRadius = centerRadius;

  /**
   * The Goal dimensions
   * @type {!THREE.Vector3}
   */
  this.goalDimensions = goalDimensions;

  /**
   * The goal area dimensions
   * @type {!THREE.Vector2}
   */
  this.goalAreaDimensions = goalAreaDimensions;

  /**
   * The penalty area dimensions + penalty kick spot
   * @type {?THREE.Vector3}
   */
  this.penaltyAreaDimensions = penaltyAreaDimensions;

  /**
   * The number of bright and dark green stripes on each field half
   * @type {?number}
   */
  this.fieldTextureRepeat = fieldTextureRepeat;

  /**
   * The color number for the left goal
   * @type {!number}
   */
  this.leftGoalColor = leftGoalColor !== undefined ? leftGoalColor : 0xcccc00;

  /**
   * The color number for the right goal
   * @type {!number}
   */
  this.rightGoalColor = rightGoalColor !== undefined ? rightGoalColor : 0x0088bb;
};



/**
 * Check if this world parameters define a penalty area.
 *
 * @return {!boolean} true, if there exists a definition for the penalty area, false otherwise
 */
JaSMIn.WorldParameter.prototype.hasPenaltyArea = function() {
  return this.penaltyAreaDimensions !== null;
};



/**
 * Check if this world parameters define a field texture repeat value.
 *
 * @return {!boolean} true, if there exists a definition for the field texture repeat, false otherwise
 */
JaSMIn.WorldParameter.prototype.hasFieldTextureRepeat = function() {
  return this.fieldTextureRepeat !== null;
};


/**
 * Fetch the world parameter set for the given type and version.
 *
 * @param  {!number} type the replay type (2D or 3D)
 * @param  {!number} version the field version
 * @return {!JaSMIn.WorldParameter} the parameter set for the given type and version
 */
JaSMIn.WorldParameter.createParameters = function(type, version) {
  // For 2D, we only know one world
  if (type === JaSMIn.ReplayTypes.TWOD) {
    return JaSMIn.WorldParameter.create2DParameters(version);
  } else {
    return JaSMIn.WorldParameter.create3DParameters(version);
  }
};


/**
 * Fetch the 2D world parameter set for the given version.
 *
 * @param  {!number} version the field version
 * @return {!JaSMIn.WorldParameter} the parameter set for the given type and version
 */
JaSMIn.WorldParameter.create2DParameters = function(version) {
  return new JaSMIn.WorldParameter(
      0.15,                               // field line
      0.2,                                // ball radius
      new THREE.Vector2(105, 68),         // field dim
      9.15,                               // center radius
      new THREE.Vector3(1.2, 14.64, 1.5), // goal dim
      new THREE.Vector2(5.5, 18.32),      // goal area dim
      new THREE.Vector3(16.5, 40.3, 11),  // penalty area dim + kick spot
      10                                  // field texture repeats
    );
};


/**
 * Fetch the 3D world parameter set for the given version.
 *
 * @param  {!number} version the field version
 * @return {!JaSMIn.WorldParameter} the parameter set for the given type and version
 */
JaSMIn.WorldParameter.create3DParameters = function(version) {
  switch (version) {
    case 62:
      return new JaSMIn.WorldParameter(
          0.03,                             // field line
          0.042,                            // ball radius
          new THREE.Vector2(12, 8),         // field dim
          1,                                // center radius
          new THREE.Vector3(0.4, 1.4, 0.8), // goal dim
          new THREE.Vector2(1.2, 4),        // goal area dim
          null,                             // penalty area dim + kick spot
          null                              // field texture repeats
        );
    case 63:
      return new JaSMIn.WorldParameter(
          0.04,                             // field line
          0.042,                            // ball radius
          new THREE.Vector2(18, 12),        // field dim
          1.8,                              // center radius
          new THREE.Vector3(0.6, 2.1, 0.8), // goal dim
          new THREE.Vector2(1.8, 6),        // goal area dim
          null,                             // penalty area dim + kick spot
          null                              // field texture repeats
        );
    case 64:
      return new JaSMIn.WorldParameter(
          0.04,                             // field line
          0.042,                            // ball radius
          new THREE.Vector2(21, 14),        // field dim
          1.8,                              // center radius
          new THREE.Vector3(0.6, 2.1, 0.8), // goal dim
          new THREE.Vector2(1.8, 6),        // goal area dim
          null,                             // penalty area dim + kick spot
          null                              // field texture repeats
        );
    case 66:
    default:
      return new JaSMIn.WorldParameter(
          0.04,                             // field line
          0.042,                            // ball radius
          new THREE.Vector2(30, 20),        // field dim
          2,                                // center radius
          new THREE.Vector3(0.6, 2.1, 0.8), // goal dim
          new THREE.Vector2(1.8, 6),        // goal area dim
          null,                             // penalty area dim + kick spot
          null                              // field texture repeats
        );
  }
};
