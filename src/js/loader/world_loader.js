/**
 * The WorldLoader class definition.
 *
 * The WorldLoader provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.WorldLoader');

goog.require('JaSMIn');
goog.require('JaSMIn.Agent');
goog.require('JaSMIn.Ball');
goog.require('JaSMIn.Field');
goog.require('JaSMIn.NaoSpecification');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.RobotModelFactory');
goog.require('JaSMIn.SceneUtils');
goog.require('JaSMIn.Team');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.World');
goog.require('JaSMIn.WorldParameter');





/**
 * WorldLoader Constructor
 *
 * @constructor
 */
JaSMIn.WorldLoader = function() {

  /**
   * The robot model factory.
   * @type {!JaSMIn.RobotModelFactory}
   */
  this.modelFactory = new JaSMIn.RobotModelFactory();
};



/**
 * Create a world representation for the given replay.
 *
 * @param  {!JaSMIn.Replay} replay the replay
 * @return {!JaSMIn.World} the world representation
 */
JaSMIn.WorldLoader.prototype.loadWorldFor = function(replay) {
  return this.loadWorld(replay.type, replay.fieldVersion, replay.leftTeam, replay.rightTeam);
};




/**
 * Create a world representation.
 *
 * @param  {!number} type the world (replay) type (2D or 3D)
 * @param  {!number} version the field version
 * @param  {!JaSMIn.TeamDescription} leftTeamDescription the left team description
 * @param  {!JaSMIn.TeamDescription} rightTeamDescription the right team description
 * @return {!JaSMIn.World} the world representation
 */
JaSMIn.WorldLoader.prototype.loadWorld = function(type, version, leftTeamDescription, rightTeamDescription) {
  // Fetch corresponding world parameter set
  var worldParams = JaSMIn.WorldParameter.createParameters(type, version);

  // Create field, ball, left- and right team
  var field = this.loadField(worldParams);
  var ball = this.loadBall(worldParams.ballRadius);
  var leftTeam = this.loadTeam(type, new JaSMIn.Team(leftTeamDescription));
  var rightTeam = this.loadTeam(type, new JaSMIn.Team(rightTeamDescription));

  // Create world representation
  var world = new JaSMIn.World(field, ball, leftTeam, rightTeam, 512);

  // Create general world objects (sky-box, lights, etc.)
  world.scene.add(JaSMIn.SceneUtils.createSkyBox(1024));
  JaSMIn.SceneUtils.addStdLighting(world.scene, worldParams.fieldDimensions.x, worldParams.fieldDimensions.y);

  return world;
};



/**
 * Create a field representation.
 *
 * @param  {!JaSMIn.WorldParameter} worldParams the world parameters
 * @return {!JaSMIn.Field} the field representation
 */
JaSMIn.WorldLoader.prototype.loadField = function(worldParams) {
  var field = new JaSMIn.Field(worldParams.fieldDimensions,
                               worldParams.centerRadius,
                               worldParams.goalDimensions,
                               worldParams.goalAreaDimensions,
                               worldParams.penaltyAreaDimensions);

  var fieldLength = worldParams.fieldDimensions.x;
  var fieldWidth = worldParams.fieldDimensions.y;


  // Create field plane
  JaSMIn.SceneUtils.addFieldPlane(field.objGroup,
                                  fieldLength,
                                  fieldWidth,
                                  worldParams.fieldTextureRepeat);


  // Create field lines
  JaSMIn.SceneUtils.addFieldLines(field.objGroup,
                                  worldParams.lineWidth,
                                  worldParams.fieldDimensions,
                                  worldParams.centerRadius,
                                  worldParams.goalAreaDimensions,
                                  worldParams.penaltyAreaDimensions);


  // Create left goal
  var goalGroup = JaSMIn.SceneUtils.createHockeyGoal('leftGoal',
                                                     worldParams.lineWidth,
                                                     worldParams.goalDimensions,
                                                     worldParams.leftGoalColor);
  goalGroup.position.x = -fieldLength / 2;
  goalGroup.rotation.y = Math.PI;
  field.objGroup.add(goalGroup);


  // Create right goal
  goalGroup = JaSMIn.SceneUtils.createHockeyGoal('rightGoal',
                                                 worldParams.lineWidth,
                                                 worldParams.goalDimensions,
                                                 worldParams.rightGoalColor);
  goalGroup.position.x = fieldLength / 2;
  field.objGroup.add(goalGroup);

  return field;
};



/**
 * Create a ball representation.
 *
 * @param  {!number} ballRadius the radius of the ball
 * @return {!JaSMIn.Ball} the ball representation
 */
JaSMIn.WorldLoader.prototype.loadBall = function(ballRadius) {
  var ball = new JaSMIn.Ball(ballRadius);

  // Create simple ball placeholder
  var placeholder = JaSMIn.SceneUtils.createSimpleBall(ballRadius);
  ball.objGroup.add(placeholder);

  // Load nice looking ball object
  JaSMIn.SceneUtils.loadObject('soccer_ball.json',
    /**
     * @param  {!THREE.Scene} scene the loaded scene
     * @return {void}
     */
    function(scene) { // onLoad
      var geometry = new THREE.BufferGeometry();
      geometry.fromGeometry(/**@type {!THREE.Geometry}*/(/**@type {!THREE.Mesh}*/(scene.getObjectByName('soccerball')).geometry));
      geometry.name = 'ballGeo';

      var material = JaSMIn.SceneUtils.createStdPhongMat('ballMat', 0xffffff, 'rcs-soccerball.png');

      var mesh = new THREE.Mesh(geometry, material);
      mesh.name = 'ballSphere';
      mesh.castShadow = true;
      mesh.receiveShadow = true;
      mesh.scale.setScalar(ball.radius);

      // Exchange placeholder with nice looking ball mesh
      ball.objGroup.remove(placeholder);
      ball.objGroup.add(mesh);
    });

  return ball;
};



/**
 * Load a team representation.
 * This method can be called repeatedly to load additional agents to a team,
 * as well as additional robot models to all agents of the team (this comes handy for streaming).
 *
 * @param  {!number} type the world (replay) type (2D or 3D)
 * @param  {!JaSMIn.Team} team the team representation
 * @return {!JaSMIn.Team} the given team
 */
JaSMIn.WorldLoader.prototype.loadTeam = function(type, team) {
  var teamDescription = team.description;

  for (var i = 0; i < teamDescription.agents.length; ++i) {
    var agent = team.agents[i];

    // Create agent representation if not yet present
    if (agent === undefined) {
      agent = new JaSMIn.Agent(teamDescription.agents[i]);
      team.agents[i] = agent;
      team.objGroup.add(agent.objGroup);
    }

    // Create robot models for agent if not yet present
    for (var j = 0; j < agent.description.models.length; ++j) {
      if (agent.models[j] === undefined) {
        agent.models[j] = this.modelFactory.createModel(type, agent.description.models[j], agent.description.playerNo);
        agent.models[j].setTeamColor(team.color);
        agent.objGroup.add(agent.models[j].objGroup);
      }
    }
  }

  return team;
};
