/**
 * The ObjectState class definition.
 *
 * This basic ObjectState provides information about the object's position and orientation at a specific point in time.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ObjectState');



/**
 * ObjectState Constructor
 * Create a new ObjectState with the given position and orientation.
 *
 * @struct
 * @constructor
 * @export
 * @param {THREE.Vector3} position the position of the object
 * @param {THREE.Quaternion} quaternion the orientation of the object
 */
JaSMIn.ObjectState = function(position, quaternion) {
  /**
   * The object position.
   * @type {THREE.Vector3}
   */
  this.position = position;

  /**
   * The object orientation.
   * @type {THREE.Quaternion}
   */
  this.quaternion = quaternion;
};



/**
 * Checks ObjectState for validity.
 * @return {boolean} true if position and orientation are defined, false otherwise
 */
JaSMIn.ObjectState.prototype.isValid = function() {
  return this.position !== null && this.quaternion !== null;
};
