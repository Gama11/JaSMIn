/**
 * The PartialWorldState class definition.
 *
 * The PartialWorldState provides information about the state of the game, the ball and all agents on the field.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.PartialWorldState');

goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.WorldState');



/**
 * PartialWorldState Constructor
 * Create a new PartialWorldState holding the given information.
 *
 * @constructor
 * @struct
 * @param {!number} time the global time
 * @param {!number} gameTime the game time
 * @param {!string} gameState the game state
 * @param {!number} goalsLeft the number of goals for the left team
 * @param {!number} goalsRight the number of goals for the right team
 */
JaSMIn.PartialWorldState = function(time, gameTime, gameState, goalsLeft, goalsRight) {
  /**
   * The global time.
   * @type {!number}
   */
  this.time = time;

  /**
   * The game time.
   * @type {!number}
   */
  this.gameTime = gameTime;

  /**
   * The state of the game (Before-Kick-Off, Kick-Off-Left, etc.).
   * @type {!string}
   */
  this.gameState = gameState;

  /**
   * The left team score.
   * @type {!number}
   */
  this.scoreLeft = goalsLeft;

  /**
   * The right team score.
   * @type {!number}
   */
  this.scoreRight = goalsRight;

  /**
   * The state of the ball.
   * @type {?JaSMIn.ObjectState}
   */
  this.ballState = null;

  /**
   * The states of all left agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.leftAgentStates = [];

  /**
   * The states of all right agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.rightAgentStates = [];
};



/**
 * Check if this partial world state is a fully valid world state.
 *
 * @return {!boolean} true if the partial world state is complete, false otherwise
 */
JaSMIn.PartialWorldState.prototype.isValid = function() {
  return this.ballState !== null && (this.leftAgentStates.length + this.rightAgentStates.length) > 0;
};



/**
 * Reinitialize this partial state.
 *
 * @param {!number} gameTime the game time
 * @param {!string} gameState the game state
 * @param {!number} goalsLeft the number of goals for the left team
 * @param {!number} goalsRight the number of goals for the right team
 */
JaSMIn.PartialWorldState.prototype.reset = function(gameTime, gameState, goalsLeft, goalsRight) {
  this.gameTime = gameTime;
  this.gameState = gameState;
  this.scoreLeft = goalsLeft;
  this.scoreRight = goalsRight;
  this.ballState = null;
  this.leftAgentStates = [];
  this.rightAgentStates = [];
};



/**
 * Create a new world state instance from this partial world state if possible.
 *
 * @return {?JaSMIn.WorldState} a new world state instance
 */
JaSMIn.PartialWorldState.prototype.toWorldState = function() {
  if (this.ballState !== null && (this.leftAgentStates.length + this.rightAgentStates.length) > 0) {
    return new JaSMIn.WorldState(
      this.time,
      this.gameTime,
      this.gameState,
      this.scoreLeft,
      this.scoreRight,
      this.ballState,
      this.leftAgentStates,
      this.rightAgentStates
    );
  }

  return null;
};
