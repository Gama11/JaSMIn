/**
 * The AgentState class definition.
 *
 * The AgentState provides information about the state of an agent at a specific time.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.AgentState');






/**
 * AgentState Constructor
 * Create a new AgentState for the given number, position, orientation and data.
 *
 * @struct
 * @constructor
 * @extends {JaSMIn.ObjectState}
 * @export
 * @param {!number} number the player number
 * @param {!number} modelIdx the index of the currently used robot model
 * @param {THREE.Vector3} position the position of the agent
 * @param {THREE.Quaternion} quaternion the orientation of the agent
 * @param {!Array<number>} jointAngles array holding the joint angles
 * @param {!Array<number>} data dynamic data associated with the agent (stamina, fouls, etc.)
 */
JaSMIn.AgentState = function(number, modelIdx, position, quaternion, jointAngles, data) {
  goog.base(this, position, quaternion);

  /**
   * The agent player number.
   * @type {!number}
   */
  this.playerNo = number;

  /**
   * The index of the currently used robot model.
   * @type {!number}
   */
  this.modelIdx = modelIdx;

  /**
   * The joint angles of the robot model.
   * @type {!Array<number>}
   */
  this.jointAngles = jointAngles;

  /**
   * Dynamic agent data (stamina, fouls, etc.).
   * @type {!Array<number>}
   */
  this.data = data;
};
goog.inherits(JaSMIn.AgentState, JaSMIn.ObjectState);
