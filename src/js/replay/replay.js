/**
 * The Replay class definition.
 *
 * The Replay is the central class holding a replay file
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Replay');

goog.require('JaSMIn');
goog.require('JaSMIn.GameStateChange');
goog.require('JaSMIn.PartialWorldState');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');



/**
 * @enum {!string}
 */
JaSMIn.ReplayChangeEvents = {
  TEAMS: 'teams',
  STATES: 'states'
};






/**
 * Replay Constructor
 * Create a new replay.
 *
 * @constructor
 * @export
 * @param {!number} type the replay type (see JaSMIn.ReplayType enum)
 * @param {!number} version the replay version
 * @param {!number} frequency the state update frequency
 * @param {!number} fieldVersion the version of the field
 * @param {?string} fieldURL the URL to the world json file
 * @param {!JaSMIn.TeamDescription} leftTeam the description of the left team
 * @param {!JaSMIn.TeamDescription} rightTeam the description of the right team
 * @param {!Array<!JaSMIn.WorldState>} states a list of world states
 * @param {!Array<!JaSMIn.GameStateChange>} gameStateChanges a list of game state changes
 */
JaSMIn.Replay = function(type, version, frequency, fieldVersion, fieldURL, leftTeam, rightTeam, states, gameStateChanges) {

  /**
   * The replay file url.
   * @type {?string}
   */
  this.url = null;

  /**
   * The replay type (see JaSMIn.ReplayType for more information).
   * @type {!number}
   */
  this.type = type;

  /**
   * The replay version.
   * @type {!number}
   */
  this.version = version;

  /**
   * The state update frequency of the replay.
   * @type {!number}
   */
  this.frequency = frequency;

  /**
   * The version of the field.
   * @type {!number}
   */
  this.fieldVersion = fieldVersion;

  /**
   * The URL to the world json file.
   * @type {?string}
   */
  this.fieldURL = fieldURL;

  /**
   * The description of the left team.
   * @type {!JaSMIn.TeamDescription}
   */
  this.leftTeam = leftTeam;

  /**
   * The description of the right team.
   * @type {!JaSMIn.TeamDescription}
   */
  this.rightTeam = rightTeam;

  /**
   * The list of all world states.
   * @type {!Array<!JaSMIn.WorldState>}
   */
  this.states = states;

  /**
   * A list of game state changes.
   * @type {!Array<!JaSMIn.GameStateChange>}
   */
  this.gameStateChanges = gameStateChanges;

  /**
   * The time value of the first state.
   * @type {!number}
   */
  this.startTime = 0;

  /**
   * The time value of the last state.
   * @type {!number}
   */
  this.endTime = 0;

  /**
   * The duration of the replay.
   * @type {!number}
   */
  this.duration = 0;

  /**
   * The partial state used during parsing.
   * @type {?JaSMIn.PartialWorldState}
   */
  this.partialState = null;

  /**
   * Indicator if the replay is fully loaded.
   * @type {!boolean}
   */
  this.fullyLoaded = false;

  /**
   * The callback function to call when the replay is refreshed
   * @type {!Function | undefined}
   */
  this.onChange = undefined;

  // Update times
  if (states.length > 0) {
    this.startTime = states[0].time;
    this.endTime = states[states.length - 1].time;
    this.duration = this.endTime - this.startTime;
  }
};



/**
 * Fetch the index of the world state that corresponds to the given time.
 *
 * @param  {!number} time the global time
 * @return {!number} the world state index corresponding to the specified time
 */
JaSMIn.Replay.prototype.getIndexForTime = function(time) {
  var idx = Math.floor(time * this.frequency);

  if (idx < 0) {
    return 0;
  } else if (idx >= this.states.length) {
    return this.states.length - 1;
  }

  return idx;
};



/**
 * Retrieve the world state for the given time.
 *
 * @param  {!number} time the global time
 * @return {JaSMIn.WorldState} the world state closest to the specified time
 */
JaSMIn.Replay.prototype.getStateForTime = function(time) {
  return this.states[this.getIndexForTime(time)];
};



/**
 * Called to indicate that the team descriptions of the replay were updated.
 *
 * @return {void}
 */
JaSMIn.Replay.prototype.onTeamsUpdated = function() {
  if (this.onChange !== undefined) {
    this.onChange(JaSMIn.ReplayChangeEvents.TEAMS);
  }
};



/**
 * Called to indicate that the replay data was changed/extended.
 *
 * @return {void}
 */
JaSMIn.Replay.prototype.onStatesUpdated = function() {
  // Update times
  if (this.states.length > 0) {
    this.startTime = this.states[0].time;
    this.endTime = this.states[this.states.length - 1].time;
    this.duration = this.endTime - this.startTime;
  }

  if (this.onChange !== undefined) {
    this.onChange(JaSMIn.ReplayChangeEvents.STATES);
  }
};



/**
 * Called to indicate that the replay file is fully loaded and parsed and no further states, etc. will be appended.
 *
 * @return {void}
 */
JaSMIn.Replay.prototype.finalize = function() {
  this.fullyLoaded = true;

  // Add final state if valid
  if (this.partialState !== null && this.partialState.isValid()) {
    var newState = this.partialState.toWorldState();
    if (newState !== null) {
      this.states.push(newState);
    }
  }
  this.partialState = null;

  // Refresh the replay information a last time and publish change to finished
  this.onStatesUpdated();

  // Clear onChange listener
  this.onChange = undefined;
};
