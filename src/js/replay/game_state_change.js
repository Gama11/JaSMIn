/**
 * The GameStateChange class definition.
 *
 * The GameStateChange class simply holds a
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.GameStateChange');



/**
 * GameStateChange Constructor
 * Create a new GameStateChange.
 *
 * @struct
 * @constructor
 * @export
 * @param {!number} time the global time
 * @param {!string} oldState the old game state
 * @param {!string} newState the new game state
 */
JaSMIn.GameStateChange = function(time, oldState, newState) {
  /**
   * The global time.
   * @type {!number}
   */
  this.time = time;

  /**
   * The old game state.
   * @type {!string}
   */
  this.oldState = oldState;

  /**
   * The new game state
   * @type {!string}
   */
  this.newState = newState;
};
