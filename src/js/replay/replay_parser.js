/**
 * The ReplayParser class definition.
 *
 * The ReplayParser provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ReplayParser');

goog.require('JaSMIn');
goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.GameStateChange');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.PartialWorldState');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.ReplayDataIterator');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');



/**
 * The Replay parser util.
 */
JaSMIn.ReplayParser = {

};


/**
 * Parse a replay file.
 * Be sure that the replay data already contains the full header information!
 *
 * @param  {!JaSMIn.ReplayDataIterator} iterator the replay data iterator
 * @return {JaSMIn.Replay} the converted replay file
 */
JaSMIn.ReplayParser.parseReplay = function(iterator) {
  var fallback = false;

  // ==================== Parse Replay Header ====================
  // -------------------- Version-line ----------
  var line = iterator.next();
  if (line === null) {
    throw 'Replay corrupt!';
  } else if (line[0] != 'V') {
    // No version line specified try fallback to initial 2D replay format
    if (line[0] == 'T' && line.length > 2) {
      // Looks like a old replay format, use fallback parser
      fallback = true;
      console.log('ReplayParser: Detected old 2D replay file format!');
    } else {
      throw 'Invalid replay version line!';
    }
  } else if (line.length < 4) {
    throw 'Invalid replay version line!';
  } else if (line[1] !== '2D' && line[1] !== '3D') {
    throw 'Invalid replay type (only 2D or 3D allowed)!';
  }

  var type = JaSMIn.ReplayTypes.TWOD;
  var version = 0;
  var frequency = 10;
  var fieldVersion = 0;
  var fieldURL = null;
  var leftTeam = null;
  var rightTeam = null;

  if (fallback) {
    // Fallback parser
    leftTeam = new JaSMIn.TeamDescription(line[1].slice(1, -1), new THREE.Color(0xffff00), JaSMIn.TeamSide.LEFT);
    rightTeam = new JaSMIn.TeamDescription(line[2].slice(1, -1), new THREE.Color(0xff0000), JaSMIn.TeamSide.RIGHT);
  } else {
    type = line[1] == '2D' ? JaSMIn.ReplayTypes.TWOD : JaSMIn.ReplayTypes.THREED;
    version = parseInt(line[2], 10);
    frequency = parseFloat(line[3]);

    // -------------------- Teams-line ----------
    line = iterator.next();
    if (line === null) {
      throw 'Replay corrupt!';
    } else if (line.length < 5 || line[0] != 'T') {
      throw 'Invalid teams line!';
    }

    leftTeam = new JaSMIn.TeamDescription(line[1].slice(1, -1), new THREE.Color(line[2]), JaSMIn.TeamSide.LEFT);
    rightTeam = new JaSMIn.TeamDescription(line[3].slice(1, -1), new THREE.Color(line[4]), JaSMIn.TeamSide.RIGHT);

    // -------------------- World-line ----------
    line = iterator.next();
    if (line === null) {
      throw 'Replay corrupt!';
    } else if (line.length < 2 || line[0] != 'F') {
      throw 'Invalid world line!';
    }

    fieldVersion = parseInt(line[1], 10);
    fieldURL = line.length > 1 ? line[2] : null;
  }

  var replay = new JaSMIn.Replay(type, version, frequency, fieldVersion, fieldURL, leftTeam, rightTeam, [], []);

  // Parse the first n world states
  iterator.next();
  var maxStates = type == JaSMIn.ReplayTypes.TWOD ? 100 : 50;
  JaSMIn.ReplayParser.parseReplayBody(iterator, replay, maxStates);

  return replay;
};


/**
 * [parseReplayBody description]
 *
 * @param  {!JaSMIn.ReplayDataIterator} iterator the replay data iterator
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!number} maxStates the maximum number of states to parse
 * @param  {!Array<number>=} leftIdxList the list of model indexes
 * @param  {!Array<number>=} rightIdxList the list of model indexes
 * @return {void}
 */
JaSMIn.ReplayParser.parseReplayBody = function(iterator, replay, maxStates, leftIdxList, rightIdxList) {
  // Extract model index lists if not defined
  var agents = null;
  var i = 0;

  if (leftIdxList === undefined) {
    leftIdxList = [];

    agents = replay.leftTeam.agents;
    i = agents.length;

    while (i--) {
      leftIdxList[agents[i].playerNo] = agents[i].recentModelIdx;
    }
  }

  if (rightIdxList === undefined) {
    rightIdxList = [];

    agents = replay.rightTeam.agents;
    i = agents.length;

    while (i--) {
      rightIdxList[agents[i].playerNo] = agents[i].recentModelIdx;
    }
  }

  // Create partial state if not yet present
  if (replay.partialState === null) {
    replay.partialState = new JaSMIn.PartialWorldState(0, 0, '', 0, 0);
  }

  // -------------------- Parse replay body -----------
  var line = iterator.line;
  if (line === null) {
    // Try to restart the iterator
    line = iterator.next();
  }
  var newStatesCnt = 0;
  var partialState = replay.partialState;
  var timeStep = 1 / replay.frequency;
  var previousGameState = replay.states.length > 0 ? replay.states[replay.states.length - 1].gameState : '';
  var newState = null;
  var newAgent = null;

  while (line !== null && newStatesCnt < maxStates) {
    switch (line[0]) {
      case 'T': // Team info line
        if (line.length > 4) {
          replay.leftTeam.name = line[1].slice(1, -1);
          replay.leftTeam.color = new THREE.Color(line[2]);
          replay.rightTeam.name = line[3].slice(1, -1);
          replay.rightTeam.color = new THREE.Color(line[4]);
          replay.onTeamsUpdated();
        } else if (line.length > 2) {
          replay.leftTeam.name = line[1].slice(1, -1);
          replay.rightTeam.name = line[3].slice(1, -1);
          replay.onTeamsUpdated();
        }
        break;

      case 'S': // State line
        // Try to create a new world state from partial state
        newState = partialState.toWorldState();

        if (newState !== null) {
          // New world state successfully created
          replay.states.push(newState);
          newStatesCnt++;

          // Check for game state change
          if (newState.time > 0 && previousGameState != newState.gameState) {
            replay.gameStateChanges.push(new JaSMIn.GameStateChange(newState.time, previousGameState, newState.gameState));
            previousGameState = newState.gameState;
          }

          // Progress time
          partialState.time = partialState.time + timeStep;
        }

        // Reset partial game state with new state line information
        partialState.reset(
          parseFloat(line[1]) / 10,
          line[2],
          parseInt(line[3], 10),
          parseInt(line[4], 10)
        );
        break;

      case 'b': // Ball line
        partialState.ballState = JaSMIn.ReplayParser.parseBallState(line, replay.type);
        break;

      case 'l': // Left agent line
      case 'L':
        newAgent = JaSMIn.ReplayParser.parseAgentState(line, replay.type, replay.version, replay.leftTeam, leftIdxList);

        if (newAgent !== null) {
          partialState.leftAgentStates[newAgent.playerNo] = newAgent;
        }
        break;

      case 'r': // Right agent line
      case 'R':
        newAgent = JaSMIn.ReplayParser.parseAgentState(line, replay.type, replay.version, replay.rightTeam, rightIdxList);

        if (newAgent !== null) {
          partialState.rightAgentStates[newAgent.playerNo] = newAgent;
        }
        break;
    }

    line = iterator.next();
  }

  // Refresh replay
  if (newStatesCnt > 0) {
    replay.onStatesUpdated();
  }

  // Start parsing job, parsing $maxStates world states per run
  if (line !== null) {
    setTimeout(JaSMIn.ReplayParser.parseReplayBody, 1, iterator, replay, maxStates, leftIdxList, rightIdxList);
  } else if (iterator.fullyLoaded) {
    replay.finalize();
  }
};


/**
 * [parseAgentState description]
 *
 * @param  {!Array<string>} line the agent line
 * @param  {!number} type the replay type
 * @param  {!number} version the replay version
 * @param  {!JaSMIn.TeamDescription} teamDescription
 * @param  {!Array<number>} indexList
 * @return {?JaSMIn.AgentState}
 */
JaSMIn.ReplayParser.parseAgentState = function(line, type, version, teamDescription, indexList) {
  // Agent line format:
  // Idx,  Parameter,                     Description
  // 0:    team- & definition-indicator   L/l and R/r for left / right team, upper case letters indicate an agent definition
  // 1:    player-number                  The player number
  // 2:    robot-model                    The robot model of the agent (only present if first parameter was a upper case letter)

  // 2D data (starting at dataIdx):
  // +0:   position                       The agent's x and y position
  // +2:   orientation                    The agent's z rotation
  // +3:   stamina                        The stamina of the agent (version >=2)
  // +4:   head-angle                     The local view direction of the agent; or neck-yaw angle (version >=2)

  // 3D data (starting at dataIdx):
  // +0:   position                       The agent's x, y and z position
  // +3:   orientation                    The agent's rotation as quaternion: scalar, x, y, z
  // +7:   joint-angles                   A list of joint angle values

  var playerNo = parseInt(line[1], 10);
  var dataIdx = 2;

  // Check for model definition
  if (version > 0 && (line[0] === 'L' || line[0] === 'R')) {
    teamDescription.addAgent(playerNo, line[2]);
    indexList[playerNo] = teamDescription.getRecentModelIdx(playerNo);
    dataIdx = 3;
  }

  var modelIdx = indexList[playerNo] === undefined ? 0 : indexList[playerNo];

  // Parse player state data
  var position = null;
  var quat = null;
  var jointData = [];
  var agentData = [];
  var angle = 0;

  if (type == JaSMIn.ReplayTypes.TWOD) {
    if (line.length < dataIdx + 3) {
      // Not enough data!
      return null;
    }

    position = new THREE.Vector3(parseFloat(line[dataIdx]), 0, parseFloat(line[dataIdx + 1]));
    angle = parseFloat(line[dataIdx + 2]);
    quat = new THREE.Quaternion();
    quat.setFromAxisAngle(JaSMIn.Vector3_unitY, -angle * JaSMIn.PIby180);
    dataIdx += 3;

    if (line.length >= dataIdx + 2) {
      angle = parseFloat(line[dataIdx]) - angle;
      if (angle > 180) {
        angle -= 360;
      } else if (angle < -180) {
        angle += 360;
      }

      jointData[0] = -angle * JaSMIn.PIby180;
      agentData[JaSMIn.AgentStateData.STAMINA] = parseFloat(line[dataIdx + 1].slice(1));
    }
  } else {
    if (line.length < dataIdx + 7) {
      // Not enough data!
      return null;
    }

    position = new THREE.Vector3(
        parseInt(line[dataIdx], 10) / 1000,
        parseInt(line[dataIdx + 2], 10) / 1000,
        -parseInt(line[dataIdx + 1], 10) / 1000);
    quat = new THREE.Quaternion(
        parseInt(line[dataIdx + 4], 10) / 1000,
        parseInt(line[dataIdx + 6], 10) / 1000,
        -parseInt(line[dataIdx + 5], 10) / 1000,
        parseInt(line[dataIdx + 3], 10) / 1000);
    dataIdx += 7;

    for (var i = dataIdx; i < line.length; ++i) {
      jointData[i - dataIdx] = parseFloat(line[i]) * JaSMIn.PIby180 / 100;
    }
  }

  return new JaSMIn.AgentState(playerNo, modelIdx, position, quat, jointData, agentData);
};


/**
 * [parseBallState description]
 *
 * @param  {!Array<string>} line
 * @param  {!number} type
 * @return {?JaSMIn.ObjectState}
 */
JaSMIn.ReplayParser.parseBallState = function(line, type) {
  // Ball line format:
  // Idx,  Parameter,                     Description
  // 0:    b                              The first parameter is defined as the small letter 'b'

  // 2D data:
  // 1:    position                       The ball's x and y position
  // 3:    dont-no                        Some further two values... have to clarify

  // 3D data:
  // 1:    position                       The ball's x, y and z position
  // 4:    orientation                    The ball's rotation as quaternion: scalar, x, y, z

  var position = null;
  var quat = null;

  if (type == JaSMIn.ReplayTypes.TWOD) {
    if (line.length < 2) {
      // Not enough data!
      return null;
    }

    position = new THREE.Vector3(parseFloat(line[1]), 0.2, parseFloat(line[2]));
    quat = new THREE.Quaternion();
  } else {
    if (line.length < 7) {
      // Not enough data!
      return null;
    }

    position = new THREE.Vector3(
        parseInt(line[1], 10) / 1000,
        parseInt(line[3], 10) / 1000,
        -parseInt(line[2], 10) / 1000);
    quat = new THREE.Quaternion(
        parseInt(line[5], 10) / 1000,
        parseInt(line[7], 10) / 1000,
        -parseInt(line[6], 10) / 1000,
        parseInt(line[4], 10) / 1000);
  }

  return new JaSMIn.ObjectState(position, quat);
};
