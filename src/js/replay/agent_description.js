/**
 * The AgentDescription class definition.
 *
 * The AgentDescription provides information about the robot model and player number of an agent.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.AgentDescription');

goog.require('JaSMIn');



/**
 * AgentDescription Constructor
 * Create a new AgentDescription.
 *
 * @struct
 * @constructor
 * @export
 * @param {!number} number the player number of this agent
 * @param {!number} side the team side (see JaSMIn.TeamSide)
 * @param {!string} model the name of the robot model used by this agent
 */
JaSMIn.AgentDescription = function(number, side, model) {
  /**
   * The player number of the agent.
   * @type {!number}
   */
  this.playerNo = number;

  /**
   * The team side (see JaSMIn.TeamSide)
   * @type {!number}
   */
  this.side = side;

  /**
   * A list of robot models used by this agent.
   * @type {!Array<string>}
   */
  this.models = [];
  this.models.push(model);

  /**
   * The index of the last used robot model of this agent.
   * @type {!number}
   */
  this.recentModelIdx = 0;
};



/**
 * Check if this agent is the goal keeper.
 * @return {boolean} true if this agent is the goal keeper, false otherwise
 */
JaSMIn.AgentDescription.prototype.isGoalie = function() {
  return this.playerNo == 1;
};



/**
 * Add the given robot model to the list of models if not yet present.
 *
 * @param {!string} model the agent robot model
 * @return {!boolean} false if nothing was modified, true otherwise
 */
JaSMIn.AgentDescription.prototype.addModel = function(model) {
  var idx = this.models.indexOf(model);

  // Add model to model list if not yet present
  if (idx === -1) {
    this.models.push(model);
    this.recentModelIdx = this.models.length - 1;
    return true;
  } else {
    this.recentModelIdx = idx;
    return false;
  }
};


/**
 * Retrieve a letter representing the side.
 *
 * @param  {!boolean=} uppercase true for upper case letter, false for lower case
 * @return {!string} 'l'/'L' for left side, 'r'/'R' for right side
 */
JaSMIn.AgentDescription.prototype.getSideLetter = function(uppercase) {
  return JaSMIn.getSideLetter(this.side, uppercase);
};
