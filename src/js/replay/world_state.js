/**
 * The WorldState class definition.
 *
 * The WorldState provides information about the state of the game, the ball and all agents on the field.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.WorldState');

goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.ObjectState');



/**
 * WorldState Constructor
 * Create a new WorldState holding the given information.
 *
 * @constructor
 * @struct
 * @export
 * @param {!number} time the global time
 * @param {!number} gameTime the game time
 * @param {!string} gameState the game state
 * @param {!number} goalsLeft the number of goals for the left team
 * @param {!number} goalsRight the number of goals for the right team
 * @param {!JaSMIn.ObjectState} ball the ball state
 * @param {!Array<!JaSMIn.AgentState | undefined>} leftAgents array of agent states for the left team
 * @param {!Array<!JaSMIn.AgentState | undefined>} rightAgents array of agent states for the right team
 */
JaSMIn.WorldState = function(time, gameTime, gameState, goalsLeft, goalsRight, ball, leftAgents, rightAgents) {
  /**
   * The global time.
   * @type {!number}
   */
  this.time = time;

  /**
   * The game time.
   * @type {!number}
   */
  this.gameTime = gameTime;

  /**
   * The state of the game (Before-Kick-Off, Kick-Off-Left, etc.).
   * @type {!string}
   */
  this.gameState = gameState;

  /**
   * The left team score.
   * @type {!number}
   */
  this.scoreLeft = goalsLeft;

  /**
   * The right team score.
   * @type {!number}
   */
  this.scoreRight = goalsRight;

  /**
   * The state of the ball.
   * @type {!JaSMIn.ObjectState}
   */
  this.ballState = ball;

  /**
   * The states of all left agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.leftAgentStates = leftAgents;

  /**
   * The states of all right agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.rightAgentStates = rightAgents;
};
