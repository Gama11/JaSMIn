/**
 * The TeamDescription class definition.
 *
 * The TeamDescription provides information about a team.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.TeamDescription');

goog.require('JaSMIn.AgentDescription');



/**
 * TeamDescription Constructor
 * Create a new TeamDescription with the given parameters.
 *
 * @struct
 * @constructor
 * @export
 * @param {!string} name the name of the team
 * @param {!THREE.Color} color the team color
 * @param {!number} side the team side (see JaSMIn.TeamSide)
 */
JaSMIn.TeamDescription = function(name, color, side) {
  /**
   * The name of the team.
   * @type {!string}
   */
  this.name = name;

  /**
   * The list of agents playing for this team.
   * @type {!Array<!JaSMIn.AgentDescription>}
   */
  this.agents = [];

  /**
   * The team color.
   * @type {!THREE.Color}
   */
  this.color = color;

  /**
   * The team side (see JaSMIn.TeamSide)
   * @type {!number}
   */
  this.side = side;
};



/**
 * Add an agent description for the given player number and robot model.
 * If there doesn't exist an agent description to the given player number,
 * this method will create a new agent description with the given player number and robot model.
 * If there already exists an agent description with the given player number,
 * this method will add the given robot model to the agent description if it is not yet present.
 *
 * @param {number} number the agent player number
 * @param {string} model the agent robot model
 * @return {!boolean} false if nothing was modified, true otherwise
 */
JaSMIn.TeamDescription.prototype.addAgent = function(number, model) {
  var i = this.agents.length;

  // Check if there already exists a agent description with the given number
  while (i--) {
    if (this.agents[i].playerNo === number) {
      // Add the given robot model to the agent
      return this.agents[i].addModel(model);
    }
  }

  // If no agent definition was found for the given player number, create a new one
  this.agents.push(new JaSMIn.AgentDescription(number, this.side, model));

  return true;
};


/**
 * Retrieve the index of the last used model of the agent with the given player number.
 *
 * @param  {!number} number the player number of the agent of interest
 * @return {!number} the index of the last used robot model
 */
JaSMIn.TeamDescription.prototype.getRecentModelIdx = function(number) {
  var i = this.agents.length;

  // Retrieve the requested index from the agent description if existing
  while (i--) {
    if (this.agents[i].playerNo === number) {
      return this.agents[i].recentModelIdx;
    }
  }

  // return zero by default, if no corresponding agent description was found
  return 0;
};


/**
 * Retrieve a letter representing the side.
 *
 * @param  {!boolean=} uppercase true for upper case letter, false for lower case
 * @return {!string} 'l'/'L' for left side, 'r'/'R' for right side
 */
JaSMIn.TeamDescription.prototype.getSideLetter = function(uppercase) {
  return JaSMIn.getSideLetter(this.side, uppercase);
};
