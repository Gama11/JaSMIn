goog.provide('JaSMIn.CameraController');

goog.require('JaSMIn');



/**
 * CameraController Constructor
 *
 * @interface
 */
JaSMIn.CameraController = function() {};





/**
 * Enable/Disable the camera controller.
 *
 * @param {!boolean} enabled true to enable the camera controller, false for disabling
 */
JaSMIn.CameraController.prototype.setEnabled = function(enabled) {};



/**
 * Set the object to track with the camera.
 *
 * @param  {!THREE.Vector3} bounds the new world bounds
 * @return {void}
 */
JaSMIn.CameraController.prototype.setBounds = function(bounds) {};



/**
 * Set the object to track with the camera.
 *
 * @param  {!THREE.Vector2} fieldDimensions the field dimensions
 * @return {void}
 */
JaSMIn.CameraController.prototype.setFieldDimensions = function(fieldDimensions) {};



/**
 * Update the camera controller.
 * The update is needed for keyboard movements, as well for tracking objects.
 *
 * @param  {!number} deltaT the time since the last render call
 * @return {void}
 */
JaSMIn.CameraController.prototype.update = function(deltaT) {};
