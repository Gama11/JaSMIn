/**
 * The Monitor class definition.
 *
 * The Monitor is the central class representing the player logic, canvas handling, etc.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Monitor');

goog.require('JaSMIn');
goog.require('JaSMIn.CameraController');
goog.require('JaSMIn.FPSMeter');
goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.UI.GameInfoBoard');
goog.require('JaSMIn.UI.MonitorStatisticsBoard');
goog.require('JaSMIn.World');



/**
 * Monitor Constructor
 *
 * @constructor
 * @param {!Element} container the monitor root element
 */
JaSMIn.Monitor = function(container) {
  /**
   * The player container (player root dom-element)
   * @type {!Element}
   */
  this.domElement = container;

  /**
   * The (persistant) monitor settings.
   * @type {!JaSMIn.MonitorSettings}
   */
  this.settings = new JaSMIn.MonitorSettings();

  /**
   * The width of the canvas.
   * @type {!number}
   */
  this.canvasWidth = container.clientWidth;

  /**
   * The height of the canvas.
   * @type {!number}
   */
  this.canvasHeight = container.clientHeight;

  /**
   * The time passed since the last render call.
   * @type {!number}
   */
  this.timeSinceLastRenderCall = 0;

  /**
   * The world representation
   * @type {?JaSMIn.World}
   */
  this.world = null;

  /**
   * The welcome scene.
   * @type {!THREE.Scene}
   */
  this.welcomeScene = new THREE.Scene();

  /**
   * The Camera object.
   * @type {!THREE.PerspectiveCamera}
   */
  this.camera = new THREE.PerspectiveCamera(45, this.canvasWidth / this.canvasHeight, 0.1, 2000);

  this.camera.position.set(20, 15, 15);
  this.camera.lookAt(JaSMIn.Vector3_zero);
  this.camera.updateMatrix();

  /**
   * The WebGLRenderer from threejs.
   * @type {!THREE.WebGLRenderer}
   */
  this.renderer = new THREE.WebGLRenderer({ antialias: true });

  this.renderer.setSize(this.canvasWidth, this.canvasHeight);
  this.renderer.shadowMap.enabled = this.settings.getShadowsEnabled();
  // this.renderer.shadowMap.type = THREE.BasicShadowMap;
  // this.renderer.shadowMap.type = THREE.PCFShadowMap; // default
  // this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  this.domElement.appendChild(this.renderer.domElement);

  /**
   * The camera controller.
   * @type {?JaSMIn.CameraController}
   */
  this.cameraController = null;

  /**
   * The clock used to measure render times.
   * @type {!THREE.Clock}
   */
  this.clock = new THREE.Clock(true);

  /**
   * A helper util for monitoring the fps of the monitor.
   * @type {!JaSMIn.FPSMeter}
   */
  this.fpsMeter = new JaSMIn.FPSMeter(10);

  /**
   * A listener to notify when a render cycle was triggered.
   * @type {(!Function | undefined)}
   */
  this.onBeforeRenderCallback = undefined;

  /**
   * The render function bound to this monitor instance.
   * @type {!Function}
   */
  this.renderFunction = this.render.bind(this);

  /**
   * The game info board.
   * @type {!JaSMIn.UI.GameInfoBoard}
   */
  this.gameInfoBoard = new JaSMIn.UI.GameInfoBoard();

  this.gameInfoBoard.setVisible(false);
  this.domElement.appendChild(this.gameInfoBoard.domElement);

  /**
   * The game info board.
   * @type {!JaSMIn.UI.MonitorStatisticsBoard}
   */
  this.monitorStats = new JaSMIn.UI.MonitorStatisticsBoard();

  this.monitorStats.setResolution(this.canvasWidth, this.canvasHeight);
  this.monitorStats.setVisible(this.settings.getMonitorStatisticsEnabled());
  this.domElement.appendChild(this.monitorStats.domElement);


  // Create the monitor welcome scene
  this.createWelcomeScene();


  // -------------------- Listeners -------------------- //
  /** @type {!Function} */
  this.shadowListener = this.applyShadowSettings.bind(this);
  /** @type {!Function} */
  this.teamColorsListener = this.applyTeamColorSettings.bind(this);
  /** @type {!Function} */
  this.monitorStatsListener = this.applyMonitorStatisticsSettings.bind(this);

  // Add settings change listeners
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLOR_LEFT, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.SHADOWS_ENABLED, this.shadowListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED, this.monitorStatsListener);



  /** @type {!Function} */
  this.onNewSecondListener = this.onNewSecond.bind(this);

  // Add fullscreen change listeners
  this.fpsMeter.onNewSecond = this.onNewSecondListener;
};



/**
 * Create the welcome scene displayed while loading.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.createWelcomeScene = function() {
  // TODO: Create some simple logo or so in 3D...
};



/**
 * Set the current world to render.
 *
 * @param {?JaSMIn.World} world the world representation
 * @return {void}
 */
JaSMIn.Monitor.prototype.setWorld = function(world) {
  this.world = world;

  var wordValid = world !== null;

  // Hide game info panel
  this.gameInfoBoard.setVisible(wordValid);

  // Disable camera controller
  this.cameraController.setEnabled(wordValid);

  if (wordValid) {
    // Apply team color settings and set team names in game info board
    this.applyTeamColorSettings();
    this.world.setShadowsEnabled(this.settings.getShadowsEnabled());
    this.gameInfoBoard.updateTeamNames(world.leftTeam.description.name, world.rightTeam.description.name);

    this.cameraController.setBounds(world.boundingBox);
    this.cameraController.setFieldDimensions(world.field.fieldDimensions);
  } else {
    // Reset camera perspective for welcome scene
    this.camera.position.copy(JaSMIn.Vector3_unitZ);
    this.camera.lookAt(JaSMIn.Vector3_zero);
    this.camera.updateMatrix();
  }

  // Render either world (continuously) or welcome scene (once)
  requestAnimationFrame(this.renderFunction);
};



/**
 * Set a new camera controller instance.
 *
 * @param {?JaSMIn.CameraController} camCon the new camera controller
 * @return {void}
 */
JaSMIn.Monitor.prototype.setCameraController = function(camCon) {
  this.cameraController = camCon;

  if (this.cameraController !== null) {
    if (this.world !== null) {
      this.cameraController.setEnabled(true);

      this.cameraController.setBounds(this.world.boundingBox);
      this.cameraController.setFieldDimensions(this.world.field.fieldDimensions);
    } else {
      this.cameraController.setEnabled(false);

      // Reset camera perspective for welcome scene
      this.camera.position.copy(JaSMIn.Vector3_unitZ);
      this.camera.lookAt(JaSMIn.Vector3_zero);
      this.camera.updateMatrix();
    }
  }
};



/**
 * The central render function.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.render = function() {
  if (this.world !== null) {
    requestAnimationFrame(this.renderFunction);
  }

  this.timeSinceLastRenderCall = this.clock.getDelta();

  // Update fps-meter
  this.fpsMeter.update(this.clock.elapsedTime);

  if (this.timeSinceLastRenderCall > 0.5) {
    console.log('LAAAAG: ' + this.timeSinceLastRenderCall);
  }

  // Notify camera controller and render listener if present
  if (this.onBeforeRenderCallback !== undefined) {
    this.onBeforeRenderCallback(this.timeSinceLastRenderCall);
  }
  if (this.cameraController !== null) {
    this.cameraController.update(this.timeSinceLastRenderCall);
  }

  // Render scene
  var scene = this.world !== null ? this.world.scene : this.welcomeScene;
  this.renderer.render(scene, this.camera);
};



/**
 * Automatically resize the monitor canvas to fit the root container.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.autoResize = function() {
  this.setPlayerDimensions(this.domElement.clientWidth, this.domElement.clientHeight);
};


/**
 * Set the monitor canvas dimensions.
 *
 * @param {!number} width the canvas width
 * @param {!number} height the canvas height
 */
JaSMIn.Monitor.prototype.setPlayerDimensions = function(width, height) {
  if (this.canvasWidth === width && this.canvasHeight === height) {
    return;
  }

  // Remember new player dimensions
  this.canvasWidth = width;
  this.canvasHeight = height;

  this.monitorStats.setResolution(width, height);

  // Update renderer size
  this.renderer.setSize(width, height);

  // Update camera parameters
  this.camera.aspect = width / height;
  this.camera.updateProjectionMatrix();
};



/**
 * Toggle monitor fullscreen mode.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.toggleFullscreen = function() {
  if (this.domElement === JaSMIn.UI.getFullscreenElement()) {
    JaSMIn.UI.cancelFullscreen();
  } else {
    JaSMIn.UI.requestFullscreenFor(this.domElement);
  }
};



/**
 * Apply team color settings.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.applyTeamColorSettings = function() {
  var useDefault = !this.settings.getTeamColorsEnabled();

  if (this.world !== null) {
    this.world.leftTeam.setColor(useDefault ? undefined : this.settings.getTeamColor(true));
    this.world.rightTeam.setColor(useDefault ? undefined : this.settings.getTeamColor(false));
    this.gameInfoBoard.updateTeamColors(this.world.leftTeam.color, this.world.rightTeam.color);
  }
};



/**
 * Apply shadow setting.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.applyShadowSettings = function() {
  var enabled = this.settings.getShadowsEnabled();
  this.renderer.shadowMap.enabled = enabled;

  if (this.world !== null) {
    this.world.setShadowsEnabled(enabled);
  }
};



/**
 * Apply monitor info settings.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.applyMonitorStatisticsSettings = function() {
  this.monitorStats.setVisible(this.settings.getMonitorStatisticsEnabled());
};



/**
 * Callback for when a new second started.
 *
 * @return {void}
 */
JaSMIn.Monitor.prototype.onNewSecond = function() {
  if (this.settings.getMonitorStatisticsEnabled()) {
    this.monitorStats.setFPS(this.fpsMeter.getMostRecentFPS());
  }
};
