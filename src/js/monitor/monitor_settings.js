/**
 * The MonitorSettings class definition.
 *
 * The MonitorSettings provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MonitorSettings');



/**
 * MonitorSettings Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.MonitorSettings = function() {

  /**
   * Use user defined team colors?
   * @type {!boolean}
   */
  this.teamColorsEnabled = JaSMIn.Persistance.readBoolean(JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED, false);

  /**
   * User defined color for the left team.
   * @type {!THREE.Color}
   */
  this.leftTeamColor = new THREE.Color(JaSMIn.Persistance.readString(JaSMIn.MonitorSettings.TEAM_COLOR_LEFT, '#cccc00'));

  /**
   * User defined color for the right team.
   * @type {!THREE.Color}
   */
  this.rightTeamColor = new THREE.Color(JaSMIn.Persistance.readString(JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT, '#008fff'));

  /**
   * Interpolate world states?
   * @type {!boolean}
   */
  this.interpolateStates = JaSMIn.Persistance.readBoolean(JaSMIn.MonitorSettings.INTERPOLATE_STATES, true);

  /**
   * Are shadows enabled?
   * @type {!boolean}
   */
  this.shadowsEnabled = JaSMIn.Persistance.readBoolean(JaSMIn.MonitorSettings.SHADOWS_ENABLED, false);

  /**
   * Show monitor statistics?
   * @type {!boolean}
   */
  this.monitorStatisticsEnabled = JaSMIn.Persistance.readBoolean(JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED, false);
};





/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED = 'teamColorsEnabled';

/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.TEAM_COLOR_LEFT = 'teamColorLeft';

/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT = 'teamColorRight';

/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.INTERPOLATE_STATES = 'interpolateStates';

/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.SHADOWS_ENABLED = 'shadowsEnabled';

/**
 * Item key constant.
 * @const {!string}
 */
JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED = 'monitorStatisticsEnabled';



/**
 * Enable/Disable usage of user defined team colors.
 *
 * @param {!boolean} value true for enabled, false for disabled
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.setTeamColorsEnabled = function(value) {
  if (this.teamColorsEnabled !== value) {
    this.teamColorsEnabled = value;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED, value);

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED,
      newValue: value
    });
  }
};



/**
 * Retrieve the flag for using user defined team colors.
 *
 * @return {!boolean} true for enabled, false for disabled
 */
JaSMIn.MonitorSettings.prototype.getTeamColorsEnabled = function() {
  return this.teamColorsEnabled;
};



/**
 * Store the given color as the user defined color for the left team.
 *
 * @param {!THREE.Color} color the user defined team color
 * @param {!boolean} leftSide true if the color is for the left team, false for the right team
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.setTeamColor = function(color, leftSide) {
  if (leftSide) {
    this.leftTeamColor = color;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.TEAM_COLOR_LEFT, color.getStyle());

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.TEAM_COLOR_LEFT,
      newValue: color
    });
  } else {
    this.rightTeamColor = color;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT, color.getStyle());

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT,
      newValue: color
    });
  }
};



/**
 * Read the user defined color for a team.
 *
 * @param  {!boolean} leftSide true for left side, false for right side
 * @return {!THREE.Color} the user defined team color
 */
JaSMIn.MonitorSettings.prototype.getTeamColor = function(leftSide) {
  return leftSide ? this.leftTeamColor : this.rightTeamColor;
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorSettings.prototype.setInterpolateStates = function(value) {
  if (this.interpolateStates !== value) {
    this.interpolateStates = value;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.INTERPOLATE_STATES, value);

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.INTERPOLATE_STATES,
      newValue: value
    });
  }
};



/**
 * @return {!boolean}
 */
JaSMIn.MonitorSettings.prototype.getInterpolateStates = function() {
  return this.interpolateStates;
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorSettings.prototype.setShadowsEnabled = function(value) {
  if (this.shadowsEnabled !== value) {
    this.shadowsEnabled = value;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.SHADOWS_ENABLED, value);

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.SHADOWS_ENABLED,
      newValue: value
    });
  }
};



/**
 * @return {!boolean}
 */
JaSMIn.MonitorSettings.prototype.getShadowsEnabled = function() {
  return this.shadowsEnabled;
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorSettings.prototype.setMonitorStatisticsEnabled = function(value) {
  if (this.monitorStatisticsEnabled !== value) {
    this.monitorStatisticsEnabled = value;
    JaSMIn.Persistance.storeItem(JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED, value);

    this.dispatchEvent({
      type: JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED,
      newValue: value
    });
  }
};



/**
 * @return {!boolean}
 */
JaSMIn.MonitorSettings.prototype.getMonitorStatisticsEnabled = function() {
  return this.monitorStatisticsEnabled;
};




// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.MonitorSettings.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.MonitorSettings.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.MonitorSettings.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
