/**
 * The InfoOverlay class definition.
 *
 * The InfoOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.InfoOverlay');

goog.require('JaSMIn.UI');



/**
 * InfoOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 */
JaSMIn.UI.InfoOverlay = function() {
  goog.base(this, 'about-pane centered');
  // goog.base(this, 'jsm-info');

  this.innerElement.innerHTML = '<h1>JaSMIn</h1>' +
      '<h4>Javascript Soccer Monitor Interface</h4>' +
      '<h5>v' + JaSMIn.REVISION + '</h5>' +
      '<a href="https://gitlab.com/robocup.info/JaSMIn" target="_blank">GitLab Project</a><br/>';
};
goog.inherits(JaSMIn.UI.InfoOverlay, JaSMIn.UI.Overlay);
