/**
 * The SettingsOverlay class definition.
 *
 * The SettingsOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.SettingsOverlay');

goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.UI.SingleChoiceItem');
goog.require('JaSMIn.UI.ToggleItem');



/**
 * SettingsOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 * @param {!JaSMIn.MonitorSettings} settings the monitor settings
 */
JaSMIn.UI.SettingsOverlay = function(settings) {
  goog.base(this, 'jsm-settings');

  /**
   * The monitor settings.
   * @type {!JaSMIn.MonitorSettings}
   */
  this.settings = settings;


  var scope = this;

  /**
   * The main menu list.
   * @type {!Element}
   */
  this.mainMenu = JaSMIn.UI.createUL('jsm-menu');
  this.innerElement.appendChild(this.mainMenu);

  /**
   * The interpolate state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.interpolateItem = new JaSMIn.UI.ToggleItem('Interpolation', 'On', 'Off', settings.getInterpolateStates(), 'item');
  this.interpolateItem.onChanged = function(isOn) {
    settings.setInterpolateStates(isOn);
  };
  this.mainMenu.appendChild(this.interpolateItem.domElement);

  /**
   * The shadows enabeld state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.shadowsItem = new JaSMIn.UI.ToggleItem('Shadows', 'On', 'Off', settings.getShadowsEnabled(), 'item');
  this.shadowsItem.onChanged = function(isOn) {
    settings.setShadowsEnabled(isOn);
  };
  this.mainMenu.appendChild(this.shadowsItem.domElement);

  /**
   * The monitor statistics state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.statisticsItem = new JaSMIn.UI.ToggleItem('Monitor Statistics', 'On', 'Off', settings.getMonitorStatisticsEnabled(), 'item');
  this.statisticsItem.onChanged = function(isOn) {
    settings.setMonitorStatisticsEnabled(isOn);
  };
  this.mainMenu.appendChild(this.statisticsItem.domElement);

  /**
   * The team colors enabled state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.teamColorsItem = new JaSMIn.UI.ToggleItem('Team Colors', 'Fix', 'Auto', settings.getTeamColorsEnabled(), 'item');
  this.teamColorsItem.onChanged = function(isOn) {
    settings.setTeamColorsEnabled(isOn);
    scope.teamColorChooserItem.style.height = isOn ? scope.teamColorChooserItem.scrollHeight + 'px' : '0px';
  };
  this.mainMenu.appendChild(this.teamColorsItem.domElement);

  /**
   * The team color chooser item.
   * @type {!Element}
   */
  this.teamColorChooserItem = JaSMIn.UI.createDiv('collapsable');
  this.teamColorChooserItem.onclick = function(event) { event.stopPropagation(); };

  if (!settings.getTeamColorsEnabled()) {
    this.teamColorChooserItem.style.height = '0px';
  }

  /**
   * The left team color chooser.
   * @type {!Element}
   */
  this.leftTeamColorChooser = JaSMIn.UI.createColorChooser('#' + settings.getTeamColor(true).getHexString(), 'Left team color', 'team-color');
  this.leftTeamColorChooser.onchange = function() {
    settings.setTeamColor(scope.leftTeamColorChooser.value, true);
  };

  this.rightTeamColorChooser = JaSMIn.UI.createColorChooser('#' + settings.getTeamColor(false).getHexString(), 'Right team color', 'team-color');
  this.rightTeamColorChooser.onchange = function() {
    settings.setTeamColor(scope.rightTeamColorChooser.value, false);
  };
  this.teamColorChooserItem.appendChild(this.leftTeamColorChooser);
  this.teamColorChooserItem.appendChild(this.rightTeamColorChooser);
  this.teamColorsItem.domElement.appendChild(this.teamColorChooserItem);



  // -------------------- Listeners -------------------- //
  /** @type {!Function} */
  this.interpolationListener = this.applyInterpolationSettings.bind(this);
  /** @type {!Function} */
  this.shadowListener = this.applyShadowSettings.bind(this);
  /** @type {!Function} */
  this.teamColorsListener = this.applyTeamColorSettings.bind(this);
  /** @type {!Function} */
  this.monitorStatsListener = this.applyMonitorStatisticsSettings.bind(this);

  // Add settings change listeners
  this.settings.addEventListener(JaSMIn.MonitorSettings.INTERPOLATE_STATES, this.interpolationListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLORS_ENABLED, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLOR_LEFT, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.TEAM_COLOR_RIGHT, this.teamColorsListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.SHADOWS_ENABLED, this.shadowListener);
  this.settings.addEventListener(JaSMIn.MonitorSettings.MONITOR_STATISTICS_ENABLED, this.monitorStatsListener);
};
goog.inherits(JaSMIn.UI.SettingsOverlay, JaSMIn.UI.Overlay);







/**
 * Apply team color settings.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyTeamColorSettings = function() {
  var isOn = this.settings.getTeamColorsEnabled();

  this.teamColorsItem.setState(isOn);
  this.teamColorChooserItem.style.height = isOn ? this.teamColorChooserItem.scrollHeight + 'px' : '0px';
  this.leftTeamColorChooser.value = '#' + this.settings.getTeamColor(true).getHexString();
  this.rightTeamColorChooser.value = '#' + this.settings.getTeamColor(false).getHexString();
};



/**
 * Apply shadow setting.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyShadowSettings = function() {
  this.shadowsItem.setState(this.settings.getShadowsEnabled());
};



/**
 * Apply interpolate states setting.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyInterpolationSettings = function() {
  this.interpolateItem.setState(this.settings.getInterpolateStates());
};



/**
 * Apply monitor info settings.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyMonitorStatisticsSettings = function() {
  this.statisticsItem.setState(this.settings.getMonitorStatisticsEnabled());
};
