goog.provide('JaSMIn.UI.OverlayManager');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.Overlay');



/**
 * Enum holding the known overlays.
 * @enum {!number}
 */
JaSMIn.UI.Overlays = {
  ABOUT: 0,
  SETTINGS: 1,
  INFO: 2
};


/**
 * OverlayManager Constructor
 *
 * @constructor
 * @param {!JaSMIn.Monitor} monitor the monitor instance
 * @param {!Element} container the container to add all overlays
 */
JaSMIn.UI.OverlayManager = function(monitor, container) {
  /**
   * The about overlay.
   * @type {!JaSMIn.UI.AboutOverlay}
   */
  this.aboutOverlay = new JaSMIn.UI.AboutOverlay();
  container.appendChild(this.aboutOverlay.domElement);

  /**
   * The settings overlay.
   * @type {!JaSMIn.UI.SettingsOverlay}
   */
  this.settingsOverlay = new JaSMIn.UI.SettingsOverlay(monitor.settings);
  container.appendChild(this.settingsOverlay.domElement);

  /**
   * The info overlay.
   * @type {!JaSMIn.UI.InfoOverlay}
   */
  this.infoOverlay = new JaSMIn.UI.InfoOverlay();
  container.appendChild(this.infoOverlay.domElement);
};



/**
 * Toggle the overlay with the given ID.
 *
 * @param  {!JaSMIn.UI.Overlays} overlay the overlay ID
 */
JaSMIn.UI.OverlayManager.prototype.toggleOverlay = function(overlay) {
  switch (overlay) {
    case JaSMIn.UI.Overlays.ABOUT:
      this.hideAll(this.aboutOverlay);
      this.aboutOverlay.toggleVisibility();
      break;
    case JaSMIn.UI.Overlays.SETTINGS:
      this.hideAll(this.settingsOverlay);
      this.settingsOverlay.toggleVisibility();
      break;
    case JaSMIn.UI.Overlays.INFO:
      this.hideAll(this.infoOverlay);
      this.infoOverlay.toggleVisibility();
      break;
    default:
      this.hideAll();
      break;
  }
};



/**
 * Hide all visible overlays.
 *
 * @param  {?JaSMIn.UI.Overlay=} but the overlay which visibility should be untouched
 */
JaSMIn.UI.OverlayManager.prototype.hideAll = function(but) {
  if (this.aboutOverlay !== but) {
    this.aboutOverlay.setVisible(false);
  }

  if (this.settingsOverlay !== but) {
    this.settingsOverlay.setVisible(false);
  }

  if (this.infoOverlay !== but) {
    this.infoOverlay.setVisible(false);
  }
};

