/**
 * The AboutOverlay class definition.
 *
 * The AboutOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.AboutOverlay');

goog.require('JaSMIn.UI');



/**
 * AboutOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 */
JaSMIn.UI.AboutOverlay = function() {
  goog.base(this, 'about-pane centered');

  this.innerElement.innerHTML = '<h1>JaSMIn</h1>' +
      '<h4>Javascript Soccer Monitor Interface</h4>' +
      '<h5>v' + JaSMIn.REVISION + '</h5>' +
      '<a href="https://gitlab.com/robocup.info/JaSMIn" target="_blank">GitLab Project</a><br/>' +
      '';//<a href="https://wiki.robocup.info/jasmin/" target="_blank">Wiki Pages</a>';
};
goog.inherits(JaSMIn.UI.AboutOverlay, JaSMIn.UI.Overlay);
