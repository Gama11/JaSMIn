goog.provide('JaSMIn.UI.GameInfoBoard');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.WorldState');



/**
 * GameInfoBoard Contructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 */
JaSMIn.UI.GameInfoBoard = function() {
  goog.base(this, 'jsm-game-info');

  var infoLine = JaSMIn.UI.createDiv('info-line');
  this.domElement.appendChild(infoLine);

  var stateLine = JaSMIn.UI.createDiv('state-line');
  this.domElement.appendChild(stateLine);

  /**
   * The game time label.
   * @type {!Element}
   */
  this.gameTimeLbl = JaSMIn.UI.createSpan('00:00.<small>00</small>', 'game_time_lbl');
  infoLine.appendChild(this.gameTimeLbl);

  /**
   * The left team label
   * @type {!Element}
   */
  this.leftTeamLbl = JaSMIn.UI.createSpan('Left', 'left-team');
  infoLine.appendChild(this.leftTeamLbl);

  /**
   * The left team score label
   * @type {!Element}
   */
  this.leftScoreLbl = JaSMIn.UI.createSpan('0', 'left-score');
  infoLine.appendChild(this.leftScoreLbl);

  /**
   * The score divider label
   * @type {!Element}
   */
  this.scoreDividerLbl = JaSMIn.UI.createSpan(':', 'score-divider');
  infoLine.appendChild(this.scoreDividerLbl);

  /**
   * The right team score label
   * @type {!Element}
   */
  this.rightScoreLbl = JaSMIn.UI.createSpan('0', 'right-score');
  infoLine.appendChild(this.rightScoreLbl);

  /**
   * The right team label
   * @type {!Element}
   */
  this.rightTeamLbl = JaSMIn.UI.createSpan('Right', 'right-team');
  infoLine.appendChild(this.rightTeamLbl);

  /**
   * The game state label
   * @type {!Element}
   */
  this.gameStateLbl = JaSMIn.UI.createSpan('Unknown', 'game_state_lbl');
  stateLine.appendChild(this.gameStateLbl);

  /**
   * The world state used during the last update.
   * @type {?JaSMIn.WorldState}
   */
  this.previousWorldState = null;
};
goog.inherits(JaSMIn.UI.GameInfoBoard, JaSMIn.UI.Panel);



/**
 * Update the time, score and game state labels.
 *
 * @param  {!JaSMIn.WorldState} state the current world state
 * @return {void}
 */
JaSMIn.UI.GameInfoBoard.prototype.update = function(state) {
  // Do a full update for the first incomming state
  if (this.previousWorldState === null) {
    this.gameTimeLbl.innerHTML = JaSMIn.UI.toMMSScs(state.gameTime, true);
    this.gameStateLbl.innerHTML = state.gameState;
    this.leftScoreLbl.innerHTML = state.scoreLeft;
    this.rightScoreLbl.innerHTML = state.scoreRight;
    this.previousWorldState = state;
    return;
  }

  // Update game time label if changed
  if (this.previousWorldState.gameTime !== state.gameTime) {
    this.gameTimeLbl.innerHTML = JaSMIn.UI.toMMSScs(state.gameTime, true);
  }

  // Update game state label if changed
  if (this.previousWorldState.gameState !== state.gameState) {
    this.gameStateLbl.innerHTML = state.gameState;
  }

  // Update left score label if changed
  if (this.previousWorldState.scoreLeft !== state.scoreLeft) {
    this.leftScoreLbl.innerHTML = state.scoreLeft;
  }

  // Update right score label if changed
  if (this.previousWorldState.scoreRight !== state.scoreRight) {
    this.rightScoreLbl.innerHTML = state.scoreRight;
  }

  // Remember current state
  this.previousWorldState = state;
};



/**
 * Update the team labels.
 *
 * @param  {!string} leftTeamName the name of the left team
 * @param  {!string} rightTeamName the name of the right team
 * @return {void}
 */
JaSMIn.UI.GameInfoBoard.prototype.updateTeamNames = function(leftTeamName, rightTeamName) {
  this.leftTeamLbl.innerHTML = leftTeamName;
  this.rightTeamLbl.innerHTML = rightTeamName;
};



/**
 * Update the team labels.
 *
 * @param  {!THREE.Color} leftTeamColor the color of the left team
 * @param  {!THREE.Color} rightTeamColor the color of the right team
 * @return {void}
 */
JaSMIn.UI.GameInfoBoard.prototype.updateTeamColors = function(leftTeamColor, rightTeamColor) {
  // Left Team
  var leftColor = JaSMIn.UI.getForegroundColor(leftTeamColor);

  this.leftTeamLbl.style.backgroundColor = this.leftScoreLbl.style.backgroundColor = leftTeamColor.getStyle();
  this.leftTeamLbl.style.color = this.leftScoreLbl.style.color = leftColor.getStyle();

  // Right Team
  var rightColor = JaSMIn.UI.getForegroundColor(rightTeamColor);

  this.rightTeamLbl.style.backgroundColor = this.rightScoreLbl.style.backgroundColor = rightTeamColor.getStyle();
  this.rightTeamLbl.style.color = this.rightScoreLbl.style.color = rightColor.getStyle();
};
