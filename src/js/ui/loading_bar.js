goog.provide('JaSMIn.UI.LoadingBar');

goog.require('JaSMIn.ReplayLoader');
goog.require('JaSMIn.ReplayLoaderEvents');
goog.require('JaSMIn.UI');



/**
 * LoadingBar Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 */
JaSMIn.UI.LoadingBar = function() {
  goog.base(this, 'jsm-loading-bar');

  /**
   * The progress label.
   * @type {!Element}
   */
  this.progressBar = document.createElement('div');
  this.progressBar.style.width = '0px';
  this.domElement.appendChild(this.progressBar);

  /**
   * The progress label.
   * @type {!Element}
   */
  this.label = document.createElement('span');
  this.label.innerHTML = '0 / 0 KB';
  this.domElement.appendChild(this.label);

  // By default hide the loading bar
  this.setVisible(false);
};
goog.inherits(JaSMIn.UI.LoadingBar, JaSMIn.UI.Panel);



/**
 * @override
 */
JaSMIn.UI.LoadingBar.prototype.onVisibilityChanged = function(visible) {
  if (visible) {
    this.progressBar.style.width = '0%';
    this.label.innerHTML = '0 / 0 MB';
  }
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!number} percent the loaded percentage
 * @param  {!number} loadedMB the MBs loaded
 * @param  {!number} totalMB the total MBs to load
 * @return {void}
 */
JaSMIn.UI.LoadingBar.prototype.setProgress = function(percent, loadedMB, totalMB) {
  this.progressBar.style.width = '' + percent.toFixed(1) + '%';
  this.label.innerHTML = '' + loadedMB.toFixed(3) + ' / ' + totalMB.toFixed(3) + ' MB';
};



