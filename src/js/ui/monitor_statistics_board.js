goog.provide('JaSMIn.UI.MonitorStatisticsBoard');

goog.require('JaSMIn.UI');



/**
 * MonitorStatisticsBoard Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 */
JaSMIn.UI.MonitorStatisticsBoard = function() {
  goog.base(this, 'jsm-monitor-stats');

  var list = document.createElement('ul');
  this.domElement.appendChild(list);

  /**
   * The FPS label.
   * @type {!Element}
   */
  this.fpsLbl = JaSMIn.UI.createSpan('0');

  var item = document.createElement('li');
  item.appendChild(JaSMIn.UI.createSpan('FPS:', 'label'));
  item.appendChild(this.fpsLbl);
  list.appendChild(item);

  /**
   * The reosultion label.
   * @type {!Element}
   */
  this.resolutionLbl = JaSMIn.UI.createSpan('0 x 0px');

  // item = document.createElement('li');
  // item.appendChild(JaSMIn.UI.createSpan('Resolution:', 'label'));
  // item.appendChild(this.resolutionLbl);
  // list.appendChild(item);
};
goog.inherits(JaSMIn.UI.MonitorStatisticsBoard, JaSMIn.UI.Panel);



/**
 * Set the fps label.
 *
 * @param {!number} fps the current fps
 */
JaSMIn.UI.MonitorStatisticsBoard.prototype.setFPS = function(fps) {
  this.fpsLbl.innerHTML = fps;
};



/**
 * Set the fps label.
 *
 * @param {!number} width the monitor width
 * @param {!number} height the monitor height
 */
JaSMIn.UI.MonitorStatisticsBoard.prototype.setResolution = function(width, height) {
  this.resolutionLbl.innerHTML = '' + width + ' x ' + height + 'px';
};
