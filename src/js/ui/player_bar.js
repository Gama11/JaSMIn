/**
 * The PlayerBar class definition.
 *
 * The PlayerBar abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.PlayerBar');

goog.require('JaSMIn.Player');
goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.OverlayManager');



/**
 * PlayerBar Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!JaSMIn.Player} player the player instance
 * @param {!JaSMIn.UI.OverlayManager} overlayManager
 */
JaSMIn.UI.PlayerBar = function(player, overlayManager) {
  goog.base(this, 'full-size');

  /**
   * The player instance.
   * @type {?JaSMIn.Player}
   */
  this.player = player;

  /**
   * The shadow pane.
   * @type {!Element}
   */
  this.shadowPane = JaSMIn.UI.createDiv('jsm-shadow-pane');
  this.domElement.appendChild(this.shadowPane);

  /**
   * The waiting indicator.
   * @type {!Element}
   */
  this.waitingIndicator = JaSMIn.UI.createDiv('jsm-waiting-indicator');
  this.waitingIndicator.title = 'Waiting for new stream data...';
  this.domElement.appendChild(this.waitingIndicator);
  JaSMIn.UI.setVisibility(this.waitingIndicator, false);


  /**
   * The bottom player bar pane.
   * @type {!Element}
   */
  this.barPane = JaSMIn.UI.createDiv('jsm-player-bar');
  this.domElement.appendChild(this.barPane);

  var scope = this;


  /**
   * The player time slider.
   * @type {!Element}
   */
  this.timeSlider = document.createElement('input');
  this.timeSlider.className = 'time-slider';
  this.timeSlider.type = 'range';
  this.timeSlider.min = 0;
  this.timeSlider.max = player.replay.states.length - 1;
  this.timeSlider.step = 1;
  this.timeSlider.value = 0;
  this.timeSlider.addEventListener('change', function(evt) {
    scope.player.jump(this.value);
  });
  this.timeSlider.addEventListener('input', function(evt) {
    scope.player.jump(this.value);
  });
  this.barPane.appendChild(this.timeSlider);

  /**
   * The player controls pane.
   * @type {!Element}
   */
  this.leftPane = JaSMIn.UI.createDiv('left');
  this.barPane.appendChild(this.leftPane);

  /**
   * The player settings pane.
   * @type {!Element}
   */
  this.rightPane = JaSMIn.UI.createDiv('right');
  this.barPane.appendChild(this.rightPane);


  /**
   * The Play / Pause / Replay button
   * @type {!Element}
   */
  this.playBtn = JaSMIn.UI.createButton('',
    'player-btn icon-play',
    'Play',
    function() {
      overlayManager.hideAll();
      scope.player.playPause();
    },
    true);
  this.leftPane.appendChild(this.playBtn);

  /**
   * The jump previous goal button
   * @type {!Element}
   */
  this.jumpPreviousGoalBtn = JaSMIn.UI.createButton('',
    'player-btn icon-jump-prev',
    'Jump Previous Goal',
    function() {
      overlayManager.hideAll();
      scope.player.jumpGoal(true);
    },
    true);
  this.leftPane.appendChild(this.jumpPreviousGoalBtn);

  /**
   * The step backwards button
   * @type {!Element}
   */
  this.stepBackwardsBtn = JaSMIn.UI.createButton('',
    'player-btn icon-step-back',
    'Step Backwards',
    function() {
      overlayManager.hideAll();
      scope.player.step(true);
    },
    true);
  this.leftPane.appendChild(this.stepBackwardsBtn);

  /**
   * The step forwards button
   * @type {!Element}
   */
  this.stepForwardBtn = JaSMIn.UI.createButton('',
    'player-btn icon-step-fwd',
    'Step Forwards',
    function() {
      overlayManager.hideAll();
      scope.player.step();
    },
    true);
  this.leftPane.appendChild(this.stepForwardBtn);

  /**
   * The jump next goal button
   * @type {!Element}
   */
  this.jumpNextGoalBtn = JaSMIn.UI.createButton('',
    'player-btn icon-jump-next',
    'Jump Next Goal',
    function() {
      overlayManager.hideAll();
      scope.player.jumpGoal();
    },
    true);
  this.leftPane.appendChild(this.jumpNextGoalBtn);

  /**
   * The current time label
   * @type {!Element}
   */
  this.currentTimeLbl = JaSMIn.UI.createSpan('0:00.<small>00</small>', 'current-time');
  this.leftPane.appendChild(this.currentTimeLbl);

  /**
   * The time divider label
   * @type {!Element}
   */
  this.timeDividerLbl = JaSMIn.UI.createSpan('/', 'time-divider');
  this.leftPane.appendChild(this.timeDividerLbl);

  /**
   * The total time label
   * @type {!Element}
   */
  this.totalTimeLbl = JaSMIn.UI.createSpan(JaSMIn.UI.toMMSS(player.replay.duration), 'total-time');
  this.leftPane.appendChild(this.totalTimeLbl);


  /**
   * The toggle info button
   * @type {!Element}
   */
  this.infoBtn = JaSMIn.UI.createButton('',
    'player-btn icon-info',
    'Show Info',
    function() { overlayManager.toggleOverlay(JaSMIn.UI.Overlays.INFO); },
    true);
  this.rightPane.appendChild(this.infoBtn);

  /**
   * The toggle settings button
   * @type {!Element}
   */
  this.settingsBtn = JaSMIn.UI.createButton('',
    'player-btn icon-settings',
    'Settings',
    function() { overlayManager.toggleOverlay(JaSMIn.UI.Overlays.SETTINGS); },
    true);
  this.rightPane.appendChild(this.settingsBtn);

  /**
   * The fullscreen button
   * @type {!Element}
   */
  this.fullscreenBtn = JaSMIn.UI.createButton('',
    'player-btn icon-fullscreen',
    'Fullscreen',
    function() {
      overlayManager.hideAll();
      scope.player.monitor.toggleFullscreen();
    },
    true);
  this.rightPane.appendChild(this.fullscreenBtn);

  if (!JaSMIn.UI.isFullscreenEnabled()) {
    this.fullscreenBtn.disabled = true;
    this.fullscreenBtn.title = 'Fullscreen not supported!';
  }



  // Refresh state of controls
  this.refreshPlayBtn();



  /** @type {!Function} */
  this.onPlayerStateChangedListener = this.onPlayerStateChanged.bind(this);
  /** @type {!Function} */
  this.onPlayTimeChangedListener = this.onPlayTimeChanged.bind(this);
  /** @type {!Function} */
  this.onReplayChangedListener = this.onReplayChanged.bind(this);

  // Add listeners
  player.addEventListener(JaSMIn.PlayerEvents.STATE_CHANGE, this.onPlayerStateChangedListener);
  player.addEventListener(JaSMIn.PlayerEvents.TIME_PROGRESS, this.onPlayTimeChangedListener);
  player.addEventListener(JaSMIn.PlayerEvents.REPLAY_CHANGE, this.onReplayChangedListener);
};
goog.inherits(JaSMIn.UI.PlayerBar, JaSMIn.UI.Panel);



/**
 * Set the player instance.
 *
 * @param  {?JaSMIn.Player} player the new player instance
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.setPlayer = function(player) {
  if (this.player === player) {
    return;
  }

  if (this.player !== null) {
    this.player.removeEventListener(JaSMIn.PlayerEvents.STATE_CHANGE, this.onPlayerStateChangedListener);
    this.player.removeEventListener(JaSMIn.PlayerEvents.TIME_PROGRESS, this.onPlayTimeChangedListener);
    this.player.removeEventListener(JaSMIn.PlayerEvents.REPLAY_CHANGE, this.onReplayChangedListener);
  }

  this.player = player;

  if (this.player !== null) {
    // Rest controls
    this.timeSlider.value = 0;
    this.timeSlider.max = this.player.replay.states.length - 1;

    JaSMIn.UI.setIcon(this.playBtn, 'icon-play');
    this.playBtn.title = 'Play';

    this.currentTimeLbl.innerHTML = '0:00.<small>00</small>';
    this.totalTimeLbl.innerHTML = JaSMIn.UI.toMMSS(this.player.replay.duration);

    this.refreshPlayBtn();

    // Make player bar visible
    this.setVisible(true);


    this.player.addEventListener(JaSMIn.PlayerEvents.STATE_CHANGE, this.onPlayerStateChangedListener);
    this.player.addEventListener(JaSMIn.PlayerEvents.TIME_PROGRESS, this.onPlayTimeChangedListener);
    this.player.addEventListener(JaSMIn.PlayerEvents.REPLAY_CHANGE, this.onReplayChangedListener);
  } else {
    // No valid player insance, thus hide player bar
    this.setVisible(false);
  }
};



/**
 * The onPlayerStateChanged callback.
 *
 * @param  {{type: !string, newState: !JaSMIn.PlayerStates}} event the event
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.onPlayerStateChanged = function(event) {
  this.refreshPlayBtn();

  if (this.player.state === JaSMIn.PlayerStates.WAITING) {
    JaSMIn.UI.setVisibility(this.waitingIndicator, true);
  } else {
    JaSMIn.UI.setVisibility(this.waitingIndicator, false);
  }
};



/**
 * Refresh the player button.
 *
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.refreshPlayBtn = function() {
  switch (this.player.state) {
    case JaSMIn.PlayerStates.PAUSE:
      JaSMIn.UI.setIcon(this.playBtn, 'icon-play');
      this.playBtn.title = 'Play';
      break;
    case JaSMIn.PlayerStates.PLAY:
    case JaSMIn.PlayerStates.WAITING:
      JaSMIn.UI.setIcon(this.playBtn, 'icon-pause');
      this.playBtn.title = 'Pause';
      break;
    case JaSMIn.PlayerStates.END:
      JaSMIn.UI.setIcon(this.playBtn, 'icon-replay');
      this.playBtn.title = 'Replay';
      break;
    default:
      break;
  }
};



/**
 * The onPlayTimeChanged callback.
 *
 * @param  {{type: !string, newTime: !number}} event the event
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.onPlayTimeChanged = function(event) {
  this.timeSlider.value = this.player.playIndex;

  // Hack for webkit-browsers which don't support input range progress indication
  var percent = (this.timeSlider.value / this.timeSlider.max) * 100;
  this.timeSlider.style.background = '-webkit-linear-gradient(left, #e00 0%, #e00 ' + percent + '%, rgba(204,204,204, 0.7) ' + percent + '%)';

  this.currentTimeLbl.innerHTML = JaSMIn.UI.toMMSScs(event.newTime);
};



/**
 * The onPlayerStateChanged callback.
 *
 * @param  {{type: !string, newState: !JaSMIn.PlayerStates}} event the event
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.onReplayChanged = function(event) {
  this.timeSlider.max = this.player.replay.states.length - 1;

  // Hack for webkit-browsers which don't support input range progress indication
  var percent = (this.timeSlider.value / this.timeSlider.max) * 100;
  this.timeSlider.style.background = '-webkit-linear-gradient(left, #e00 0%, #e00 ' + percent + '%, rgba(204,204,204, 0.7) ' + percent + '%)';

  this.totalTimeLbl.innerHTML = JaSMIn.UI.toMMSS(this.player.replay.duration);
};


/**
 * @param {!boolean} entering true if the monitor entered fullscreen, false if it was leaving fullscreen
 * @return {void}
 */
JaSMIn.UI.PlayerBar.prototype.onFullscreenChange = function(entering) {
  if (entering) {
    JaSMIn.UI.setIcon(this.fullscreenBtn, 'icon-partscreen');
    this.fullscreenBtn.title = 'Leave Fullscreen';
  } else {
    JaSMIn.UI.setIcon(this.fullscreenBtn, 'icon-fullscreen');
    this.fullscreenBtn.title = 'Fullscreen';
  }
};
