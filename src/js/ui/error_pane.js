/**
 * The ErrorOverlay class definition.
 *
 * The ErrorOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.ErrorOverlay');

goog.require('JaSMIn.UI');



/**
 * ErrorOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 */
JaSMIn.UI.ErrorOverlay = function() {
  goog.base(this, 'jsm-error-pane full-size');

  // Create title label
  this.domElement.appendChild(JaSMIn.UI.createSpan('Error...', 'title'));

  /**
   * The error message label.
   * @type {!Element}
   */
  this.errorLbl = JaSMIn.UI.createSpan('n/a', 'error');
  this.domElement.appendChild(this.errorLbl);
};
goog.inherits(JaSMIn.UI.ErrorOverlay, JaSMIn.UI.Panel);



/**
 * Set the error message to display and show/hide the component.
 *
 * @param {!string=} message the error message to display, or undefined to clear the last error and hide the component
 */
JaSMIn.UI.ErrorOverlay.prototype.setErrorMessage = function(message) {
  if (message === undefined) {
    this.setVisible(false);
  } else {
    this.errorLbl.innerHTML = message;

    this.setVisible(true);
  }
};
