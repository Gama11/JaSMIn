goog.provide('JaSMIn.UI');

goog.require('JaSMIn');




/**
 * General user interface namespace, definitions, etc.
 *
 * @author Stefan Glaser
 */
JaSMIn.UI = {};



/**
 * Calculate the brightness value of a color.
 *
 * @param  {!THREE.Color} color the color to check
 * @return {!number} the brightness value between 0 and 255
 */
JaSMIn.UI.getBrightness = function(color) {
  return 255 * Math.sqrt(
      color.r * color.r * 0.241 +
      color.g * color.g * 0.691 +
      color.b * color.b * 0.068);
};



/**
 * Retrieve the default foreground color for a given background color.
 *
 * @param  {!THREE.Color} color the background color
 * @return {!THREE.Color} the forground color
 */
JaSMIn.UI.getForegroundColor = function(color) {
  return JaSMIn.UI.getBrightness(color) < 130 ? JaSMIn.Color_eee : JaSMIn.Color_333;
};



/**
 * Make the given element visible or invisible.
 *
 * @param {!Element} element the DOM element
 * @param {!boolean=} visible true for visible, false for invisible
 */
JaSMIn.UI.setVisibility = function(element, visible) {
  if (visible === undefined || visible) {
    element.style.display = '';
  } else {
    element.style.display = 'none';
  }
};



/**
 * Check if the given component is visible.
 *
 * @param {!Element} element the DOM element
 * @return {!boolean} true for visible, false for invisible
 */
JaSMIn.UI.isVisible = function(element) {
  return element.style.display != 'none';
};



/**
 * Create a new DOM Element.
 *
 * @param  {!string} element the tag name
 * @param  {!string=} content the content of the new element
 * @param  {!string=} className the css class string
 * @return {!Element}
 */
JaSMIn.UI.createElement = function(element, content, className) {
  var newElement = document.createElement(element);

  if (content !== undefined) {
    newElement.innerHTML = content;
  }

  if (className !== undefined) {
    newElement.className = className;
  }

  return newElement;
};



/**
 * Create a new div element.
 *
 * @param  {!string=} className the css class string
 * @return {!Element} the new div element
 */
JaSMIn.UI.createDiv = function(className) {
  return JaSMIn.UI.createElement('div', undefined, className);
};



/**
 * Create a new span element.
 *
 * @param  {!string=} text the content of the span
 * @param  {!string=} className the css class string
 * @return {!Element} the new span element
 */
JaSMIn.UI.createSpan = function(text, className) {
  return JaSMIn.UI.createElement('span', text, className);
};



/**
 * Create a new button input element.
 *
 * @param  {!string=} text the button text
 * @param  {!string=} className the button css class string
 * @param  {!string=} toolTip the button tool tip
 * @param  {!Function=} action the button action
 * @param  {!boolean=} preventDefault prevent the default mouse action
 * @return {!Element} the new button input element
 */
JaSMIn.UI.createButton = function(text, className, toolTip, action, preventDefault) {
  var btn = JaSMIn.UI.createElement('button', text, className);

  if (toolTip !== undefined) {
    btn.title = toolTip;
  }

  if (action !== undefined) {
    if (preventDefault) {
      btn.addEventListener('mousedown', function(evt) {
        evt.preventDefault();
        action(evt);
      });
    } else {
      btn.addEventListener('mousedown', action);
    }

    btn.addEventListener('keydown', function(evt) {
      if (evt.keyCode == JaSMIn.KeyCodes.ENTER ||
          evt.keyCode == JaSMIn.KeyCodes.SPACE) {
        action(evt);
      }
    });
  }

  return btn;
};



/**
 * Create a new ul element.
 *
 * @param  {!string=} className the css class string
 * @return {!Element} the new ul element
 */
JaSMIn.UI.createUL = function(className) {
  return JaSMIn.UI.createElement('ul', undefined, className);
};



/**
 * Create a new li element.
 *
 * @param  {!string=} className the css class string
 * @return {!Element} the new li element
 */
JaSMIn.UI.createLI = function(className) {
  return JaSMIn.UI.createElement('li', undefined, className);
};



/**
 * Create a new button input element.
 *
 * @param  {!string=} text the link text
 * @param  {!string=} href the link href attribute
 * @param  {!string=} className the link css class string
 * @param  {!string=} toolTip the link tool tip
 * @return {!Element} the new anchor (a) element
 */
JaSMIn.UI.createHref = function(text, href, className, toolTip) {
  var link = JaSMIn.UI.createElement('a', text, className);

  if (href !== undefined) {
    link.href = href;
  }

  if (toolTip !== undefined) {
    link.title = toolTip;
  }

  return link;
};



/**
 * Create a new single choice form element.
 *
 * @param  {!Array<!string>} options the options to display
 * @param  {!number=} preSelected the index of the preselected entry
 * @return {!Element} the new single choice form
 */
JaSMIn.UI.createSingleChoiceForm = function(options, preSelected) {
  if (preSelected === undefined) {
    preSelected = 0;
  }

  var form = JaSMIn.UI.createElement('form', undefined, 'jsm-s-choice');

  for (var i = 0; i < options.length; ++i) {
    var btnID = THREE.Math.generateUUID();

    var label = JaSMIn.UI.createElement('label');
    label.innerHTML = options[i];
    label.htmlFor = btnID;

    var btn = JaSMIn.UI.createElement('input');
    btn.id = btnID;
    btn.type = 'radio';
    btn.name = 'userOptions';
    btn.value = options[i];

    if (i === preSelected) {
      btn.checked = true;
    }

    form.appendChild(btn);
    form.appendChild(label);
  }

  form.onclick = function(event) { event.stopPropagation(); };

  return form;
};



/**
 * Create a new color chooser element.
 *
 * @param  {!string} value the initial value
 * @param  {!string=} title the tool tip text
 * @param  {!string=} className the css class string
 * @return {!Element} the new li element
 */
JaSMIn.UI.createColorChooser = function(value, title, className) {
  var chooser = JaSMIn.UI.createElement('input', undefined, className);
  chooser.type = 'color';
  chooser.value = value;

  if (title) {
    chooser.title = title;
  }

  return chooser;
};



/**
 * Set the icon of an element.
 *
 * @param {!Element} element the element to set the icon class on
 * @param {!string} iconClass the new icon class
 */
JaSMIn.UI.setIcon = function(element, iconClass) {
  var iconClassIdx = element.className.indexOf('icon-');

  if (iconClassIdx === -1) {
    element.className += ' ' + iconClass;
  } else {
    var spaceCharIdx = element.className.indexOf(' ', iconClassIdx);

    //console.log('Classes: ' + element.className + ' || IconIdx: ' + iconClassIdx + ' || SpaceIdx: ' + spaceCharIdx);

    if (spaceCharIdx !== -1) {
      // Intermediate class
      element.className = element.className.slice(0, iconClassIdx) + iconClass + element.className.slice(spaceCharIdx - 1);
    } else {
      // Last class
      element.className = element.className.slice(0, iconClassIdx) + iconClass;
    }

    //console.log('Classes-after: ' + element.className);
  }
};



/**
 * Convert the given time into MM:SS.cs format. E.g. 02:14.84
 *
 * @param  {!number} time the time to convert
 * @param  {!boolean=} fillZero fill leading zero minutes
 * @return {!string} the time string
 */
JaSMIn.UI.toMMSScs = function(time, fillZero) {
  var millsNum = Math.floor(time * 100);
  var minutes = Math.floor(millsNum / 6000);
  var seconds = Math.floor((millsNum - (minutes * 6000)) / 100);
  var mills = millsNum - (seconds * 100) - (minutes * 6000);

  if (fillZero && minutes < 10) { minutes = '0' + minutes; }
  if (seconds < 10) { seconds = '0' + seconds; }
  if (mills < 10) { mills = '0' + mills; }

  return minutes + ':' + seconds + '.<small>' + mills + '</small>';
};



/**
 * Convert the given time into MM:SS format. E.g. 02:14
 *
 * @param  {!number} time the time to convert
 * @param  {!boolean=} fillZero fill leading zero minutes
 * @return {!string} the time string
 */
JaSMIn.UI.toMMSS = function(time, fillZero) {
  var secNum = Math.floor(time);
  var minutes = Math.floor(secNum / 60);
  var seconds = secNum - (minutes * 60);

  if (fillZero && minutes < 10) { minutes = '0' + minutes; }
  if (seconds < 10) { seconds = '0' + seconds; }

  return minutes + ':' + seconds;
};



/**
 * Simple event listener function to prevent further event propagation.
 *
 * @param {!Event} event the event
 * @return {void}
 */
JaSMIn.UI.StopEventPropagationListener = function(event) {
  event.stopPropagation();
};



/**
 * Check if the browser supports a fullscreen mode.
 *
 * @return {!boolean} true if the browser supports a fullscreen mode, false if not
 */
JaSMIn.UI.isFullscreenEnabled = function() {
  return document.mozFullScreenEnabled === true ||
          document.msFullscreenEnabled === true ||
          document.webkitFullscreenEnabled === true ||
          document.fullscreenEnabled === true;
};



/**
 * Check if the browser is currently in fullscreen mode.
 *
 * @return {!boolean} true if the browser is currently in fullscreen mode, false if not
 */
JaSMIn.UI.inFullscreen = function() {
  var fullscreenElement = JaSMIn.UI.getFullscreenElement();

  return fullscreenElement !== undefined && fullscreenElement !== null;
};



/**
 * Check if the browser supports a fullscreen mode.
 *
 * @return {(?Element | undefined)} the fullscreen element or undefined if no such element exists
 */
JaSMIn.UI.getFullscreenElement = function() {
  if (document.mozFullScreenElement !== undefined) {
    return document.mozFullScreenElement;
  } else if (document.msFullscreenElement !== undefined) {
    return document.msFullscreenElement;
  } else if (document.webkitFullscreenElement !== undefined) {
    return document.webkitFullscreenElement;
  } else {
    return document.fullscreenElement;
  }
};



/**
 * Request fullscreen mode for the given element.
 *
 * @param {!Element} element the element to request fullscreen for
 * @return {void}
 */
JaSMIn.UI.requestFullscreenFor = function(element) {
  if (element.mozRequestFullScreen !== undefined) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullscreen !== undefined) {
    element.webkitRequestFullscreen();
  } else if (element.msRequestFullscreen !== undefined) {
    element.msRequestFullscreen();
  } else if (element.requestFullScreen !== undefined) {
    element.requestFullScreen();
  }
};



/**
 * Cancel the fullscreen mode.
 *
 * @return {void}
 */
JaSMIn.UI.cancelFullscreen = function() {
  if (document.mozFullScreen && document.mozCancelFullScreen !== undefined) {
    document.mozCancelFullScreen();
  } else if (document.webkitIsFullScreen && document.webkitCancelFullScreen !== undefined) {
    document.webkitCancelFullScreen();
  } else if (document.msFullscreenEnabled && document.msExitFullscreen !== undefined) {
    document.msExitFullscreen();
  } else if (document.fullScreen && document.cancelFullScreen !== undefined) {
    document.cancelFullScreen();
  }
};
