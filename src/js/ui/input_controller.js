/**
 * The InputController class definition.
 *
 * The InputController
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.InputController');

goog.require('JaSMIn.Monitor');
goog.require('JaSMIn.Player');
goog.require('JaSMIn.UI');
goog.require('JaSMIn.UniversalCameraController');



/**
 * The monitor input controller.
 *
 * @constructor
 * @param {!JaSMIn.Monitor} monitor the monitor instance
 */
JaSMIn.UI.InputController = function(monitor) {

  /**
   * The input controller element.
   * @type {!Element}
   */
  this.domElement = document.createElement('div');
  this.domElement.tabIndex = 0;
  this.domElement.className = 'jsm-input-pane full-size';

  /**
   * The monitor instance.
   * @type {!JaSMIn.Monitor}
   */
  this.monitor = monitor;

  /**
   * The player instance.
   * @type {?JaSMIn.Player}
   */
  this.player = null;

  /**
   * The camera controller.
   * @type {!JaSMIn.UniversalCameraController}
   */
  this.camCon = new JaSMIn.UniversalCameraController(monitor.camera, this.domElement);


  /**
   * Enable/Disable camera controller.
   * @type {!boolean}
   */
  this.enabled = true;

  /**
   * The selected object.
   * @type {?JaSMIn.MovableObject}
   */
  this.selectedObject = null;

  /**
   * Indicator if the current mouse event series indicates a select action.
   * @type {!boolean}
   */
  this.selectAction = false;

  /**
   * Indicator if the current mouse event series indicates a play/pause action.
   * @type {!boolean}
   */
  this.playPauseAction = false;

  /**
   * Indicator if document mouse listeners are active.
   * @type {!boolean}
   */
  this.docMouseEnabled = false;


  this.onContextMenuListener = this.onContextMenu.bind(this);
  this.onMouseDownListener = this.onMouseDown.bind(this);
  this.onMouseUpListener = this.onMouseUp.bind(this);
  this.onMouseMoveListener = this.onMouseMove.bind(this);
  this.onMouseWheelListener = this.onMouseWheel.bind(this);
  this.onMouseInListener = this.onMouseIn.bind(this);

  this.onTouchStartListener = this.onTouchStart.bind(this);
  this.onTouchEndListener = this.onTouchEnd.bind(this);
  this.onTouchMoveListener = this.onTouchMove.bind(this);

  this.onKeyDownListener = this.onKeyDown.bind(this);
  this.onKeyPressedListener = this.onKeyPressed.bind(this);
  this.onKeyUpListener = this.onKeyUp.bind(this);


  this.domElement.addEventListener('contextmenu', this.onContextMenuListener);
  this.domElement.addEventListener('mousedown', this.onMouseDownListener);
  this.domElement.addEventListener('mousewheel', this.onMouseWheelListener);
  this.domElement.addEventListener('MozMousePixelScroll', this.onMouseWheelListener); // firefox

  // this.domElement.addEventListener('touchstart', this.onTouchStartListener);
  // this.domElement.addEventListener('touchend', onTouchEnd);
  // this.domElement.addEventListener('touchmove', onTouchMove);

  this.domElement.addEventListener('keydown', this.onKeyDownListener);
  this.domElement.addEventListener('keypress', this.onKeyPressedListener);
  this.domElement.addEventListener('keyup', this.onKeyUpListener);

  // Set camera controller on monitor
  this.monitor.setCameraController(this.camCon);
};



/**
 * Enable/Disable the camera controller.
 *
 * @param {!boolean} enabled true to enable the camera controller, false for disabling
 */
JaSMIn.UI.InputController.prototype.setEnabled = function(enabled) {
  if (this.enabled !== enabled) {
    this.enabled = enabled;
  }
};



/**
 * Set the current player instance.
 *
 * @param {?JaSMIn.Player} player the current player instance
 */
JaSMIn.UI.InputController.prototype.setPlayer = function(player) {
  this.player = player;
};



/**
 * Select the given object.
 *
 * @param {?JaSMIn.MovableObject} obj the object to select or null to clear the selection
 */
JaSMIn.UI.InputController.prototype.selectObject = function(obj) {
  if (this.selectedObject !== null) {
    this.selectedObject.setSelected(false);
  }

  this.selectedObject = obj;

  if (this.selectedObject !== null) {
    this.selectedObject.setSelected(true);
  }
};



/**
 * The on context menu event listener.
 *
 * @param  {!Event} event the event object
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onContextMenu = function(event) {
  if (!this.enabled) { return; }

  event.preventDefault();
};

/**
 * The on mouse down event listener.
 *
 * @param  {!Event} event the mouse event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onMouseDown = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onMouseDown');

  if (!this.docMouseEnabled) {
    this.docMouseEnabled = true;
    // console.log('InputCon: enable doc listeners');
    document.addEventListener('mouseup', this.onMouseUpListener);
    document.addEventListener('mousemove', this.onMouseMoveListener);
    document.addEventListener('mouseover', this.onMouseInListener);
  }

  var clickPos = JaSMIn.eventToLocalCenterPos(this.domElement, event);

  switch (event.button) {
    case THREE.MOUSE.LEFT:
      this.camCon.handleStartRotate(clickPos);
      this.selectAction = true;
      break;
    case THREE.MOUSE.MIDDLE:
      this.camCon.handleStartZoom(clickPos);
      this.playPauseAction = true;
      break;
    case THREE.MOUSE.RIGHT:
      this.camCon.handleStartPan(clickPos);
      break;
    default:
      break;
  }
};

/**
 * The on mouse up event listener.
 *
 * @param  {!Event} event the mouse event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onMouseUp = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onMouseUp');

  var clickPos = JaSMIn.eventToLocalCenterPos(this.domElement, event);

  switch (event.button) {
    case THREE.MOUSE.LEFT:
      this.camCon.handleEndRotate();

      if (this.selectAction) {
        // TODO: Try to select the actual object below the cursor
        this.selectObject(null);
      }
      break;
    case THREE.MOUSE.MIDDLE:
      this.camCon.handleEndZoom();

      if (this.player !== null && this.playPauseAction) {
        this.player.playPause();
      }
      break;
    case THREE.MOUSE.RIGHT:
      this.camCon.handleEndPan();
      break;
    default:
      break;
  }

  if (!this.camCon.isWaitingForMouseEvents()) {
    this.docMouseEnabled = false;
    // console.log('InputCon: disable doc listeners');
    document.removeEventListener('mouseup', this.onMouseUpListener);
    document.removeEventListener('mousemove', this.onMouseMoveListener);
    document.removeEventListener('mouseover', this.onMouseInListener);
  }
};

/**
 * The on mouse move event listener.
 *
 * @param  {!Event} event the mouse event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onMouseMove = function(event) {
  if (!this.enabled) { return; }

  // Prevent browser from selecting any text, drag/drop the element, etc.
  event.preventDefault();
  event.stopPropagation();

  this.selectAction = false;
  this.playPauseAction = false;

  // console.log('InputCon: onMouseMove');

  var clickPos = JaSMIn.eventToLocalCenterPos(this.domElement, event);

  this.camCon.handleRotate(clickPos);
  this.camCon.handleMouseZoom(clickPos);
  this.camCon.handlePan(clickPos);
};

/**
 * The on mouse wheel event listener.
 *
 * @param  {!Event} event the mouse event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onMouseWheel = function(event) {
  if (!this.enabled) { return; }

  // Prevent browser from scrolling and other stuff
  event.preventDefault();
  event.stopPropagation();

  // console.log('InputCon: onMouseWheel');

  var scrollAmount = 0;

  if (event.wheelDelta !== undefined) {
    // WebKit / Opera / Explorer 9
    scrollAmount = event.wheelDelta;
  } else if (event.detail !== undefined) {
    // Firefox
    scrollAmount = -event.detail;
  }

  if (scrollAmount !== 0) {
    var clickPos = JaSMIn.eventToLocalCenterPos(this.domElement, event);

    this.camCon.handleWheelZoom(clickPos, scrollAmount);
  }
};

/**
 * The on mouse in event listener.
 *
 * @param  {!Event} event the mouse event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onMouseIn = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onMouseIn');

  if ((0x01 & event.buttons) === 0) {
    this.camCon.handleEndRotate();
  }

  if ((0x02 & event.buttons) === 0) {
    this.camCon.handleEndPan();
  }

  if ((0x04 & event.buttons) === 0) {
    this.camCon.handleEndZoom();
  }

  if (!this.camCon.isWaitingForMouseEvents()) {
    this.docMouseEnabled = false;
    // console.log('InputCon: disable doc listeners');
    document.removeEventListener('mouseup', this.onMouseUpListener);
    document.removeEventListener('mousemove', this.onMouseMoveListener);
    document.removeEventListener('mouseover', this.onMouseInListener);
  }
};


/**
 * The on touch start event listener.
 *
 * @param  {!Event} event te touch event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onTouchStart = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onTouchStart');
};



/**
 * The on touch end event listener.
 *
 * @param  {!Event} event te touch event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onTouchEnd = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onTouchEnd');
};



/**
 * The on touch move event listener.
 *
 * @param  {!Event} event te touch event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onTouchMove = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onTouchMove');
};



/**
 * The on key down event listener.
 *
 * @param  {!Event} event the key event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onKeyDown = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onKeyDown');
  // console.log(event);

  switch (event.keyCode) {
    case JaSMIn.KeyCodes.LEFT:
    case JaSMIn.KeyCodes.A:
      this.camCon.moveLeft();
      break;
    case JaSMIn.KeyCodes.UP:
    case JaSMIn.KeyCodes.W:
      this.camCon.moveForward();
      break;
    case JaSMIn.KeyCodes.RIGHT:
    case JaSMIn.KeyCodes.D:
      this.camCon.moveRight();
      break;
    case JaSMIn.KeyCodes.DOWN:
    case JaSMIn.KeyCodes.S:
      this.camCon.moveBack();
      break;
    case JaSMIn.KeyCodes.PAGE_UP:
    case JaSMIn.KeyCodes.Q:
      this.camCon.moveUp();
      break;
    case JaSMIn.KeyCodes.PAGE_DOWN:
    case JaSMIn.KeyCodes.E:
      this.camCon.moveDown();
      break;
  }
};



/**
 * The on key down event listener.
 *
 * @param  {!Event} event the key event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onKeyPressed = function(event) {
  if (!this.enabled) { return; }

  var preventDefault = false;

  // console.log('InputCon: onKeyDown ' + event.key);
  // console.log(event);

  switch (event.charCode) {
    case JaSMIn.CharCodes.ZERO:
      // Select the ball object
      if (this.monitor.world !== null) {
        this.selectObject(this.monitor.world.ball);
      }
      break;
    case JaSMIn.CharCodes.ONE:
      this.camCon.setPredefinedPose(0);
      break;
    case JaSMIn.CharCodes.TWO:
      this.camCon.setPredefinedPose(1);
      break;
    case JaSMIn.CharCodes.THREE:
      this.camCon.setPredefinedPose(2);
      break;
    case JaSMIn.CharCodes.FOUR:
      this.camCon.setPredefinedPose(3);
      break;
    case JaSMIn.CharCodes.FIVE:
      this.camCon.setPredefinedPose(4);
      break;
    case JaSMIn.CharCodes.SIX:
      this.camCon.setPredefinedPose(5);
      break;
    case JaSMIn.CharCodes.SEVEN:
      this.camCon.setPredefinedPose(6);
      break;
    case JaSMIn.CharCodes.EIGHT:
      this.camCon.setPredefinedPose(7);
      break;
    case JaSMIn.CharCodes.NINE:
      break;
    case JaSMIn.CharCodes.p:
      if (this.player !== null) {
        this.player.playPause();
      }
      break;
    case JaSMIn.CharCodes.PLUS:
      if (this.player !== null) {
        this.player.step();
      }
      break;
    case JaSMIn.CharCodes.MINUS:
      if (this.player !== null) {
        this.player.step(true);
      }
      break;
    case JaSMIn.CharCodes.SPACE:
      // Prevent browser from scrolling
      preventDefault = true;

      // Toggle tracking of ball
      if (this.monitor.world !== null) {
        if (this.camCon.trackingObject === null) {
          this.camCon.trackObject(this.monitor.world.ball.objGroup);
        } else {
          this.camCon.trackObject(null);
        }
      }
      break;
  }


  if (event.charCode === 0) {
    // Some keys don't provide char codes
    switch (event.keyCode) {
      case JaSMIn.KeyCodes.ENTER:
        if (event.ctrlKey) {
          this.monitor.toggleFullscreen();
        }
        break;
    }
  }


  if (preventDefault) {
    event.preventDefault();
    event.stopPropagation();
  }
};



/**
 * The on key up event listener.
 *
 * @param  {!Event} event the key event
 * @return {void}
 */
JaSMIn.UI.InputController.prototype.onKeyUp = function(event) {
  if (!this.enabled) { return; }

  // console.log('InputCon: onUp');
  // console.log(event);

  switch (event.keyCode) {
    case JaSMIn.KeyCodes.LEFT:
    case JaSMIn.KeyCodes.A:
    case JaSMIn.KeyCodes.RIGHT:
    case JaSMIn.KeyCodes.D:
      this.camCon.stopMoveLeftRight();
      break;
    case JaSMIn.KeyCodes.UP:
    case JaSMIn.KeyCodes.W:
    case JaSMIn.KeyCodes.DOWN:
    case JaSMIn.KeyCodes.S:
      this.camCon.stopMoveForwardBack();
      break;
    case JaSMIn.KeyCodes.PAGE_UP:
    case JaSMIn.KeyCodes.Q:
    case JaSMIn.KeyCodes.PAGE_DOWN:
    case JaSMIn.KeyCodes.E:
      this.camCon.stopMoveUpDown();
      break;
  }
};
