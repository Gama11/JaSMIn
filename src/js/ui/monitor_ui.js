/**
 * The MonitorUI class definition.
 *
 * The MonitorUI abstracts the handling of the player related ui elements.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MonitorUI');

goog.require('JaSMIn');
goog.require('JaSMIn.Monitor');
goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.Player');
goog.require('JaSMIn.ReplayLoader');
goog.require('JaSMIn.UI.ErrorOverlay');
goog.require('JaSMIn.UI.InputController');
goog.require('JaSMIn.UI.LoadingBar');
goog.require('JaSMIn.UI.OverlayManager');
goog.require('JaSMIn.UI.PlayerBar');
goog.require('JaSMIn.WorldLoader');



/**
 * MonitorUI Constructor
 *
 * @constructor
 * @param {!Element} container the monitor root element
 */
JaSMIn.MonitorUI = function(container) {

  /**
   * The monitor root element
   * @type {!Element}
   */
  this.domElement = document.createElement('div');

  this.domElement.className = 'jsm-root';
  container.appendChild(this.domElement);

  /**
   * The webgl monitor instance.
   * @type {!JaSMIn.Monitor}
   */
  this.monitor = new JaSMIn.Monitor(this.domElement);

  /**
   * The mouse and keyboard input controller.
   * @type {!JaSMIn.UI.InputController}
   */
  this.inputController = new JaSMIn.UI.InputController(this.monitor);

  this.domElement.appendChild(this.inputController.domElement);

  /**
   * The player instance.
   * @type {?JaSMIn.Player}
   */
  this.player = null;

  /**
   * The replay loader.
   * @type {?JaSMIn.ReplayLoader}
   */
  this.replayLoader = new JaSMIn.ReplayLoader();

  /**
   * The replay loader.
   * @type {!JaSMIn.WorldLoader}
   */
  this.worldLoader = new JaSMIn.WorldLoader();

  /**
   * The top loading bar.
   * @type {!JaSMIn.UI.LoadingBar}
   */
  this.loadingBar = new JaSMIn.UI.LoadingBar();
  this.domElement.appendChild(this.loadingBar.domElement);

  /**
   * The error overlay.
   * @type {!JaSMIn.UI.ErrorOverlay}
   */
  this.errorOverlay = new JaSMIn.UI.ErrorOverlay();
  this.errorOverlay.setVisible(false);
  this.domElement.appendChild(this.errorOverlay.domElement);

  /**
   * The overlay manager.
   * @type {!JaSMIn.UI.OverlayManager}
   */
  this.overlayManager = new JaSMIn.UI.OverlayManager(this.monitor, this.domElement);

  /**
   * The player bar.
   * @type {?JaSMIn.UI.PlayerBar}
   */
  this.playerBar = null;



  /** @type {!Function} */
  this.onStartLoadingListener = this.onStartLoading.bind(this);
  /** @type {!Function} */
  this.onProgressListener = this.onProgress.bind(this);
  /** @type {!Function} */
  this.onNewReplayListener = this.onNewReplay.bind(this);
  /** @type {!Function} */
  this.onEndLoadingListener = this.onEndLoading.bind(this);

  // Add event listereners
  this.replayLoader.addEventListener(JaSMIn.ReplayLoaderEvents.START, this.onStartLoadingListener);
  this.replayLoader.addEventListener(JaSMIn.ReplayLoaderEvents.NEW_REPLAY, this.onNewReplayListener);
  this.replayLoader.addEventListener(JaSMIn.ReplayLoaderEvents.PROGRESS, this.onProgressListener);
  this.replayLoader.addEventListener(JaSMIn.ReplayLoaderEvents.FINISHED, this.onEndLoadingListener);
  this.replayLoader.addEventListener(JaSMIn.ReplayLoaderEvents.ERROR, this.onEndLoadingListener);



  /** @type {!Function} */
  this.onFullscreenChangeListener = this.onFullscreenChange.bind(this);


  // Add fullscreen change listeners
  document.addEventListener('fullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('mozfullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('msfullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('webkitfullscreenchange', this.onFullscreenChangeListener);



  /** @type {!Function} */
  this.onResizeListener = this.onResize.bind(this);


  // Add window resize listener
  window.addEventListener('resize', this.onResizeListener);
};



/**
 * Load and replay a replay file.
 *
 * @param {!string} url the replay file url
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.loadReplay = function(url) {
  // TODO: Enable/Disable controls
  this.errorOverlay.setVisible(false);

  this.replayLoader.load(url);
};



/**
 * Set a new player instance.
 *
 * @param {!JaSMIn.Player} player the new player instance
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.setPlayer = function(player) {
  this.player = player;
  this.inputController.setPlayer(player);

  if (this.playerBar !== null) {
    this.playerBar.setPlayer(player);
  } else if (player !== null) {
    this.playerBar = new JaSMIn.UI.PlayerBar(player, this.overlayManager);
    this.domElement.appendChild(this.playerBar.domElement);
    this.inputController.camCon.setPredefinedPose();
  }

  // Request focus for monitor input pane if the new player instance is valid
  if (player !== null) {
    this.inputController.domElement.focus();
  }
};



/**
 * Connect to the given streaming server and play the replay stream.
 *
 * @param {!string} url the replay streaming server url
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.connectStream = function(url) {
  // Not implemented yet
  throw 'Not implemented yet!';
};



/**
 * Connect to a simulation server.
 *
 * @param {!string} url the simulation server web-socket url.
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.connectSimulator = function(url) {
  // Not implemented yet
  throw 'Not implemented yet!';
};



/**
 * Handle a load/connection error.
 *
 * @param {!string} message the error message
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.handleError = function(message) {
  this.errorOverlay.setErrorMessage(message);
};



/**
 * Handle resizing of window.
 *
 * @param {!Object=} evt the resize event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onResize = function(evt) {
  this.monitor.autoResize();
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onStartLoading = function(event) {
  this.loadingBar.setVisible(true);
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onNewReplay = function(event) {
  // Create a new player instance
  this.setPlayer(new JaSMIn.Player(this.monitor, event.replay, this.worldLoader));
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onProgress = function(event) {
  this.loadingBar.setProgress(100 * event.loaded / event.total,
                              event.loaded / 1000000,
                              event.total / 1000000);
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onEndLoading = function(event) {
  this.loadingBar.setVisible(false);

  if (event.type === JaSMIn.ReplayLoaderEvents.ERROR) {
    this.handleError(event.msg);
  }
};



/**
 * The callback triggered when the window enters / leaves fullscreen.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.MonitorUI.prototype.onFullscreenChange = function(event) {
  var element = JaSMIn.UI.getFullscreenElement();

  if (element === this.domElement) {
    if (this.playerBar !== null) {
      this.playerBar.onFullscreenChange(true);
    }
  } else {
    if (this.playerBar !== null) {
      this.playerBar.onFullscreenChange(false);
    }
  }
};
