/**
 * The DynamicRobotModel class definition.
 *
 * The DynamicRobotModel provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.DynamicRobotModel');



/**
 * DynamicRobotModel Constructor
 *
 * @constructor
 * @extends {JaSMIn.RobotModel}
 * @param {!string} name the name of the agent model
 * @param {!JaSMIn.RobotSpecification=} specification the dynamic model specification
 */
JaSMIn.DynamicRobotModel = function(name, specification) {
  goog.base(this, name);

  if (specification !== undefined) {
    this.createModel(specification);
  }
};
goog.inherits(JaSMIn.DynamicRobotModel, JaSMIn.RobotModel);



/**
 * Create a robot model to the given name.
 *
 * @param  {!JaSMIn.RobotSpecification} spec the robot specification
 * @return {void}
 */
JaSMIn.DynamicRobotModel.prototype.createModel = function(spec) {
  var i = 0;

  // Create root body
  var rootBody = new THREE.Object3D();
  rootBody.name = spec.name;
  this.objGroup.add(rootBody);

  if (spec.meshes.length > 0) {
    // Create placeholder
    var placeholder = JaSMIn.SceneUtils.createDummyMesh();
    rootBody.add(placeholder);

    var onLoaded = function() {
      var body = rootBody;
      var ph = placeholder;

      return function(mesh) {
        body.remove(ph);
        body.add(mesh);
      };
    }();

    // Create meshes
    i = spec.meshes.length;
    while (i--) {
      spec.meshFactory.createMesh(spec.meshes[i].name, spec.meshes[i].material, spec.meshes[i].matrix, onLoaded);
    }
  }

  // Create child body parts
  for (i = 0; i < spec.children.length; ++i) {
    rootBody.add(this.createBodyParts(spec.meshFactory, spec.children[i]));
  }

  // Extract team materials
  i = spec.teamMaterialNames.length;
  while (i--) {
    var mat = spec.meshFactory.materialCache[spec.teamMaterialNames[i]];
    if (mat !== undefined) {
      this.teamMatList.push(mat);
    }
  }
};



/**
 * Create a body part hierarchy according to the given specification.
 *
 * @param  {!JaSMIn.MeshFactory} meshFactory the mesh factory
 * @param  {!JaSMIn.BodyPartSpecification} specification the body part specification
 * @return {!THREE.Object3D} an object representing this body part
 */
JaSMIn.DynamicRobotModel.prototype.createBodyParts = function(meshFactory, specification) {
  var i = 0;
  var bodyGroup = new THREE.Object3D();
  bodyGroup.name = specification.name;
  this.jointGroups.push(bodyGroup);

  // Set body part data
  bodyGroup.position.copy(specification.translation);
  bodyGroup.jointAxis = specification.jointAxis;

  if (specification.meshes.length > 0) {
    // Create placeholder
    var placeholder = JaSMIn.SceneUtils.createDummyMesh();
    bodyGroup.add(placeholder);

    var onLoaded = function() {
      var body = bodyGroup;
      var ph = placeholder;

      return function(mesh) {
        body.remove(ph);
        body.add(mesh);
      };
    }();

    // Create meshes
    i = specification.meshes.length;
    while (i--) {
      meshFactory.createMesh(specification.meshes[i].name, specification.meshes[i].material, specification.meshes[i].matrix, onLoaded);
    }
  }

  // Create child body parts
  for (i = 0; i < specification.children.length; ++i) {
    bodyGroup.add(this.createBodyParts(meshFactory, specification.children[i]));
  }

  return bodyGroup;
};
