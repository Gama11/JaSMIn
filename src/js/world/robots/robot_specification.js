/**
 * The RobotSpecification interface definition.
 *
 * The RobotSpecification describes the API of an agent bundle.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.BodyPartSpecification');
goog.provide('JaSMIn.MeshSpecification');
goog.provide('JaSMIn.RobotSpecification');

goog.require('JaSMIn.MeshFactory');



/**
 * MeshSpecification Constructor
 *
 * @constructor
 * @struct
 * @param {!string} name the name of the mesh
 * @param {!string} material the name of the material
 * @param {!THREE.Matrix4=} matrix the mesh transformation matrix
 */
JaSMIn.MeshSpecification = function(name, material, matrix) {
  /**
   * The name of the mesh.
   * @type {!string}
   */
  this.name = name;

  /**
   * The name of the material.
   * @type {!string}
   */
  this.material = material;

  /**
   * The name of the material.
   * @type {!THREE.Matrix4}
   */
  this.matrix = matrix !== undefined ? matrix : new THREE.Matrix4();
};



/**
 * BodyPartSpecification Constructor
 *
 * @constructor
 * @struct
 * @param {!string} name the name of the body part
 * @param {!Array<!JaSMIn.MeshSpecification>} meshes the list of mesh specifications representing this body part
 * @param {!THREE.Vector3} translation the translation from the parent body part to this body part
 * @param {!THREE.Vector3} jointAxis the rotation axis of the joint attached to this body part
 * @param {!Array<!JaSMIn.BodyPartSpecification>} children the child body parts
 */
JaSMIn.BodyPartSpecification = function(name, meshes, translation, jointAxis, children) {
  /**
   * The name of the body part.
   * @type {!string}
   */
  this.name = name;

  /**
   * The Array of mesh specifications representing this body part.
   * @type {!Array<!JaSMIn.MeshSpecification>}
   */
  this.meshes = meshes;

  /**
   * The translation from the parent body part to this body part.
   * @type {!THREE.Vector3}
   */
  this.translation = translation;

  /**
   * The Array of body part object names.
   * @type {!THREE.Vector3}
   */
  this.jointAxis = jointAxis;

  /**
   * The Array of child body parts.
   * @type {!Array<!JaSMIn.BodyPartSpecification>}
   */
  this.children = children;
};



/**
 * RobotSpecification Constructor
 *
 * @constructor
 * @param {!string} name the name of the root body part
 * @param {!JaSMIn.MeshFactory} meshFactory the mesh factory for this robot model
 * @param {!Array<!string>=} teamMaterialNames the names of the team materials
 * @param {!Array<!JaSMIn.MeshSpecification>=} meshes the list of mesh specifications representing this body part
 * @param {!Array<!JaSMIn.BodyPartSpecification>=} children the child body parts
 */
JaSMIn.RobotSpecification = function(name, meshFactory, teamMaterialNames, meshes, children) {
  /**
   * The name of the root body part.
   * @type {!string}
   */
  this.name = name;

  /**
   * The factory for creating the meshes of this robot model.
   * @type {!JaSMIn.MeshFactory}
   */
  this.meshFactory = meshFactory;

  /**
   * The names of the team materials.
   * @type {!Array<!string>}
   */
  this.teamMaterialNames = teamMaterialNames !== undefined ? teamMaterialNames : [];

  /**
   * The Array of mesh specifications representing the root body part.
   * @type {!Array<!JaSMIn.MeshSpecification>}
   */
  this.meshes = meshes !== undefined ? meshes : [];

  /**
   * The Array of child body parts.
   * @type {!Array<!JaSMIn.BodyPartSpecification>}
   */
  this.children = children !== undefined ? children : [];
};
