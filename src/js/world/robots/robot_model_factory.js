goog.provide('JaSMIn.RobotModelFactory');

goog.require('JaSMIn.NaoSpecification');
goog.require('JaSMIn.RobotModel');
goog.require('JaSMIn.RobotSpecification');
goog.require('JaSMIn.SoccerBot2DSpecification');



/**
 * RobotModelFactory Constructor
 *
 * @constructor
 */
JaSMIn.RobotModelFactory = function() {

};


/**
 * Create a robot model to the given name.
 *
 * @param  {!number} type the world (replay) type (2D or 3D)
 * @param  {!string} name the name of the robot model
 * @param  {!number} playerNo the player number
 * @return {!JaSMIn.RobotModel} a new robot model
 */
JaSMIn.RobotModelFactory.prototype.createModel = function(type, name, playerNo) {
  if (type === JaSMIn.ReplayTypes.TWOD) {
    return new JaSMIn.SoccerBot2D(playerNo);
  } else {
    if (name.startsWith('nao')) {
      // Check for the nao hetero model type
      /** @type {!number} */
      var modelType = 0;

      try {
        modelType = parseInt(name.slice(-1), 10);
      } catch (err) {
      }

      return new JaSMIn.Nao(modelType, playerNo);
    }
  }

  // Unknown robot model -> create a dummy model for representing the unknown model
  // TODO: return new JaSMIn.DummyBotSpecification();
  return new JaSMIn.Nao(0, playerNo);
};
