/**
 * The SoccerBot2DSpecification interface definition.
 *
 * The SoccerBot2DSpecification describes the API of an agent bundle.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SoccerBot2DSpecification');

goog.require('JaSMIn.BodyPartSpecification');
goog.require('JaSMIn.GeometryFactory');
goog.require('JaSMIn.MaterialFactory');
goog.require('JaSMIn.MeshSpecification');
goog.require('JaSMIn.RobotSpecification');
goog.require('JaSMIn.SceneUtils');





/**
 * SoccerBot2DSpecification Constructor
 *
 * @constructor
 * @extends {JaSMIn.RobotSpecification}
 */
JaSMIn.SoccerBot2DSpecification = function() {
  goog.base(this, 'torso',
      new JaSMIn.MeshFactory(
          JaSMIn.SoccerBot2DSpecification.GeometryFactory,
          new JaSMIn.SoccerBot2DMaterialFactory()),
      ['team']);

  var matStamina = 'stamina';
  var matBlack = 'sbBlack';
  var matBlue = 'sbBlue';
  var matTeam = 'team';

  var m4Body = JaSMIn.mM4(1, 0, 0, 0, 0, 1, 0, 0.3, 0, 0, 1, 0);
  var m4Team = JaSMIn.mM4(1, 0, 0, 0, 0, 0.2, 0, 0.6, 0, 0, 1, 0);
  var m4Stamina = JaSMIn.mM4(-1, 0, 0, 0, 0, 0.2, 0, 0.6, 0, 0, -1, 0);
  var m4Nose = JaSMIn.mM4(1, 0, 0, 0.25, 0, 1, 0, 0, 0, 0, 1, 0);

  /**
   * The list of stamina material names.
   * @type {!Array<!string>}
   */
  this.staminaMaterialNames = [matStamina];

  // torso meshes
  this.meshes.push(new JaSMIn.MeshSpecification('bodyCylinder', matBlack, m4Body));
  this.meshes.push(new JaSMIn.MeshSpecification('bodyTeamSphere', matTeam, m4Team));
  this.meshes.push(new JaSMIn.MeshSpecification('bodyStaminaSphere', matStamina, m4Stamina));

  // Head
  this.children.push(
      new JaSMIn.BodyPartSpecification('head',
        [ // head meshes
          new JaSMIn.MeshSpecification('headCylinder', matBlue),
          new JaSMIn.MeshSpecification('headNoseBox', matBlue, m4Nose)
        ],
        new THREE.Vector3(0, 0.651, 0),
        JaSMIn.Vector3_unitY,
        [])); // No further children here
};
goog.inherits(JaSMIn.SoccerBot2DSpecification, JaSMIn.RobotSpecification);









/**
 * The material factory for the soccer bot 2D robot models.
 *
 * @constructor
 * @implements {JaSMIn.MaterialFactory}
 */
JaSMIn.SoccerBot2DMaterialFactory = function() {

};

/**
 * Create the material with the given name.
 *
 * @param  {!string} name the unique name of the material
 * @return {(!THREE.Material | !THREE.MultiMaterial)} the requested (multi-)material
 *                             or a default material if the requested material definition was not found
 */
JaSMIn.SoccerBot2DMaterialFactory.prototype.createMaterial = function(name) {
  switch (name) {
    case 'sbBlue':
      return JaSMIn.SoccerBot2DMaterialFactory.BLUE_MATERIAL;
      break;
    case 'sbBlack':
      return JaSMIn.SoccerBot2DMaterialFactory.BLACK_MATERIAL;
    default:
      // By default create a very dark grey material
      return JaSMIn.SceneUtils.createStdPhongMat(name, 0x111111);
      break;
  }
};

/**
 * Constant nao white material.
 * @type {!THREE.Material}
 */
JaSMIn.SoccerBot2DMaterialFactory.BLACK_MATERIAL = JaSMIn.SceneUtils.createStdPhongMat('sbBlack', 0x000000);

/**
 * Constant nao black material.
 * @type {!THREE.Material}
 */
JaSMIn.SoccerBot2DMaterialFactory.BLUE_MATERIAL = JaSMIn.SceneUtils.createStdPhongMat('sbBlue', 0x001166);




/**
 * The geometry factory for the soccer bot 2D robot models.
 *
 * @constructor
 * @implements {JaSMIn.GeometryFactory}
 */
JaSMIn.SoccerBot2DGeometryFactory = function() {

};

/**
 * Create the geometry with the given name.
 *
 * @param  {!string} name the unique name of the geometry
 * @param  {!Function} onLoad the callback function to call on successfull creation
 * @param  {!Function=} onError the callback function to call when creating the geometry failed
 * @return {void}
 */
JaSMIn.SoccerBot2DGeometryFactory.prototype.createGeometry = function(name, onLoad, onError) {
    switch (name) {
    case 'bodyCylinderGeo':
      onLoad(new THREE.CylinderBufferGeometry(0.5, 0.5, 0.6, 32));
      break;
    case 'bodyTeamSphereGeo':
      onLoad(new THREE.SphereBufferGeometry(0.44, 16, 4, Math.PI / 2, Math.PI, 0, Math.PI / 2));
      break;
    case 'bodyStaminaSphereGeo':
      onLoad(new THREE.SphereBufferGeometry(0.44, 16, 4, Math.PI / 2, Math.PI, 0, Math.PI / 2));
      break;
    case 'headCylinderGeo':
      onLoad(new THREE.CylinderBufferGeometry(0.1, 0.1, 0.1, 16));
      break;
    case 'headNoseBoxGeo':
      onLoad(new THREE.BoxBufferGeometry(0.5, 0.1, 0.1));
      break;
    default:
      // Log error
      console.log('Geometry "' + name + '" not found!');

      if (onError) {
        onError('Geometry "' + name + '" not found!');
      }
      break;
    }
};







/**
 * The static geometry factory instance.
 *
 * @type {!JaSMIn.SoccerBot2DGeometryFactory}
 */
JaSMIn.SoccerBot2DSpecification.GeometryFactory = new JaSMIn.SoccerBot2DGeometryFactory();
