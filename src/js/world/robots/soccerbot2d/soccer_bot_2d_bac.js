/**
 * The SoccerBot2D class definition.
 *
 * The SoccerBot2D provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SoccerBot2D');

goog.require('JaSMIn');
goog.require('JaSMIn.SceneUtils');





/**
 * SoccerBot2D Constructor
 *
 * @constructor
 * @extends {JaSMIn.RobotModel}
 * @param {!number} playerNo the player number
 * @param {!number} teamSide the team side
 * @param {!number} maxStamina the maximum possible stamina
 * @param {!number} teamColor the team color
 */
JaSMIn.SoccerBot2D = function(playerNo, teamSide, maxStamina, teamColor) {
  goog.base(this, 'SoccerBot2D');

  var sideCode = JaSMIn.getSideLetter(teamSide, true);

  /**
   * The maximum stamina value
   * @type {!number}
   */
  this.maxStamina = maxStamina;

  /**
   * The agent stamina material
   * @type {!THREE.Material}
   */
  this.staminaMat = JaSMIn.SceneUtils.createStdPhongMat('staminaMat' + sideCode + playerNo, 0x111111);

  this.teamMat = JaSMIn.SceneUtils.createStdPhongMat('teamMat', teamColor);
  this.agentMat = JaSMIn.SceneUtils.createStdPhongMat('agentMat' + sideCode + playerNo, ((0xffffff & teamColor) ^ 0xffffff));


  // Create body parts
  var body = this.createBody(playerNo);
  var head = this.createHead();

  // Construct Robot
  body.add(head);

  // Setup agent instance
  this.objGroup.add(body);
  this.objGroup.scale.setScalar(2);

  // Construct joint object list
  this.jointGroups = [head];
};
goog.inherits(JaSMIn.SoccerBot2D, JaSMIn.RobotModel);



/**
 * Create a main body part.
 *
 * @param  {!number} playerNo the player number
 * @return {!THREE.Object3D}
 */
JaSMIn.SoccerBot2D.prototype.createBody = function(playerNo) {
  var body = new THREE.Object3D();
  body.name = 'body';
  body.position.y = 0.175;

  var geometry = JaSMIn.SoccerBot2D.getGeometryFor(JaSMIn.SoccerBot2D.GeometryIndices.BODY_CORE);
  var material = JaSMIn.SceneUtils.createStdPhongMat('bodyMat', 0x000000);
  var bodyCylinder = new THREE.Mesh(geometry, material);
  bodyCylinder.name = 'bodyCylinder';
  bodyCylinder.castShadow = true;
  body.add(bodyCylinder);

  geometry = JaSMIn.SoccerBot2D.getGeometryFor(JaSMIn.SoccerBot2D.GeometryIndices.BODY_CAP);
  material = playerNo === 1 ? this.agentMat : this.teamMat;
  var backSphere = new THREE.Mesh(geometry, material);
  backSphere.name = 'bodyTeamSphere';
  backSphere.position.y = 0.175;
  backSphere.scale.y = 0.20;
  body.add(backSphere);

  var frontSphere = new THREE.Mesh(geometry, this.staminaMat);
  frontSphere.name = 'bodyStaminaSphere';
  frontSphere.position.y = 0.175;
  frontSphere.scale.y = 0.20;
  frontSphere.rotation.y = Math.PI;
  body.add(frontSphere);

  return body;
};



/**
 * Create a head body part.
 *
 * @return {!THREE.Object3D} the new head object group
 */
JaSMIn.SoccerBot2D.prototype.createHead = function() {
  var head = new THREE.Object3D();
  head.jointAxis = JaSMIn.Vector3_unitY;
  head.name = 'head';
  head.position.y = 0.201;

  var geometry = JaSMIn.SoccerBot2D.getGeometryFor(JaSMIn.SoccerBot2D.GeometryIndices.HEAD);
  var material = JaSMIn.SceneUtils.createStdPhongMat('headMat', 0x001166);
  var headCylinder = new THREE.Mesh(geometry, material);
  headCylinder.name = 'headCylinder';
  head.add(headCylinder);

  geometry = JaSMIn.SoccerBot2D.getGeometryFor(JaSMIn.SoccerBot2D.GeometryIndices.NOSE);
  var nose = new THREE.Mesh(geometry, material);
  nose.name = 'headNoseBox';
  nose.position.x = 0.125;
  head.add(nose);

  return head;
};



/**
 * Update the joint objects according to the given angles.
 *
 * @param  {!Array<number>} data the agent data of the current state
 * @param  {Array<number>=} nextData the agent data of the next state
 * @param  {!number=} t the interpolation time
 * @return {void}
 */
JaSMIn.SoccerBot2D.prototype.updateData = function(data, nextData, t) {
  /* Data Array for SoccerBot2D:
   * idx | description
   *   0 | stamina (optional)
   */

  if (data[0] === undefined) {
    return;
  }

  var stamina = nextData === undefined ? data[0] : data[0] + (nextData[0] - data[0]) * t;

  // Apply stamina color
  stamina = THREE.Math.clamp(stamina, 0, this.maxStamina);
  this.staminaMat.color.setScalar((this.maxStamina - stamina) / this.maxStamina);
  this.staminaMat.needsUpdate = true;
};










/**
 * Enum for holding the different geometry indexes.
 * @enum {!number}
 */
JaSMIn.SoccerBot2D.GeometryIndices = {
  BODY_CORE: 0,
  BODY_CAP: 1,
  HEAD: 2,
  NOSE: 3
};



/**
 * The geometry cache.
 * @type {!Array<(!THREE.BufferGeometry | undefined)>}
 */
JaSMIn.SoccerBot2D.GeometryCache = [];



/**
 * Fetch the geometry for the given body part.
 *
 * @param  {!number} bodyPart the body part index (see JaSMIn.SoccerBot2D.GeometryIndices)
 * @return {(!THREE.BufferGeometry | undefined)} the geometry for the given index, or undefined if no such body part exists
 */
JaSMIn.SoccerBot2D.getGeometryFor = function(bodyPart) {
  var geometry = JaSMIn.SoccerBot2D.GeometryCache[bodyPart];

  if (geometry === undefined) {
    switch (bodyPart) {
    case JaSMIn.SoccerBot2D.GeometryIndices.BODY_CORE:
      geometry = new THREE.CylinderBufferGeometry(0.25, 0.25, 0.35, 32);
      break;
    case JaSMIn.SoccerBot2D.GeometryIndices.BODY_CAP:
      geometry = new THREE.SphereBufferGeometry(0.22, 16, 4, Math.PI / 2, Math.PI, 0, Math.PI / 2);
      break;
    case JaSMIn.SoccerBot2D.GeometryIndices.HEAD:
      geometry = new THREE.CylinderBufferGeometry(0.05, 0.05, 0.05, 16);
      break;
    case JaSMIn.SoccerBot2D.GeometryIndices.NOSE:
      geometry = new THREE.BoxBufferGeometry(0.25, 0.05, 0.05);
      break;
    }

    if (geometry !== undefined) {
      JaSMIn.SoccerBot2D.GeometryCache[bodyPart] = geometry;
    }
  }

  return geometry;
};
