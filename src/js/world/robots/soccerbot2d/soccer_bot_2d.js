/**
 * The SoccerBot2D class definition.
 *
 * The SoccerBot2D provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SoccerBot2D');

goog.require('JaSMIn');
goog.require('JaSMIn.DynamicRobotModel');
goog.require('JaSMIn.SoccerBot2DSpecification');





/**
 * SoccerBot2D Constructor
 *
 * @constructor
 * @extends {JaSMIn.DynamicRobotModel}
 * @param {!number} playerNo the player number
 */
JaSMIn.SoccerBot2D = function(playerNo) {
  goog.base(this, 'SoccerBot2D');

  /**
   * The list of stamina materials.
   * @type {!Array<!THREE.Material>}
   */
  this.staminaMatList = [];

  var spec = new JaSMIn.SoccerBot2DSpecification();

  // Create model
  this.createModel(spec);

  // Extract stamina materials
  var i = spec.staminaMaterialNames.length;
  while (i--) {
    var mat = spec.meshFactory.materialCache[spec.staminaMaterialNames[i]];
    if (mat !== undefined) {
      this.staminaMatList.push(mat);
    }
  }
};
goog.inherits(JaSMIn.SoccerBot2D, JaSMIn.DynamicRobotModel);



/**
 * Update the joint objects according to the given angles.
 *
 * @param  {!Array<number>} data the agent data of the current state
 * @param  {Array<number>=} nextData the agent data of the next state
 * @param  {!number=} t the interpolation time
 * @return {void}
 */
JaSMIn.SoccerBot2D.prototype.updateData = function(data, nextData, t) {
  /* Data Array for SoccerBot2D:
   * idx | description
   *   0 | stamina (optional)
   */

  if (data[0] === undefined) {
    return;
  }

  var stamina = nextData === undefined ? data[0] : data[0] + (nextData[0] - data[0]) * t;
  stamina = THREE.Math.clamp(stamina, 0, JaSMIn.SoccerBot2D.MaxStamina);
  stamina = (JaSMIn.SoccerBot2D.MaxStamina - stamina) / JaSMIn.SoccerBot2D.MaxStamina;

  // Apply stamina color
  var i = this.staminaMatList.length;
  while (i--) {
    var mat = this.staminaMatList[i];
    if (mat.color.r === stamina) {
      // Prevent material updates if stamina value hasn't changed (e.g. on pausing)
      break;
    }
    mat.color.setScalar(stamina);
    mat.needsUpdate = true;
  }
};



/**
 * The maximum stamina of a soccer bot in 2D.
 * @const {!number}
 */
JaSMIn.SoccerBot2D.MaxStamina = 8000;
