/**
 * The NaoSpecification interface definition.
 *
 * The NaoSpecification describes the API of an agent bundle.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.NaoSpecification');

goog.require('JaSMIn');
goog.require('JaSMIn.BodyPartSpecification');
goog.require('JaSMIn.JSONGeometryFactory');
goog.require('JaSMIn.MeshSpecification');
goog.require('JaSMIn.RobotSpecification');
goog.require('JaSMIn.SceneUtils');





/**
 * NaoSpecification Constructor
 *
 * @constructor
 * @extends {JaSMIn.RobotSpecification}
 * @param {!number} type the nao hetero model type
 * @param {!number} playerNo the player number
 */
JaSMIn.NaoSpecification = function(type, playerNo) {
  goog.base(this, 'torso',
      new JaSMIn.MeshFactory(
          JaSMIn.NaoSpecification.GeometryFactory,
          new JaSMIn.NaoMaterialFactory()),
      [JaSMIn.NaoSpecification.NAO_TEAM]);

  var matNum = 'num' + playerNo;
  var matWhite = JaSMIn.NaoSpecification.NAO_WHITE;
  var matBlack = JaSMIn.NaoSpecification.NAO_BLACK;
  var matGrey = JaSMIn.NaoSpecification.NAO_GREY;
  var matTeam = JaSMIn.NaoSpecification.NAO_TEAM;

  var m4Torso = JaSMIn.mM4(0, 0, 0.1, 0, 0, 0.1, 0, 0, -0.1, 0, 0, 0);
  var m4lUpperArm = JaSMIn.mM4(0, 0.07, 0, 0.02, 0, 0, 0.07, 0, 0.07, 0, 0, -0.01);
  var m4rUpperArm = JaSMIn.mM4(0, 0.07, 0, 0.02, 0, 0, 0.07, 0, 0.07, 0, 0, 0.01);
  var m4LowerArm = JaSMIn.mM4(0, 0.05, 0, 0.05, 0, 0, 0.05, 0, 0.05, 0, 0, 0);
  var m4Thigh = JaSMIn.mM4(0, 0.07, 0, 0.01, 0, 0, 0.07, -0.04, 0.07, 0, 0, 0);
  var m4Shank = JaSMIn.mM4(0, 0.08, 0, -0.005, 0, 0, 0.08, -0.055, 0.08, 0, 0, 0);
  var m4Foot = JaSMIn.mM4(0, 0.08, 0, 0.03, 0, 0, 0.08, -0.04, 0.08, 0, 0, 0);

  // torso meshes
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_coreBody', matWhite, m4Torso));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_coreInner', matBlack, m4Torso));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_chestButton', matTeam, m4Torso));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_chestBow', matTeam, m4Torso));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_numberBatch', matNum, m4Torso));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_lCollar', matWhite, JaSMIn.mM4(0, 0, -0.1, 0, 0, 0.1, 0, 0, 0.1, 0, 0, 0)));
  this.meshes.push(new JaSMIn.MeshSpecification('nao_torso_rCollar', matWhite, m4Torso));

  // Head
  this.children.push(
      new JaSMIn.BodyPartSpecification('neck',
        [], // neck meshes
        new THREE.Vector3(0, 0.09, 0),
        JaSMIn.Vector3_unitY,
        [ // neck children
          new JaSMIn.BodyPartSpecification('head',
            [ // head meshes
              new JaSMIn.MeshSpecification('nao_head_core', matWhite, m4Torso),
              new JaSMIn.MeshSpecification('nao_head_ears', matGrey, m4Torso),
              new JaSMIn.MeshSpecification('nao_head_teamMarker', matTeam, m4Torso),
              new JaSMIn.MeshSpecification('nao_head_camera', matBlack, m4Torso)
            ],
            new THREE.Vector3(0, 0.06, 0),
            JaSMIn.Vector3_unitZ,
            []) // No further children here
        ]));

  // left arm
  this.children.push(
      new JaSMIn.BodyPartSpecification('lShoulder',
        [], // lShoulder meshes
        new THREE.Vector3(0, 0.075, -0.098),
        JaSMIn.Vector3_unitZ,
        [ // lShoulder children
          new JaSMIn.BodyPartSpecification('lUpperArm',
            [ // lUpperArm meshes
              new JaSMIn.MeshSpecification('nao_lUpperArm_cylinder', matBlack, m4lUpperArm),
              new JaSMIn.MeshSpecification('nao_lUpperArm_protector', matWhite, m4lUpperArm),
              new JaSMIn.MeshSpecification('nao_lUpperArm_teamMarker', matTeam, m4lUpperArm)
            ],
            JaSMIn.Vector3_zero,
            JaSMIn.Vector3_unitY,
            [ // lUpperArm children
              new JaSMIn.BodyPartSpecification('lElbow',
                [], // lElbow meshes
                new THREE.Vector3(0.09, 0.009, 0),
                JaSMIn.Vector3_unitX,
                [ // lElbow children
                  new JaSMIn.BodyPartSpecification('lLowerArm',
                    [ // lLowerArm meshes
                      new JaSMIn.MeshSpecification('nao_lLowerArm_core', matWhite, m4LowerArm),
                      new JaSMIn.MeshSpecification('nao_lLowerArm_teamMarker', matTeam, m4LowerArm)
                    ],
                    JaSMIn.Vector3_zero,
                    JaSMIn.Vector3_unitY,
                    []) // No further children here
                ])
            ])
        ]));

  // Left leg
  this.children.push(
      new JaSMIn.BodyPartSpecification('lHip1',
        [], // lHip1 meshes
        new THREE.Vector3(-0.01, -0.115, -0.055),
        new THREE.Vector3(0, -0.7071, -0.7071),
        [ // lHip1 children
          new JaSMIn.BodyPartSpecification('lHip2',
            [], // lHip2 meshes
            JaSMIn.Vector3_zero,
            JaSMIn.Vector3_unitX,
            [ // lHip2 children
              new JaSMIn.BodyPartSpecification('lThigh',
                [ // lThigh meshes
                  new JaSMIn.MeshSpecification('nao_lThigh_core', matWhite, m4Thigh),
                  new JaSMIn.MeshSpecification('nao_lThigh_teamMarker', matTeam, m4Thigh)
                ],
                JaSMIn.Vector3_zero,
                JaSMIn.Vector3_unitZ,
                [ // lThigh children
                  new JaSMIn.BodyPartSpecification('lShank',
                    [ // lShank meshes
                      new JaSMIn.MeshSpecification('nao_lShank_coreInner', matBlack, m4Shank),
                      new JaSMIn.MeshSpecification('nao_lShank_coreBody', matWhite, m4Shank),
                      new JaSMIn.MeshSpecification('nao_lShank_teamMarker', matTeam, m4Shank)
                    ],
                    new THREE.Vector3(0.005, -0.12, 0),
                    JaSMIn.Vector3_unitZ,
                    [ // lShank children
                      new JaSMIn.BodyPartSpecification('lAnkle',
                        [], // lAnkle meshes
                        new THREE.Vector3(0, -0.1, 0),
                        JaSMIn.Vector3_unitZ,
                        [ // lAnkle children
                          new JaSMIn.BodyPartSpecification('lFoot',
                            [ // lFoot meshes
                              new JaSMIn.MeshSpecification('nao_lFoot_core', matWhite, m4Foot),
                              new JaSMIn.MeshSpecification('nao_lFoot_teamMarker', matTeam, m4Foot)
                            ],
                            JaSMIn.Vector3_zero,
                            JaSMIn.Vector3_unitX,
                            []) // No further children here
                        ])
                    ])
                ])
            ])
        ]));

  // Right arm
  this.children.push(
      new JaSMIn.BodyPartSpecification('rShoulder',
        [], // rShoulder meshes
        new THREE.Vector3(0, 0.075, 0.098),
        JaSMIn.Vector3_unitZ,
        [ // rShoulder children
          new JaSMIn.BodyPartSpecification('rUpperArm',
            [ // rUpperArm meshes
              new JaSMIn.MeshSpecification('nao_rUpperArm_cylinder', matBlack, m4rUpperArm),
              new JaSMIn.MeshSpecification('nao_rUpperArm_protector', matWhite, m4rUpperArm),
              new JaSMIn.MeshSpecification('nao_rUpperArm_teamMarker', matTeam, m4rUpperArm)
            ],
            JaSMIn.Vector3_zero,
            JaSMIn.Vector3_unitY,
            [ // rUpperArm children
              new JaSMIn.BodyPartSpecification('rElbow',
                [], // rElbow meshes
                new THREE.Vector3(0.09, 0.009, 0),
                JaSMIn.Vector3_unitX,
                [ // rElbow children
                  new JaSMIn.BodyPartSpecification('rLowerArm',
                    [ // rLowerArm meshes
                      new JaSMIn.MeshSpecification('nao_rLowerArm_core', matWhite, m4LowerArm),
                      new JaSMIn.MeshSpecification('nao_rLowerArm_teamMarker', matTeam, m4LowerArm)
                    ],
                    JaSMIn.Vector3_zero,
                    JaSMIn.Vector3_unitY,
                    []) // No further children here
                ])
            ])
        ]));

  // Right leg
  this.children.push(
      new JaSMIn.BodyPartSpecification('rHip1',
        [], // rHip1 meshes
        new THREE.Vector3(-0.01, -0.115, 0.055),
        new THREE.Vector3(0, 0.7071, -0.7071),
        [ // rHip1 children
          new JaSMIn.BodyPartSpecification('rHip2',
            [], // rHip2 meshes
            JaSMIn.Vector3_zero,
            JaSMIn.Vector3_unitX,
            [ // rHip2 children
              new JaSMIn.BodyPartSpecification('rThigh',
                [ // rThigh meshes
                  new JaSMIn.MeshSpecification('nao_rThigh_core', matWhite, m4Thigh),
                  new JaSMIn.MeshSpecification('nao_rThigh_teamMarker', matTeam, m4Thigh),
                  new JaSMIn.MeshSpecification('nao_rThigh_noMarker', matNum, m4Thigh)
                ],
                JaSMIn.Vector3_zero,
                JaSMIn.Vector3_unitZ,
                [ // rThigh children
                  new JaSMIn.BodyPartSpecification('rShank',
                    [ // rShank meshes
                      new JaSMIn.MeshSpecification('nao_rShank_coreInner', matBlack, m4Shank),
                      new JaSMIn.MeshSpecification('nao_rShank_coreBody', matWhite, m4Shank),
                      new JaSMIn.MeshSpecification('nao_rShank_teamMarker', matTeam, m4Shank)
                    ],
                    new THREE.Vector3(0.005, -0.12, 0),
                    JaSMIn.Vector3_unitZ,
                    [ // rShank children
                      new JaSMIn.BodyPartSpecification('rAnkle',
                        [], // rAnkle meshes
                        new THREE.Vector3(0, -0.1, 0),
                        JaSMIn.Vector3_unitZ,
                        [ // rAnkle children
                          new JaSMIn.BodyPartSpecification('rFoot',
                            [ // rFoot meshes
                              new JaSMIn.MeshSpecification('nao_rFoot_core', matWhite, m4Foot),
                              new JaSMIn.MeshSpecification('nao_rFoot_teamMarker', matTeam, m4Foot)
                            ],
                            JaSMIn.Vector3_zero,
                            JaSMIn.Vector3_unitX,
                            []) // No further children here
                        ])
                    ])
                ])
            ])
        ]));

  /**
   * The nao hetero model type.
   * @type {!number}
   */
  this.type = type !== undefined ? type : 0;

  // Apply type specific modifications
  switch (this.type) {
    case 4:
      this.applyType4Modifications();
      break;
    case 3:
      this.applyType3Modifications();
      break;
    case 1:
      this.applyType1Modifications();
      break;
    default:
      break;
  }
};
goog.inherits(JaSMIn.NaoSpecification, JaSMIn.RobotSpecification);



/**
 * Change the default model to a type 1 model.
 *
 * @return {void}
 */
JaSMIn.NaoSpecification.prototype.applyType1Modifications = function() {
  var lElbow = this.children[1].children[0].children[0];
  var rElbow = this.children[3].children[0].children[0];

  lElbow.translation.x = 0.12664;
  rElbow.translation.x = 0.12664;

  var lShank = this.children[2].children[0].children[0].children[0];
  var rShank = this.children[4].children[0].children[0].children[0];

  lShank.translation.y = -0.13832;
  rShank.translation.y = -0.13832;

  var lAnkle = lShank.children[0];
  var rAnkle = rShank.children[0];

  lAnkle.translation.y = -0.11832;
  rAnkle.translation.y = -0.11832;
};



/**
 * Change the default model to a type 3 model.
 *
 * @return {void}
 */
JaSMIn.NaoSpecification.prototype.applyType3Modifications = function() {
  var lElbow = this.children[1].children[0].children[0];
  var rElbow = this.children[3].children[0].children[0];

  lElbow.translation.x = 0.145736848;
  rElbow.translation.x = 0.145736848;

  var lHip1 = this.children[2];
  var rHip1 = this.children[4];

  lHip1.translation.z = -0.072954143;
  rHip1.translation.z = 0.072954143;

  var lShank = rHip1.children[0].children[0].children[0];
  var rShank = lHip1.children[0].children[0].children[0];

  lShank.translation.y = -0.147868424;
  rShank.translation.y = -0.147868424;

  var lAnkle = lShank.children[0];
  var rAnkle = rShank.children[0];

  lAnkle.translation.y = -0.127868424;
  rAnkle.translation.y = -0.127868424;
};



/**
 * Change the default model to a type 4 model.
 *
 * @return {void}
 */
JaSMIn.NaoSpecification.prototype.applyType4Modifications = function() {
  var lFoot = this.children[2].children[0].children[0].children[0].children[0].children[0];
  var rFoot = this.children[4].children[0].children[0].children[0].children[0].children[0];

  lFoot.children.push(
    new JaSMIn.BodyPartSpecification('lToe',
      [ // lToe meshes
      ],
      new THREE.Vector3(0.06, -0.04, 0),
      JaSMIn.Vector3_unitZ,
      []) // No further children here);
    );

  rFoot.children.push(
    new JaSMIn.BodyPartSpecification('rToe',
      [ // lToe meshes
      ],
      new THREE.Vector3(0.06, -0.04, 0),
      JaSMIn.Vector3_unitZ,
      []) // No further children here);
    );
};















/**
 * Constant for the name of the nao-white material.
 * @const {!string}
 */
JaSMIn.NaoSpecification.NAO_WHITE = 'naoWhite';

/**
 * Constant for the name of the nao-white material.
 * @const {!string}
 */
JaSMIn.NaoSpecification.NAO_BLACK = 'naoBlack';

/**
 * Constant for the name of the nao-white material.
 * @const {!string}
 */
JaSMIn.NaoSpecification.NAO_GREY = 'naoGrey';

/**
 * Constant for the name of the nao-team material.
 * @const {!string}
 */
JaSMIn.NaoSpecification.NAO_TEAM = 'team';









/**
 * The material factory for the nao robot models.
 *
 * @constructor
 * @implements {JaSMIn.MaterialFactory}
 */
JaSMIn.NaoMaterialFactory = function() {

};

/**
 * Create the material with the given name.
 *
 * @param  {!string} name the unique name of the material
 * @return {(!THREE.Material | !THREE.MultiMaterial)} the requested (multi-)material
 *                             or a default material if the requested material definition was not found
 */
JaSMIn.NaoMaterialFactory.prototype.createMaterial = function(name) {
  if (name.startsWith('num')) {
    var number = 0;
    try {
      number = parseInt(name.slice(3), 10);
    } catch (err) {
    }

    return JaSMIn.SceneUtils.createStdNumberMat(name, 0xcccccc, number);
  }

  switch (name) {
    case JaSMIn.NaoSpecification.NAO_BLACK:
      return JaSMIn.NaoMaterialFactory.NAO_BLACK_MATERIAL;
      break;
    case JaSMIn.NaoSpecification.NAO_GREY:
      return JaSMIn.NaoMaterialFactory.NAO_GREY_MATERIAL;
      break;
    case JaSMIn.NaoSpecification.NAO_WHITE:
      return JaSMIn.NaoMaterialFactory.NAO_WHITE_MATERIAL;
      break;
    default:
      // By default create a clone of nao white materail
      return JaSMIn.SceneUtils.createStdPhongMat(name, 0x3d3d3d);
      break;
  }
};

/**
 * Constant nao white material.
 * @type {!THREE.Material}
 */
JaSMIn.NaoMaterialFactory.NAO_WHITE_MATERIAL = JaSMIn.SceneUtils.createStdPhongMat('naoWhite', 0xcccccc);

/**
 * Constant nao black material.
 * @type {!THREE.Material}
 */
JaSMIn.NaoMaterialFactory.NAO_BLACK_MATERIAL = JaSMIn.SceneUtils.createStdPhongMat('naoBlack', 0x000000);

/**
 * Constant nao grey material.
 * @type {!THREE.Material}
 */
JaSMIn.NaoMaterialFactory.NAO_GREY_MATERIAL = JaSMIn.SceneUtils.createStdPhongMat('naoGrey', 0x3d3d3d);










/**
 * The geometry factory for the nao robot model
 * @type {!JaSMIn.JSONGeometryFactory}
 */
JaSMIn.NaoSpecification.GeometryFactory = new JaSMIn.JSONGeometryFactory('models/nao_resources_v4.json');
