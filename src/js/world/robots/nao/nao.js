/**
 * The Nao class definition.
 *
 * The Nao provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Nao');

goog.require('JaSMIn');
goog.require('JaSMIn.DynamicRobotModel');
goog.require('JaSMIn.NaoSpecification');





/**
 * Nao Constructor
 *
 * @constructor
 * @extends {JaSMIn.DynamicRobotModel}
 * @param {!number} type the nao hetero model type
 * @param {!number} playerNo the player number
 */
JaSMIn.Nao = function(type, playerNo) {
  goog.base(this, 'Nao' + type, new JaSMIn.NaoSpecification(type, playerNo));
};
goog.inherits(JaSMIn.Nao, JaSMIn.DynamicRobotModel);
