/**
 * The World class definition.
 *
 * The World provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.World');

goog.require('JaSMIn.Ball');
goog.require('JaSMIn.Field');
goog.require('JaSMIn.Team');
goog.require('JaSMIn.WorldState');



/**
 * World Constructor
 *
 * @constructor
 * @param {!JaSMIn.Field} field
 * @param {!JaSMIn.Ball} ball
 * @param {!JaSMIn.Team} leftTeam
 * @param {!JaSMIn.Team} rightTeam
 * @param {!number} bounds the field bounds
 */
JaSMIn.World = function(field, ball, leftTeam, rightTeam, bounds) {
  /**
   * The world scene
   * @type {!THREE.Scene}
   */
  this.scene = new THREE.Scene();
  this.scene.name = 'soccerScene';

  /**
   * The soccer field object
   * @type {!JaSMIn.Field}
   */
  this.field = field;

  this.scene.add(field.objGroup);

  /**
   * The soccer ball object
   * @type {!JaSMIn.Ball}
   */
  this.ball = ball;

  this.scene.add(ball.objGroup);
  this.scene.add(ball.objTwoDGroup);

  /**
   * The left team
   * @type {!JaSMIn.Team}
   */
  this.leftTeam = leftTeam;

  this.scene.add(leftTeam.objGroup);

  /**
   * The right team
   * @type {!JaSMIn.Team}
   */
  this.rightTeam = rightTeam;

  this.scene.add(rightTeam.objGroup);

  /**
   * The field bounds
   * @type {!THREE.Vector3}
   */
  this.boundingBox = new THREE.Vector3(bounds, bounds, bounds);
};



/**
 * Enable or disable shaddows.
 *
 * @param {!boolean} enabled true to enable shadows, false to disable
 */
JaSMIn.World.prototype.setShadowsEnabled = function(enabled) {
  var i = this.scene.children.length;

  while (i--) {
    var child = this.scene.children[i];

    if (child.type == 'DirectionalLight' ||
        child.type == 'PointLight' ||
        child.type == 'SpotLight') {
      child.castShadow = enabled;
    }
  }
};



/**
 * Update world objects.
 *
 * @param  {!JaSMIn.WorldState} state the current world state
 * @param  {!JaSMIn.WorldState} nextState the next world state
 * @param  {!number} t the interpolation time
 * @return {void}
 */
JaSMIn.World.prototype.update = function(state, nextState, t) {
  this.ball.update(state.ballState, nextState.ballState, t);

  this.leftTeam.update(state.leftAgentStates, nextState.leftAgentStates, t);
  this.rightTeam.update(state.rightAgentStates, nextState.rightAgentStates, t);
};
