/**
 * The Ball class definition.
 *
 * The Ball provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Ball');



/**
 * Ball Constructor
 *
 * @constructor
 * @extends {JaSMIn.MovableObject}
 * @param {number} radius the ball radius
 */
JaSMIn.Ball = function(radius) {
  goog.base(this, 'ball');

  this.radius = radius;
};
goog.inherits(JaSMIn.Ball, JaSMIn.MovableObject);



/**
 * Update movable object
 *
 * @param  {!JaSMIn.ObjectState} state the current object state
 * @param  {!JaSMIn.ObjectState=} nextState the next object state
 * @param  {!number=} t the interpolated time between the current and next state
 * @return {void}
 */
JaSMIn.Ball.prototype.update = function(state, nextState, t) {
  this.updateBodyPose(state, nextState, t);
};
