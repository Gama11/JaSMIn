/**
 * The Field class definition.
 *
 * The Field provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Field');



/**
 * Field Constructor
 *
 * @constructor
 * @param {!THREE.Vector2} fieldDimensions the dimensions of the soccer pitch
 * @param {!number} centerRadius the center circle raduis
 * @param {!THREE.Vector3} goalDimensions the dimensions of the goals
 * @param {!THREE.Vector2} goalAreaDimensions the dimensions of the goal areas
 * @param {?THREE.Vector3} penaltyAreaDimensions the dimensions of the penalty area + penalty kick spot
 */
JaSMIn.Field = function(fieldDimensions, centerRadius, goalDimensions, goalAreaDimensions, penaltyAreaDimensions) {
  /**
   * The field object group
   * @type {!THREE.Object3D}
   */
  this.objGroup = new THREE.Object3D();
  this.objGroup.name = 'field';

  /**
   * The dimensions of the field
   * @type {!THREE.Vector2}
   */
  this.fieldDimensions = fieldDimensions;

  /**
   * The radius of the center circle
   * @type {!number}
   */
  this.centerRadius = centerRadius;

  /**
   * The dimensions of the goals
   * @type {!THREE.Vector3}
   */
  this.goalDimensions = goalDimensions;

  /**
   * The dimensions of the goal area
   * @type {!THREE.Vector2}
   */
  this.goalAreaDimensions = goalAreaDimensions;

  /**
   * The dimensions of the penalty area + penalty kick spot
   * @type {?THREE.Vector3}
   */
  this.penaltyAreaDimensions = penaltyAreaDimensions;
};



/**
 * Check if this world parameters define a penalty area.
 *
 * @return {!boolean} true, if there exists a definition for the penalty area, false otherwise
 */
JaSMIn.Field.prototype.hasPenaltyArea = function() {
  return this.penaltyAreaDimensions !== null;
};
