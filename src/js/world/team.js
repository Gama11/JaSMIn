/**
 * The Team class definition.
 *
 * The Team provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Team');

goog.require('JaSMIn.Agent');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.TeamDescription');



/**
 * Team Constructor
 *
 * @constructor
 * @param {!JaSMIn.TeamDescription} description
 * @param {!Array<JaSMIn.Agent>=} agents
 */
JaSMIn.Team = function(description, agents) {
  /**
   * The team description
   * @type {!JaSMIn.TeamDescription}
   */
  this.description = description;

  /**
   * The team object group.
   * @type {!THREE.Object3D}
   */
  this.objGroup = new THREE.Object3D();
  this.objGroup.name = description.side === JaSMIn.TeamSide.LEFT ? 'leftTeam' : 'rightTeam';

  /**
   * The (dynamic) team color.
   * @type {!THREE.Color}
   */
  this.color = description.color;

  /**
   * The agents belonging to this team
   * @type {!Array<JaSMIn.Agent>}
   */
  this.agents = agents !== undefined ? agents : [];

  // Add all initial agents
  var i = this.agents.length;
  while (i--) {
    this.objGroup.add(this.agents[i].objGroup);
  }
};



/**
 * Update all agent objects of this team.
 *
 * @param  {!Array<!JaSMIn.AgentState | undefined>} states the current Agent states
 * @param  {!Array<!JaSMIn.AgentState | undefined>} nextStates the next Agent states
 * @param  {!number} t the interpolation time
 * @return {void}
 */
JaSMIn.Team.prototype.update = function(states, nextStates, t) {
  var i = this.agents.length;

  while (i--) {
    var no = this.agents[i].description.playerNo;

    this.agents[i].update(states[no], nextStates[no], t);
  }
};



/**
 * (Re)Set team color of all agents in this team.
 *
 * @param {!THREE.Color=} color the new team color (if undefined, the default team color from the description is used)
 */
JaSMIn.Team.prototype.setColor = function(color) {
  var newColor = color === undefined ? this.description.color : color;

  if (!this.color.equals(newColor)) {
    this.color = newColor;

    var i = this.agents.length;

    while (i--) {
      this.agents[i].setTeamColor(this.color);
    }
  }
};
