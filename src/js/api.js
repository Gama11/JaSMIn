goog.provide('JaSMIn.MonitorAPI');

goog.require('JaSMIn');




/**
 * External Monitor API.
 *
 * @export
 * @constructor
 * @param {Element=} container the parent element of the monitor
 */
JaSMIn.MonitorAPI = function(container) {
  if (container === undefined || container === null) {
    container = document.body;
  }

  /**
   * The monitor user interface.
   * @type {!JaSMIn.MonitorUI}
   */
  this.ui = new JaSMIn.MonitorUI(/** @type {!Element} */ (container));
};



/**
 * Load and replay a replay file.
 *
 * @export
 * @param {!string} url the replay file url
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.loadReplay = function(url) {
  this.ui.loadReplay(url);
};



/**
 * Connect to the given streaming server and play the replay stream.
 *
 * @export
 * @param {!string} url the replay streaming server url
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.connectStream = function(url) {
  // Not implemented yet
  throw 'Not implemented yet!';
};



/**
 * Connect to a simulation server.
 *
 * @export
 * @param {!string} url the simulation server web-socket url.
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.connectSimulator = function(url) {
  // Not implemented yet
  throw 'Not implemented yet!';
};



/**
 * Trigger play/pause command.
 *
 * @export
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.playPause = function() {
  if (this.ui.player !== null) {
    this.ui.player.playPause();
  }
};



/**
 * Trigger stop command.
 *
 * @export
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.stop = function() {

};



/**
 * Trigger step command.
 *
 * @export
 * @param {!boolean=} backwards fowrwards/backwards direction indicator (default: forward)
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.step = function(backwards) {
  if (this.ui.player !== null) {
    this.ui.player.step(backwards);
  }
};



/**
 * Trigger jump command.
 *
 * @export
 * @param {!number} stateIdx the state index to jump to. Negative values are interpreted as: (statesArray.length + stateIdx)
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.jump = function(stateIdx) {
  if (this.ui.player !== null) {
    this.ui.player.jump(stateIdx);
  }
};



/**
 * Trigger  command.
 *
 * @export
 * @param {!boolean=} previous next/previous indicator (default: next)
 * @return {void}
 */
JaSMIn.MonitorAPI.prototype.jumpGoal = function(previous) {
  if (this.ui.player !== null) {
    this.ui.player.jumpGoal(previous);
  }
};
