/**
 * The Player class definition.
 *
 * The Player is the central class representing the player logic, canvas handling, etc.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Player');

goog.require('JaSMIn');
goog.require('JaSMIn.Monitor');
goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.WorldLoader');



/**
 * The player states enum.
 * @enum {!number}
 */
JaSMIn.PlayerStates = {
  PAUSE: 0,
  PLAY: 1,
  WAITING: 2,
  END: 3
};



/**
 * @enum {!string}
 */
JaSMIn.PlayerEvents = {
  STATE_CHANGE: 'stateChange',
  TIME_PROGRESS: 'timeProgress',
  REPLAY_CHANGE: 'replayChange'
};



/**
 * Player Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 * @param {!JaSMIn.Monitor} monitor the monitor instance
 * @param {!JaSMIn.Replay} replay the replay to play
 * @param {!JaSMIn.WorldLoader} worldLoader the world loader
 */
JaSMIn.Player = function(monitor, replay, worldLoader) {

  /**
   * The monitor instance.
   * @type {!JaSMIn.Monitor}
   */
  this.monitor = monitor;

  /**
   * The replay to play.
   * @type {!JaSMIn.Replay}
   */
  this.replay = replay;

  /**
   * The world loader instance.
   * @type {!JaSMIn.WorldLoader}
   */
  this.worldLoader = worldLoader;

  /**
   * The player state.
   * @type {!JaSMIn.PlayerStates}
   */
  this.state = JaSMIn.PlayerStates.PAUSE;

  /**
   * The playback speed.
   * @type {!number}
   */
  this.playSpeed = 1;

  /**
   * The current play time.
   * @type {!number}
   */
  this.playTime = 0;

  /**
   * The index in the replay state array to the current play time.
   * @type {!number}
   */
  this.playIndex = 0;

  /**
   * Flag if the world scene should be updated although the player is currently not playing.
   * @type {!boolean}
   */
  this.needsUpdate = true;



  // Create new World representation
  var world = worldLoader.loadWorldFor(replay);

  // Update world representation when replay got extended
  /** @type {!Function} */
  this.onReplayChangeListener = this.onReplayChange.bind(this);
  replay.onChange = this.onReplayChangeListener;

  // Start rendering new world
  this.monitor.setWorld(world);

  // Hook into monitor render cycle
  /** @type {!Function} */
  this.onBeforeRenderListener = this.onBeforeRender.bind(this);
  this.monitor.onBeforeRenderCallback = this.onBeforeRenderListener;
};



/**
 * Dispose this player (removes listeners/callbacks from monitor).
 *
 * @return {void}
 */
JaSMIn.Player.prototype.dispose = function() {
  // Remove callback function
  this.monitor.onBeforeRenderCallback = undefined;
};



/**
 * Replay on change callback.
 *
 * @return {void}
 */
JaSMIn.Player.prototype.onReplayChange = function() {
  var world = this.monitor.world;

  this.worldLoader.loadTeam(this.replay.type, world.leftTeam);
  this.worldLoader.loadTeam(this.replay.type, world.rightTeam);

  // TODO: Think about a smart way to update team names

  this.dispatchEvent({
    type: JaSMIn.PlayerEvents.REPLAY_CHANGE
  });

  if (this.state === JaSMIn.PlayerStates.WAITING) {
    this.setState(JaSMIn.PlayerStates.PLAY);
  }
};



/**
 * Set the player state.
 *
 * @param {!JaSMIn.PlayerStates} newState the new player state
 */
JaSMIn.Player.prototype.setState = function(newState) {
  if (this.state !== newState) {
    // Every time we change the state, we should at least render once afterwards
    this.needsUpdate = true;

    var oldState = this.state;
    // console.log('Player state changed from ' + oldState + ' to ' + newState);

    this.state = newState;

    this.dispatchEvent({
      type: JaSMIn.PlayerEvents.STATE_CHANGE,
      oldState: oldState,
      newState: newState
    });
  }
};



/**
 * Set the play time of the player.
 *
 * @param {!number} newTime the new play time
 */
JaSMIn.Player.prototype.setPlayTime = function(newTime) {
  if (newTime < 0) {
    newTime = 0;
    this.setState(JaSMIn.PlayerStates.PAUSE);
  } else if (newTime > this.replay.duration) {
    newTime = this.replay.duration + 0.000005;

    this.setState(this.replay.fullyLoaded ? JaSMIn.PlayerStates.END : JaSMIn.PlayerStates.WAITING);
  } else if (this.state === JaSMIn.PlayerStates.END) {
    this.setState(JaSMIn.PlayerStates.PAUSE);
  } else if (this.state === JaSMIn.PlayerStates.WAITING) {
    this.setState(JaSMIn.PlayerStates.PLAY);
  }

  if (this.playTime === newTime) {
    return;
  }

  var oldTime = this.playTime;
  this.playTime = newTime;

  this.playIndex = this.replay.getIndexForTime(newTime);

  this.needsUpdate = true;

  this.dispatchEvent({
    type: JaSMIn.PlayerEvents.TIME_PROGRESS,
    oldTime: oldTime,
    newTime: newTime
  });
};



/**
 * Retrieve the world state to the current play time.
 *
 * @return {(!JaSMIn.WorldState | undefined)} the current world state
 */
JaSMIn.Player.prototype.getCurrentWorldState = function() {
  return this.replay.states[this.playIndex];
};



/**
 * Progress play time.
 *
 * @param  {!number} deltaT the time delta to add to the current time
 * @return {void}
 */
JaSMIn.Player.prototype.progressPlayTime = function(deltaT) {
  this.setPlayTime(this.playTime + deltaT * this.playSpeed);
};



/**
 * Callback function triggered by the monitor before it starts rendering the scene.
 *
 * @param  {!number} deltaT the time since the last render call
 * @return {void}
 */
JaSMIn.Player.prototype.onBeforeRender = function(deltaT) {
  // Progress play time if player is in playing state and the time since the last render call is below 0.5 seconds.
  if (this.state === JaSMIn.PlayerStates.PLAY && deltaT < 0.5) {
    this.progressPlayTime(deltaT);
  }

  if (this.state === JaSMIn.PlayerStates.PLAY || this.needsUpdate) {
    this.needsUpdate = false;

    // Update game info board
    this.monitor.gameInfoBoard.update(this.replay.states[this.playIndex]);

    // Update world
    var idx = this.playIndex;
    var t = 0;

    if (this.monitor.settings.getInterpolateStates()) {
      t = ((this.replay.startTime + this.playTime) - this.replay.states[idx].time) * this.replay.frequency;
    }

    if (idx + 1 >= this.replay.states.length) {
      // Final state
      --idx;
      t = 1;
    }

    this.monitor.world.update(this.replay.states[idx], this.replay.states[idx + 1], t);
  }
};



/**
 * The play/pause command.
 *
 * @return {void}
 */
JaSMIn.Player.prototype.playPause = function() {
  if (this.state === JaSMIn.PlayerStates.PLAY || this.state == JaSMIn.PlayerStates.WAITING) {
    this.setState(JaSMIn.PlayerStates.PAUSE);
  } else if (this.state === JaSMIn.PlayerStates.PAUSE) {
    this.setState(JaSMIn.PlayerStates.PLAY);
  } else if (this.state === JaSMIn.PlayerStates.END) {
    this.setPlayTime(0);
    this.setState(JaSMIn.PlayerStates.PLAY);
  }
};



/**
 * The step (forward/backward) command.
 *
 * @param {!boolean=} backwards
 * @return {void}
 */
JaSMIn.Player.prototype.step = function(backwards) {
  var amount = backwards ? -2 : 2;

  if (this.state === JaSMIn.PlayerStates.PAUSE || this.state === JaSMIn.PlayerStates.END) {
    amount = (backwards ? -1 : 1) / this.replay.frequency;
  }

  this.progressPlayTime(amount);
};



/**
 * The play/pause command.
 *
 * @param {!number} idx
 * @return {void}
 */
JaSMIn.Player.prototype.jump = function(idx) {
  if (idx < 0) {
    this.setPlayTime(0);
  } else if (idx >= this.replay.states.length) {
    this.setPlayTime(this.replay.duration + 1);
  } else {
    this.setPlayTime(this.replay.states[idx].time);
  }
};



/**
 * The play/pause command.
 *
 * @param {!boolean=} previous
 * @return {void}
 */
JaSMIn.Player.prototype.jumpGoal = function(previous) {
  var time = this.playTime;

  if (previous) {
    for (var i = this.replay.gameStateChanges.length - 1; i >= 0; --i) {
      var gameState = this.replay.gameStateChanges[i].newState;
      if (this.replay.gameStateChanges[i].time < time &&
          (gameState == 'goal_l' || gameState == 'goal_r' || gameState == 'GOAL_LEFT' || gameState == 'GOAL_RIGHT')) {
        this.setPlayTime(this.replay.gameStateChanges[i].time - 6);
        return;
      }
    }
  } else {
    time = time + 6;

    for (var i = 0; i < this.replay.gameStateChanges.length; ++i) {
      var gameState = this.replay.gameStateChanges[i].newState;
      if (this.replay.gameStateChanges[i].time > time &&
          (gameState == 'goal_l' || gameState == 'goal_r' || gameState == 'GOAL_LEFT' || gameState == 'GOAL_RIGHT')) {
        this.setPlayTime(this.replay.gameStateChanges[i].time - 6);
        return;
      }
    }
  }
};



// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.Player.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.Player.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.Player.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
