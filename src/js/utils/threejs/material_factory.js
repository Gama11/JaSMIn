goog.provide('JaSMIn.MaterialFactory');



/**
 * @interface
 */
JaSMIn.MaterialFactory = function() {

};



/**
 * Create the material with the given name.
 *
 * @param  {!string} name the unique name of the material
 * @return {(!THREE.Material | !THREE.MultiMaterial)} the requested (multi-)material
 *                             or a default material if the requested material definition was not found
 */
JaSMIn.MaterialFactory.prototype.createMaterial = function(name) {};
