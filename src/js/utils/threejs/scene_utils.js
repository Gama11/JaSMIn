goog.provide('JaSMIn.SceneUtils');



/**
 * General Scene utils.
 */
JaSMIn.SceneUtils = {};


/**
 * The threejs texture loader.
 * @type {?THREE.TextureLoader}
 */
JaSMIn.SceneUtils.TextureLoader = null;

/**
 * The texture path.
 * @const {!string}
 */
JaSMIn.SceneUtils.TexturePath = 'textures/';


/**
 * The threejs object loader.
 * @type {?THREE.ObjectLoader}
 */
JaSMIn.SceneUtils.ObjectLoader = null;

/**
 * The object/model path.
 * @const {!string}
 */
JaSMIn.SceneUtils.ModelPath = 'models/';



/**
 * Create a new texture from the given path.
 *
 * @param  {!string} path the texture path
 * @return {!THREE.Texture} a new texture object
 */
JaSMIn.SceneUtils.loadTexture = function(path) {
  if (JaSMIn.SceneUtils.TextureLoader === null) {
    JaSMIn.SceneUtils.TextureLoader = new THREE.TextureLoader();
  }

  return JaSMIn.SceneUtils.TextureLoader.load(JaSMIn.SceneUtils.TexturePath + path);
};



/**
 * Load an object from the given path.
 *
 * @param  {!string} path the object file path
 * @param  {!Function} onLoad the on load callback
 * @param  {!Function=} onProgress the on progress callback
 * @param  {!Function=} onError the on error callback
 * @return {void}
 */
JaSMIn.SceneUtils.loadObject = function(path, onLoad, onProgress, onError) {
  if (JaSMIn.SceneUtils.ObjectLoader === null) {
    JaSMIn.SceneUtils.ObjectLoader = new THREE.ObjectLoader();
  }

  JaSMIn.SceneUtils.ObjectLoader.load(JaSMIn.SceneUtils.ModelPath + path,
    onLoad,
    onProgress,
    function(xhr) {
      console.error('Error loading object "' + path + '": ' + xhr.statusText);

      if (onError !== undefined) {
        onError(xhr);
      }
    });
};



/**
 * Create a MeshPhongMaterial.
 * The texture argument can be a texture path or an actual texture object.
 * In case of a texture path, a new texture is loaded from the given path.
 * This material has by default:
 *   specular: 0x7f7f7f
 *   emissive: 0x000000
 *   shininess: 49
 *
 * @param {!string} name the name of the material
 * @param {!number} color the material color
 * @param {(!THREE.Texture | !string)=} texture the material texture
 * @return {!THREE.MeshPhongMaterial} the new material
 */
JaSMIn.SceneUtils.createStdPhongMat = function(name, color, texture) {
  var myTexture = null;

  if (texture !== undefined) {
    if (typeof texture === 'string') {
      // Load texture file
      myTexture = JaSMIn.SceneUtils.loadTexture(texture);
    } else {
      // Directly use given texture
      myTexture = texture;
    }
  }


  return new THREE.MeshPhongMaterial({
        name: name,
        color: color,
        specular: 0x7f7f7f,
        emissive: 0x000000,
        shininess: 49,
        map: myTexture
      });
};



/**
 * Create a new number material.
 *
 * @param  {!string} name the name of the material
 * @param  {!number} matColor the material color
 * @param  {!number} number the number to print on the texture
 * @param  {!number=} numColor the color of the number texture
 * @return {!THREE.Material} the number material
 */
JaSMIn.SceneUtils.createStdNumberMat = function(name, matColor, number, numColor) {
  if (numColor === undefined) {
    numColor = 0x000000;
  }

  // Create number texture
  // var texture = ;

  return JaSMIn.SceneUtils.createStdPhongMat(name, matColor);
};



/**
 * Offset the given material to avoid z-fighting.
 *
 * @param  {!THREE.Material} material the material to offset
 * @param  {!number=} factor the offset factor
 * @param  {!number=} units the offset units
 * @return {void}
 */
JaSMIn.SceneUtils.offsetMaterial = function(material, factor, units) {
  material.depthTest = true;
  material.polygonOffset = true;
  material.polygonOffsetFactor = factor || -1;
  material.polygonOffsetUnits = units || -0.1;
};



/**
 * Create a mesh with the given parameter.
 * By default, the mesh will cast and receive shadows.
 *
 * @param  {!string} name the name of the mesh
 * @param  {(!THREE.Geometry | !THREE.BufferGeometry)} geometry the mesh geometry
 * @param  {(!THREE.Material | !THREE.MultiMaterial)} material the mesh material
 * @param  {!boolean=} rotXNeg90 true if the mesh should be rotated around x about -90 degrees, false for no rotation
 * @return {!THREE.Mesh} a new mesh with the specified properties
 */
JaSMIn.SceneUtils.createMesh = function(name, geometry, material, rotXNeg90) {
  var mesh = new THREE.Mesh(geometry, material);
  mesh.name = name;
  mesh.receiveShadow = true;
  mesh.castShadow = true;

  if (rotXNeg90) {
    mesh.rotation.x = -Math.PI / 2;
  }

  return mesh;
};



/**
 * Create a mesh with the given parameter.
 *
 * @param  {!string} name the name of the mesh
 * @param  {(!THREE.Geometry | !THREE.BufferGeometry)} geometry the mesh geometry
 * @param  {(!THREE.Material | !THREE.MultiMaterial)} material the mesh material
 * @param  {!number} x the x-coordinate of the mesh
 * @param  {!number} y the y-coordinate of the mesh
 * @param  {!number} z the z-coordinate of the mesh
 * @param  {!boolean=} rotXNeg90 true if the mesh should be rotated around x about -90 degrees, false for no rotation
 * @return {!THREE.Mesh} a new mesh with the specified properties
 */
JaSMIn.SceneUtils.createMeshAt = function(name, geometry, material, x, y, z, rotXNeg90) {
  var mesh = JaSMIn.SceneUtils.createMesh(name, geometry, material, rotXNeg90);

  mesh.position.set(x, y, z);

  return mesh;
};



/**
 * Create a simple circle for representing a selected object.
 *
 * @param {!number} radius the circle radius
 * @param {!number} halfLineWidth the half circle line width
 * @return {!THREE.Mesh} the selection mesh
 */
JaSMIn.SceneUtils.createSelectionMesh = function(radius, halfLineWidth) {
  var mesh = new THREE.Mesh(new THREE.RingBufferGeometry(radius - halfLineWidth, radius + halfLineWidth, 16, 1), JaSMIn.SceneUtils.SelectionMaterial);
  mesh.name = 'selectionCircle';
  mesh.visible = false;
  mesh.receiveShadow = false;
  mesh.castShadow = false;
  mesh.rotation.x = -Math.PI / 2;

  return mesh;
};

/**
 * The selection material.
 * @type {!THREE.MeshPhongMaterial}
 */
JaSMIn.SceneUtils.SelectionMaterial = new THREE.MeshPhongMaterial({name: 'selectionMat', color: 0xeeeeee, side: THREE.DoubleSide});
JaSMIn.SceneUtils.offsetMaterial(JaSMIn.SceneUtils.SelectionMaterial, -1.5, -0.15);



/**
 * Create a dummy mesh used as placehoder for loading/failing body parts.
 * @return {!THREE.Mesh} a dummy mesh
 */
JaSMIn.SceneUtils.createDummyMesh = function() {
  var mesh = new THREE.Mesh(JaSMIn.SceneUtils.DummyGeometry, JaSMIn.SceneUtils.DummyMaterial);
  mesh.name = 'placeholder';
  mesh.receiveShadow = false;
  mesh.castShadow = false;

  return mesh;
};

/**
 * The Dummy geometry.
 * @type {!THREE.BoxBufferGeometry}
 */
JaSMIn.SceneUtils.DummyGeometry = new THREE.BoxBufferGeometry(0.1, 0.1, 0.1);

/**
 * The dummy material.
 * @type {!THREE.MeshPhongMaterial}
 */
JaSMIn.SceneUtils.DummyMaterial = new THREE.MeshPhongMaterial({name: 'dummyMat', color: 0x000000});



/**
 * Create a sky box of the given size.
 *
 * @param  {!number} size the size of the box
 * @return {!THREE.Mesh} the sky box mesh
 */
JaSMIn.SceneUtils.createSkyBox = function(size) {
  var texPosx = JaSMIn.SceneUtils.loadTexture('sky_posx.jpg');
  var texNegx = JaSMIn.SceneUtils.loadTexture('sky_negy.jpg');
  var texPosy = JaSMIn.SceneUtils.loadTexture('sky_posy.jpg');
  var texNegy = JaSMIn.SceneUtils.loadTexture('sky_negz.jpg');
  var texPosz = JaSMIn.SceneUtils.loadTexture('sky_posz.jpg');
  var texNegz = JaSMIn.SceneUtils.loadTexture('sky_negx.jpg');

  var materials = [
          new THREE.MeshBasicMaterial({ map: texPosx, side: THREE.BackSide }),
          new THREE.MeshBasicMaterial({ map: texNegx, side: THREE.BackSide }),
          new THREE.MeshBasicMaterial({ map: texPosy, side: THREE.BackSide }),
          new THREE.MeshBasicMaterial({ map: texNegy, side: THREE.BackSide }),
          new THREE.MeshBasicMaterial({ map: texPosz, side: THREE.BackSide }),
          new THREE.MeshBasicMaterial({ map: texNegz, side: THREE.BackSide }),
     ];

  var boxMaterial = new THREE.MultiMaterial(materials);
  var boxGeometry = new THREE.BoxBufferGeometry(size, size, size);

  var mesh = new THREE.Mesh(boxGeometry, boxMaterial);
  mesh.name = 'sky_box';

  return mesh;
};



/**
 * Create a simple, white, spherical ball mesh.
 *
 * @param  {!number} radius the ball radius
 * @return {!THREE.Mesh} the ball mesh
 */
JaSMIn.SceneUtils.createSimpleBall = function(radius) {
  var geometry = new THREE.SphereBufferGeometry(radius, 16, 16);
  geometry.name = 'ballGeo';

  var material = JaSMIn.SceneUtils.createStdPhongMat('ballMat', 0xffffff);

  return JaSMIn.SceneUtils.createMesh('ballSphere', geometry, material);
};



/**
 * Add a soccer field (grass) plane to the given object group.
 *
 * @param  {!THREE.Object3D} group the object group to add the field plane
 * @param  {!number} fieldLength the length of the field
 * @param  {!number} fieldWidth the width of the field
 * @param  {?number} textureRepeat the number of texture repeats
 * @return {void}
 */
JaSMIn.SceneUtils.addFieldPlane = function(group, fieldLength, fieldWidth, textureRepeat) {
  var mesh;

  /**
   * Helper method for adding meshes.
   * @param {!string} name
   * @param {!number} x
   * @param {!number} y
   * @param {!number} z
   * @return {void}
   */
  var addMesh = function(name, x, y, z) {
    mesh = new THREE.Mesh(geometry, material);
    mesh.name = name;
    mesh.position.set(x, y, z);
    mesh.rotation.x = -Math.PI / 2;
    mesh.receiveShadow = true;
    mesh.castShadow = false;

    group.add(mesh);
  };


  // Create field plane
  var halfLength = Math.floor((fieldLength + 1.99) / 2);
  var geometry = new THREE.PlaneBufferGeometry(halfLength * 2, fieldWidth);
  var texture = JaSMIn.SceneUtils.loadTexture('field6_1.png');
  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  if (textureRepeat !== null) {
    texture.repeat.set(textureRepeat, fieldWidth * textureRepeat / fieldLength);
  } else {
    texture.repeat.set(halfLength, fieldWidth);
  }
  var material = new THREE.MeshPhongMaterial({name: 'fieldMat', color: 0xcccccc, map: texture});
  addMesh('fieldPlane', 0, 0, 0);

  // Create field border
  halfLength = fieldLength / 2;
  var halfWidth = fieldWidth / 2;
  var borderSize = fieldLength / 12;
  geometry = new THREE.PlaneBufferGeometry(fieldLength + borderSize * 2, borderSize);
  texture = JaSMIn.SceneUtils.loadTexture('field_border.png');
  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  texture.repeat.set((fieldLength + borderSize * 2), borderSize);
  material = new THREE.MeshPhongMaterial({name: 'tbBorderMat', color: 0xaa99aa, map: texture});
  addMesh('topBorder', 0, 0, -halfWidth - borderSize / 2);
  addMesh('bottomBorder', 0, 0, halfWidth + borderSize / 2);

  texture = JaSMIn.SceneUtils.loadTexture('field_border.png');
  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  texture.repeat.set(borderSize, fieldWidth);
  material = new THREE.MeshPhongMaterial({name: 'lrBorderMat', color: 0xaa99aa, map: texture});
  JaSMIn.SceneUtils.offsetMaterial(material, -0.5, -0.05);
  geometry = new THREE.PlaneBufferGeometry(borderSize, fieldWidth);
  addMesh('leftBorder', -halfLength - borderSize / 2, 0, 0);
  addMesh('rightBorder', halfLength + borderSize / 2, 0, 0);
};



/**
 * Add standard field lines to the given object group.
 *
 * @param  {!THREE.Object3D} group the object group to add the field plane
 * @param  {!number} lineWidth the field line width
 * @param  {!THREE.Vector2} fieldDimensions the dimensions of the field
 * @param  {!number} centerRadius the radius of the center circle
 * @param  {!THREE.Vector2} goalAreaDimensions the dimensions of the goal area
 * @param  {?THREE.Vector3} penaltyAreaDimensions the dimensions of the penalty area + penalty kick spot
 * @return {void}
 */
JaSMIn.SceneUtils.addFieldLines = function(group,
                                           lineWidth,
                                           fieldDimensions,
                                           centerRadius,
                                           goalAreaDimensions,
                                           penaltyAreaDimensions) {
  var mesh;
  var tempX = 0;
  var lineMaterial = new THREE.MeshBasicMaterial({ color: 0xeeeeee, side: THREE.DoubleSide });
  JaSMIn.SceneUtils.offsetMaterial(lineMaterial, -1, -0.1);

  /**
   * Helper method for adding meshes.
   * @param {!string} name
   * @param {!number} x
   * @param {!number} y
   * @param {!number} z
   * @param {!number=} rotZ
   * @return {void}
   */
  var addMesh = function(name, x, y, z, rotZ) {
    mesh = new THREE.Mesh(geometry, lineMaterial);
    mesh.name = name;
    mesh.position.set(x, y, z);
    mesh.rotation.x = -Math.PI / 2;
    mesh.rotation.z = rotZ ? rotZ : 0;
    mesh.receiveShadow = true;
    mesh.castShadow = false;

    group.add(mesh);
  };

  var halfLength = fieldDimensions.x / 2;
  var halfWidth = fieldDimensions.y / 2;
  var halfLineWidth = lineWidth / 2;

  // Circle
  var radius = centerRadius;
  var geometry = new THREE.RingBufferGeometry(radius - halfLineWidth, radius + halfLineWidth, 64, 1);
  addMesh('centerCircle', 0, 0, 0);

  // General field lines (l, r, t, b, c)
  geometry = new THREE.PlaneBufferGeometry(fieldDimensions.x + lineWidth, lineWidth);
  addMesh('topBorderLine', 0, 0, -halfWidth);
  addMesh('btmBorderLine', 0, 0, halfWidth);

  geometry = new THREE.PlaneBufferGeometry(lineWidth, fieldDimensions.y);
  addMesh('leftBorderLine', -halfLength, 0, 0);
  addMesh('rightBorderLine', halfLength, 0, 0);

  addMesh('centerLine', 0, 0, 0);

  // Corner circles
  radius = fieldDimensions.x / 105.0;
  geometry = new THREE.RingBufferGeometry(radius - halfLineWidth, radius + halfLineWidth, 8, 1, 0, Math.PI / 2);
  addMesh('btmLeftCircle', -halfLength, 0, halfWidth);
  addMesh('btmRightCircle', halfLength, 0, halfWidth, Math.PI / 2);
  addMesh('topRightCircle', halfLength, 0, -halfWidth, Math.PI);
  addMesh('topLeftCircle', -halfLength, 0, -halfWidth, -Math.PI / 2);

  // Center spot
  geometry = new THREE.CircleBufferGeometry(lineWidth * 1.2, 16);
  addMesh('centerSpot', 0, 0, 0);

  // Penalty area
  if (penaltyAreaDimensions !== null) {
    // Penalty kick spots
    tempX = halfLength - penaltyAreaDimensions.z;
    addMesh('leftPenaltySpot', -tempX, 0, 0);
    addMesh('rightPenaltySpot', tempX, 0, 0);

    // Penalty area front lines
    var halfPenaltyWidth = penaltyAreaDimensions.y / 2;
    tempX = halfLength - penaltyAreaDimensions.x;
    geometry = new THREE.PlaneBufferGeometry(lineWidth, penaltyAreaDimensions.y + lineWidth);
    addMesh('leftPAFrontLine', -tempX, 0, 0);
    addMesh('rightPAFrontLine', tempX, 0, 0);

    // Penalty area top and bottom lines
    tempX = halfLength - penaltyAreaDimensions.x / 2;
    geometry = new THREE.PlaneBufferGeometry(penaltyAreaDimensions.x, lineWidth);
    addMesh('leftPATopLine', -tempX, 0, -halfPenaltyWidth);
    addMesh('leftPABtmLine', -tempX, 0, halfPenaltyWidth);

    addMesh('rightPABtmLine', tempX, 0, -halfPenaltyWidth);
    addMesh('rightPATopLine', tempX, 0, halfPenaltyWidth);

    // Penalty area arcs
    tempX = halfLength - penaltyAreaDimensions.z;
    var diffAngle = Math.acos((penaltyAreaDimensions.x - penaltyAreaDimensions.z) / centerRadius);
    geometry = new THREE.RingBufferGeometry(centerRadius - halfLineWidth, centerRadius + halfLineWidth, 32, 1, diffAngle, -2 * diffAngle);
    addMesh('leftPAArc', -tempX, 0, 0);
    addMesh('rightPAArc', tempX, 0, 0, Math.PI);
  }

  // Goal area
  var halfGoalAreaWidth = goalAreaDimensions.y / 2;
  tempX = halfLength - goalAreaDimensions.x;
  geometry = new THREE.PlaneBufferGeometry(lineWidth, goalAreaDimensions.y + lineWidth);
  addMesh('leftGAFrontLine', -tempX, 0, 0);
  addMesh('rightGAFrontLine', tempX, 0, 0);

  tempX = halfLength - goalAreaDimensions.x / 2;
  geometry = new THREE.PlaneBufferGeometry(goalAreaDimensions.x, lineWidth);
  addMesh('leftGATopLine', -tempX, 0, -halfGoalAreaWidth);
  addMesh('leftGABtmLine', -tempX, 0, halfGoalAreaWidth);

  addMesh('rightGATopLine', tempX, 0, -halfGoalAreaWidth);
  addMesh('rightGABtmLine', tempX, 0, halfGoalAreaWidth);
};



/**
 * Create a hockey (triangular) goal. The created goal is for the right side.
 *
 * @param  {!string} name the name of the goal object group
 * @param  {!number} postRadius the line width
 * @param  {!THREE.Vector3} dimensions the goal dimensions
 * @param  {!number} color the goal color
 * @return {!THREE.Object3D} the ball object
 */
JaSMIn.SceneUtils.createHockeyGoal = function(name, postRadius, dimensions, color) {
  var mesh;
  var shadows = true;
  var objGroup = new THREE.Object3D();
  objGroup.name = name;

  /**
   * Helper method for adding meshes.
   * @param {!string} name
   * @param {!THREE.Material} material
   * @param {!number} x
   * @param {!number} y
   * @param {!number} z
   * @param {!boolean=} keepRot
   * @return {!THREE.Mesh}
   */
  var addMesh = function(name, material, x, y, z, keepRot) {
    mesh = new THREE.Mesh(geometry, material);
    mesh.name = name;
    mesh.position.set(x, y, z);
    mesh.rotation.x = keepRot ? 0 : -Math.PI / 2;
    mesh.receiveShadow = shadows;
    mesh.castShadow = shadows;
    objGroup.add(mesh);

    return mesh;
  };


  var goalBarRadius = postRadius / 2;
  var halfGoalWidth = postRadius + dimensions.y / 2;
  var halfGoalHeight = (goalBarRadius + dimensions.z) / 2;

  var goalMat = JaSMIn.SceneUtils.createStdPhongMat('goalMat', color);
  goalMat.side = THREE.DoubleSide;

  var goalOffsetMat = goalMat.clone();
  JaSMIn.SceneUtils.offsetMaterial(goalOffsetMat, -1, -0.1);


  // Goal posts
  var geometry = new THREE.CylinderBufferGeometry(postRadius, postRadius, dimensions.z + goalBarRadius, 16);
  addMesh('leftPost', goalOffsetMat, postRadius, halfGoalHeight, halfGoalWidth, true);
  addMesh('rightPost', goalOffsetMat, postRadius, halfGoalHeight, -halfGoalWidth, true);


  // Upper goal bar
  geometry = new THREE.CylinderBufferGeometry(goalBarRadius, goalBarRadius, halfGoalWidth * 2, 8);
  addMesh('upperBar', goalMat, postRadius, dimensions.z, 0);


  // Goal bottom bar
  var angle = Math.atan(0.5 * dimensions.z / dimensions.x);
  var tempGeometry = new THREE.CylinderGeometry(goalBarRadius, goalBarRadius, halfGoalWidth * 2, 8, 1, false, -0.5 * Math.PI, angle);
  var mat = new THREE.Matrix4();
  mat.identity();
  mat.elements[12] = -goalBarRadius / 2;
  tempGeometry.merge(new THREE.PlaneGeometry(goalBarRadius, halfGoalWidth * 2), mat);
  mat.makeRotationY(angle);
  mat.elements[12] = -Math.cos(angle) * goalBarRadius / 2;
  mat.elements[14] = Math.sin(angle) * goalBarRadius / 2;
  tempGeometry.merge(new THREE.PlaneGeometry(goalBarRadius, halfGoalWidth * 2), mat);

  geometry = new THREE.BufferGeometry();
  geometry.fromGeometry(tempGeometry);
  addMesh('bottomBar', goalOffsetMat, dimensions.x, 0, 0);


  // Goal stand
  var triShape = new THREE.Shape();
  triShape.moveTo(0, 0);
  triShape.lineTo(dimensions.x, 0);
  triShape.lineTo(0, dimensions.z / 2);
  triShape.lineTo(0, 0);
  geometry = new THREE.BufferGeometry();
  geometry.fromGeometry(new THREE.ShapeGeometry(triShape));
  addMesh('leftStand', goalMat, 0, 0, halfGoalWidth, true);
  addMesh('rightStand', goalMat, 0, 0, -halfGoalWidth, true);


  // Goal net
  shadows = false;
  var netWidth = dimensions.y + postRadius * 2 - 0.02;
  var netDepth = dimensions.x - postRadius - 0.01;
  var netHeight = Math.sqrt(netDepth * netDepth + dimensions.z * dimensions.z);

  var netSideTexture = JaSMIn.SceneUtils.loadTexture('goalnet.png');
  netSideTexture.wrapS = THREE.RepeatWrapping;
  netSideTexture.wrapT = THREE.RepeatWrapping;
  netSideTexture.repeat.set(netDepth, dimensions.z);
  var goalNetMatSides = JaSMIn.SceneUtils.createStdPhongMat('netSideMat', 0xffffff, netSideTexture);
  goalNetMatSides.side = THREE.DoubleSide;
  goalNetMatSides.transparent = true;

  var netBackTexture = JaSMIn.SceneUtils.loadTexture('goalnet.png');
  netBackTexture.wrapS = THREE.RepeatWrapping;
  netBackTexture.wrapT = THREE.RepeatWrapping;
  netBackTexture.repeat.set(netWidth, netHeight);
  var goalNetMatBack = JaSMIn.SceneUtils.createStdPhongMat('netBackMat', 0xffffff, netBackTexture);
  goalNetMatBack.side = THREE.DoubleSide;
  goalNetMatBack.transparent = true;

  triShape = new THREE.Shape();
  triShape.moveTo(0, 0);
  triShape.lineTo(netDepth, 0);
  triShape.lineTo(0, dimensions.z);
  triShape.lineTo(0, 0);
  geometry = new THREE.BufferGeometry();
  geometry.fromGeometry(new THREE.ShapeGeometry(triShape));
  addMesh('leftNet', goalNetMatSides, postRadius, 0, netWidth / 2, true);
  addMesh('rightNet', goalNetMatSides, postRadius, 0, -netWidth / 2, true);

  geometry = new THREE.PlaneBufferGeometry(netWidth, netHeight);
  addMesh('backNet', goalNetMatBack, postRadius + netDepth / 2, dimensions.z / 2, 0, true);
  mesh.rotation.order = 'ZYX';
  mesh.rotation.y = -Math.PI / 2;
  mesh.rotation.x = (Math.PI / 2) - Math.atan(dimensions.z / netDepth);

  return objGroup;
};



/**
 * Create a hockey (triangular) goal. The created goal is for the right side.
 *
 * @param  {!THREE.Object3D} scene the scene/group to add lighting
 * @param  {!number} fieldLength the length of the field
 * @param  {!number} fieldWidth the width of the field
 * @return {void}
 */
JaSMIn.SceneUtils.addStdLighting = function(scene, fieldLength, fieldWidth) {
  // Ambient lighting
  scene.add(new THREE.AmbientLight(0xeeeeee));


  // sun lighting
  var vertical = Math.ceil(fieldWidth * 0.8);
  var horizontal = Math.ceil(fieldLength * 0.7);
  var depth = fieldWidth;

  var directionalLight = new THREE.DirectionalLight(0xeeeeee, 0.4);
  directionalLight.position.set(300, 300, 500);
  directionalLight.castShadow = true;
  directionalLight.shadow.mapSize.width = 2048;
  directionalLight.shadow.mapSize.height = 2048;

  directionalLight.shadow.camera.left = -horizontal;
  directionalLight.shadow.camera.right = horizontal;
  directionalLight.shadow.camera.top = vertical;
  directionalLight.shadow.camera.bottom = -vertical;
  directionalLight.shadow.camera.near = 655 - depth;
  directionalLight.shadow.camera.far = 655 + depth;

  scene.add(directionalLight);
};
