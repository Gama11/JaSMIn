/**
 * The ReplayDataIterator class definition.
 *
 * The ReplayDataIterator provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ReplayDataIterator');



/**
 * ReplayDataIterator Constructor
 * Create a new replay data interator.
 *
 * @constructor
 * @param {!string} data the replay data string
 * @param {!boolean=} partialData indicator if the given data is not completely loaded yet
 */
JaSMIn.ReplayDataIterator = function(data, partialData) {
  /**
   * The replay data.
   * @type {!string}
   */
  this.data = data;

  /**
   * The current position.
   * @type {!number}
   */
  this.pos = 0;

  /**
   * The next position.
   * @type {!number}
   */
  this.nextPos = -1;

  /**
   * The current line.
   * @type {Array<string>}
   */
  this.line = null;

  /**
   * The current line counter.
   * @type {!number}
   */
  this.lineIdx = 0;

  /**
   * Indicator if the iterator contains full replay data.
   * @type {!boolean}
   */
  this.fullyLoaded = partialData !== undefined ? !partialData : true;
};

/**
 * [reset description]
 * @return {void}
 */
JaSMIn.ReplayDataIterator.prototype.dispose = function() {
  this.data = '';
  this.pos = 0;
  this.nextPos = -1;
  this.line = null;
  this.lineIdx = 0;
  this.fullyLoaded = true;
};

/**
 * [hasNext description]
 * @return {!boolean} [description]
 */
JaSMIn.ReplayDataIterator.prototype.hasNext = function() {
  this.nextPos = this.data.indexOf('\n', this.pos);

  return this.nextPos !== -1;
};

/**
 * [next description]
 * @return {Array<string>} the current line array
 */
JaSMIn.ReplayDataIterator.prototype.next = function() {
  if (this.nextPos <= this.pos) {
    this.nextPos = this.data.indexOf('\n', this.pos);
  }

  if (this.nextPos > this.pos) {
    // Fetch next chunk
    this.line = this.data.slice(this.pos, this.nextPos).split(' ');
    this.pos = this.nextPos + 1;
    this.lineIdx++;
  } else {
    this.line = null;
  }

  return this.line;
};
