/**
 * The Persistance class definition.
 *
 * The Persistance provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Persistance');



/**
 * The Persistance
 */
JaSMIn.Persistance = {};



/**
 * Store the given value under the given key in the local storage.
 *
 * @param  {!string} key the storage item key
 * @param  {!string | !boolean} value the item value
 * @return {void}
 */
JaSMIn.Persistance.storeItem = function(key, value) {
  localStorage.setItem(key, value);
};



/**
 * Read an item from the local storage with the given key.
 *
 * @param  {!string} key the storage item key
 * @return {?string} the stored value for the given key, or null if no such value exists
 */
JaSMIn.Persistance.readItem = function(key) {
  return localStorage.getItem(key);
};

/**
 * Read a boolean from local storage.
 *
 * @param  {!string} key the key
 * @param  {!string} defaultVal the defaultVal value if no value for the given key was specified
 * @return {!string}
 */
JaSMIn.Persistance.readString = function(key, defaultVal) {
  var item = localStorage.getItem(key);

  if (item !== null) {
    return item;
  }

  return defaultVal;
};

/**
 * Read a boolean from local storage.
 *
 * @param  {!string} key the key
 * @param  {!boolean} defaultVal the defaultVal value if no value for the given key was specified
 * @return {!boolean}
 */
JaSMIn.Persistance.readBoolean = function(key, defaultVal) {
  var item = localStorage.getItem(key);

  if (item !== null) {
    return item == 'true';
  }

  return defaultVal;
};
