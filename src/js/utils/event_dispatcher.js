/**
 * The EventDispatcher class definition.
 *
 * The EventDispatcher is kind of copied from threejs and extended to fit into the google closure environment.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.EventDispatcher');
goog.provide('JaSMIn.IEventDispatcher');
goog.provide('JaSMIn.IPublisher');



/**
 * IPublisher Interface
 * @interface
 */
JaSMIn.IPublisher = function() {};

/**
 * Add callback function for the given event type.
 *
 * @param {!string} type the event type
 * @param {!Function} listener the callback function
 * @return {!boolean} true if listener was added, false otherwise
 */
JaSMIn.IPublisher.prototype.addEventListener = function(type, listener) {};

/**
 * Remove listener callback funtion from the given event type.
 *
 * @param  {!string} type the event name
 * @param  {!Function} listener the callback function
 * @return {!boolean}
 */
JaSMIn.IPublisher.prototype.removeEventListener = function(type, listener) {};






/**
 * IEventDispatcher Interface
 * @interface
 */
JaSMIn.IEventDispatcher = function() {};

/**
 * Call to dispatch an event to all registered listeners.
 *
 * @param  {!Object} event the event to dispatch
 * @return {void}
 */
JaSMIn.IEventDispatcher.prototype.dispatchEvent = function(event) {};






/**
 * [EventDispatcher description]
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.EventDispatcher = function() {

};

/** @override */
JaSMIn.EventDispatcher.prototype.addEventListener = function(type, listener) {
  // Lazy create listeners holder object
  if (this['__evt_obsrvs'] === undefined) {
    /** @type {!Object} the object holding all known listeners */
    this['__evt_obsrvs'] = {};
  }
  var listeners = this['__evt_obsrvs'];

  // Lazy create listener array for specific event
  if (listeners[type] === undefined) {
    listeners[type] = [];
  }

  // Add listener if not yet present
  if (listeners[type].indexOf(listener) === -1) {
    listeners[type].push(listener);
    return true;
  }

  return false;
};

/** @override */
JaSMIn.EventDispatcher.prototype.removeEventListener = function(type, listener) {
  var listeners = this['__evt_obsrvs'];

  if (listeners === undefined) {
    // No listeners defined, thus nothing to do
    return false;
  }

  var listenerArray = listeners[type];

  if (listenerArray !== undefined) {
    var index = listenerArray.indexOf(listener);

    if (index !== -1) {
      listenerArray.splice(index, 1);
      return true;
    }
  }

  return false;
};

/** @override */
JaSMIn.EventDispatcher.prototype.dispatchEvent = function(event) {
  var listeners = this['__evt_obsrvs'];

  if (listeners === undefined) {
    // No listeners defined, thus nothing to do
    return;
  }

  var listenerArray = listeners[event.type];

  if (listenerArray !== undefined) {
    var array = [], i = 0;
    var length = listenerArray.length;

    for (i = 0; i < length; i++) {
      array[i] = listenerArray[i];
    }

    for (i = 0; i < length; i++) {
      array[i].call(this, event);
    }
  }
};
