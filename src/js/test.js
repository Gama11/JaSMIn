goog.require('JaSMIn');
goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.MonitorAPI');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.ReplayLoader');
goog.require('JaSMIn.ReplayLoaderEvents');
goog.require('JaSMIn.ReplayParser');


var agent = new JaSMIn.AgentDescription(1, 1, 'testmodel');

/**
 * createAgent description
 * @export
 * @param {number} number the number
 * @param {string} model the model
 * @return {JaSMIn.AgentDescription} an agent Description
 */
JaSMIn.createAgent = function(number, model) {
  return new JaSMIn.AgentDescription(number, -1, model);
};

/**
 * printAgent description
 * @export
 * @param {JaSMIn.AgentDescription} agent
 * @return {void} an agent Description
 */
JaSMIn.printAgent = function(agent) {
  console.log('Agent-PlayerNo: ' + agent.playerNo);
  console.log('Agent-Left-Team: ' + agent.side);
  console.log('Agent-Models: ' + agent.models[0]);
};

/**
 * @export
 * @param {string} url
 * @return {void} an agent Description
 */
JaSMIn.loadReplay = function(url) {
  var loader = new JaSMIn.ReplayLoader();
  // var parser = new JaSMIn.ReplayParser();

  // var myReplay = parser.parseReplay('stuff');

  // console.log(myReplay.type);
  // console.log(myReplay.version);

  /**
   * [listenerFcn description]
   *
   * @param  {!Object} event the event object
   * @return {void}
   */
  var listenerFcn = function(event) {
    console.log('Event-Type: ' + event.type);
    console.log('Event-URL: ' + event.url);
    console.log('Event-Total: ' + event.total);
    console.log('Event-Loaded: ' + event.loaded);
    console.log('Event-Msg: ' + event.msg);
  };

  /**
   * [listenerFcn description]
   *
   * @param  {!Object} event the event object
   * @return {void}
   */
  var listenerProgress = function(event) {
    console.log('Event-Type: ' + event.type);
    console.log('Event-URL: ' + event.url);
    console.log('Event-Total: ' + event.total);
    console.log('Event-Loaded: ' + event.loaded);
    // console.log('Event-Text-Len: ' + event.xhr.target.responseText.length);
    // console.log('Event-Text-Len: ' + event.xhr.target.responseText.slice(0, 20));
    // console.log(event.xhr);
  };

  loader.addEventListener(JaSMIn.ReplayLoaderEvents.START, listenerFcn);
  loader.addEventListener(JaSMIn.ReplayLoaderEvents.FINISHED, listenerFcn);
  loader.addEventListener(JaSMIn.ReplayLoaderEvents.PROGRESS, listenerProgress);
  loader.addEventListener(JaSMIn.ReplayLoaderEvents.ERROR, listenerFcn);

  console.log(Date.now());
  loader.load(url,
    /**
     * [description]
     * @param  {JaSMIn.Replay} replay the loaded replay file
     * @return {void}
     */
    function(replay) {
      console.log('finished');
      console.log('Replay-Type: ' + replay.type);
      console.log('Replay-Version: ' + replay.version);
      console.log('Replay-States: ' + replay.states.length);

      replay.onChange = function() {
        console.log(Date.now());
      };
    },
    /**
     * [description]
     * @param  {string} str an error string
     * @return {void}
     */
    function(str) {
      console.log('Error: ' + str);
    }
  );

  // JaSMIn.LoadingManager.DefaultLoadingManager.loadXhr('testurl',
  //   function(data) {
  //     console.log(data);
  //   }
  // );
};

/**
 * createAgent description
 * @export
 * @param {Element=} container the container to add the monitor
 * @return {!JaSMIn.MonitorAPI} the new monitor api
 */
JaSMIn.createMonitor = function(container) {
  return new JaSMIn.MonitorAPI(container);
};
