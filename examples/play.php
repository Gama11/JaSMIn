<?php header('Content-Type: text/html');

define("LOG_DIR",	"replays");

?>
<!DOCTYPE html>
<html>
   <head>
      <title>SocSim Replayer - Watch RoboCup Soccer Simulation matches online</title>
	  <meta charset="utf-8">
	  <link rel="stylesheet" type="text/css" href="css/socmon.css" />
   </head>
   <body>
<?php
	$path = '';
	if (isset($_GET['path'])) {
		$extPath = $_GET['path'];
		if (strrpos($extPath, "..", -2) === FALSE && strrpos($extPath, "../") === false && file_exists(LOG_DIR . $extPath)) {
			$path = $extPath;
		}
	}
	
	// Check if the path points to an actual replay file
	if (substr($path, -7) == ".replay" || substr($path, -6) == ".rpl2d" || substr($path, -6) == ".rpl3d") {
		// Show player
		echo '   </body>';
		echo '<script src="js/three.min.js"></script>';
		echo '<script src="js/h5sm.min.js"></script>';
		echo '<script language="javascript">';
		echo 'SOCMON.runMonitor({"replay_url": "' . LOG_DIR . $path . '");';
		echo '</script>';
		
	} else {
		// Show folder listing
		$folders = array();
		$replays = array();
	
		$dirIter = new DirectoryIterator(LOG_DIR . $path);
		
		if ($dirIter->valid()) {
			$folderIdx = 0;
			$replayIdx = 0;
	
			foreach ($dirIter as $fn) {
				if ($fn->isDir() && !$fn->isDot()) {
					$folders[$folderIdx] = $fn->getFilename();
					$folderIdx++;
				} else if (!$fn->isDot()) {
					$fileSuffixL = substr($fn->getFilename(), -7);
					$fileSuffixS = substr($fn->getFilename(), -6);
					if ($fileSuffixL == ".replay" || $fileSuffixS == ".rpl2d" || $fileSuffixS == ".rpl3d") {
						$replays[$replayIdx] = $fn->getFilename();
						$replayIdx++;
					}
				}
			}
	
			sort($folders, SORT_STRING);
			sort($replays, SORT_STRING);
		}
		
		// Output listing
		echo '<ul>';
		
		if (count($path) > 0) {
			echo '<li class="parentLink">parent folder</li>';
		}
		
		foreach ($folders as $name) {
			echo '<li><a href="play.php?path=/' . $path . '/' . $name . '">' . $name . '</a></li>';
		}
		
		foreach ($replays as $name) {
			echo '<li><a href="play.php?path=/' . $path . '/' . $name . '">' . $name . '</a></li>';
		}
		
		echo '</ul>';
		echo '   </body>';
	}
?>
</html>
