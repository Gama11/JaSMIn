var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	gp = gulpLoadPlugins(),
  path = require('path'),
  compiler = require('google-closure-compiler-js').gulp();


var buildDir = path.join(__dirname, 'build');
var srcDir = path.join(__dirname, 'src');


gulp.task('build', ['build-js', 'build-sass', 'copy-textures', 'copy-models', 'copy-images', 'copy-examples']);


gulp.task('build-js', function () {
  return gulp.src(['node_modules/google-closure-library/closure/goog/base.js',
                   './src/js/externs.js',
                   // 'node_modules/three/build/three.js',
                   './src/js/threejs-externs.js',
                   './src/js/main.js',
                   './src/js/ui/ui.js',

                   './src/js/utils/replay_data_iterator.js',
                   './src/js/utils/event_dispatcher.js',
                   './src/js/utils/fps_meter.js',
                   './src/js/utils/persistance.js',
                   './src/js/utils/threejs/geometry_factory.js',
                   './src/js/utils/threejs/json_geometry_factory.js',
                   './src/js/utils/threejs/material_factory.js',
                   './src/js/utils/threejs/mesh_factory.js',
                   './src/js/utils/threejs/scene_utils.js',

                   './src/js/replay/agent_description.js',
                   './src/js/replay/team_description.js',
                   './src/js/replay/game_state_change.js',
                   './src/js/replay/object_state.js',
                   './src/js/replay/agent_state.js',
                   './src/js/replay/world_state.js',
                   './src/js/replay/partial_world_state.js',
                   './src/js/replay/replay.js',
                   './src/js/replay/replay_parser.js',
                   './src/js/world/movable_object.js',
                   './src/js/world/ball.js',
                   './src/js/world/robot_model.js',
                   './src/js/world/agent.js',
                   './src/js/world/team.js',
                   './src/js/world/field.js',
                   './src/js/world/world.js',

                   './src/js/world/robots/robot_specification.js',
                   './src/js/world/robots/dynamic_robot_model.js',
                   './src/js/world/robots/nao/nao_specification.js',
                   './src/js/world/robots/nao/nao.js',
                   './src/js/world/robots/soccerbot2d/soccer_bot_2d_specification.js',
                   './src/js/world/robots/soccerbot2d/soccer_bot_2d.js',
                   './src/js/world/robots/robot_model_factory.js',

                   './src/js/loader/replay_loader.js',
                   './src/js/loader/world_parameter.js',
                   './src/js/loader/world_loader.js',

                   './src/js/monitor/monitor_settings.js',
                   './src/js/monitor/camera_controller.js',

                   './src/js/ui/panel.js',
                   './src/js/ui/overlay/overlay.js',
                   './src/js/ui/single_choice_item.js',
                   './src/js/ui/toggle_item.js',
                   './src/js/ui/error_pane.js',
                   './src/js/ui/game_info_board.js',
                   './src/js/ui/monitor_statistics_board.js',
                   './src/js/ui/loading_bar.js',

                   './src/js/monitor/monitor.js',
                   './src/js/player/player.js',

                   './src/js/monitor/universal_camera_controller.js',
                   './src/js/ui/input_controller.js',

                   './src/js/ui/overlay/about_overlay.js',
                   './src/js/ui/overlay/settings_overlay.js',
                   './src/js/ui/overlay/info_overlay.js',
                   './src/js/ui/overlay/overlay_manager.js',

                   './src/js/ui/player_bar.js',
                   './src/js/ui/monitor_ui.js',

                   './src/js/api.js'], {base: './'})
    .pipe(gp.sourcemaps.init())
    .pipe(compiler({
        jsOutputFile: 'JaSMIn.min.js',  // filename returned to gulp
        compilationLevel: 'ADVANCED',   // as opposed to 'SIMPLE', the default
        //compilationLevel: 'SIMPLE',   // as opposed to 'ADVANCED'
        warningLevel: 'VERBOSE',        // complain loudly on errors
        createSourceMap: true,          // create output source map
        processCommonJsModules: true,   // needed to support require()
        generateExports: true,
      }))
    .pipe(gp.sourcemaps.write('/'))
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('build-sass', function () {
  return gulp.src(path.join(srcDir, 'scss/JaSMIn.scss'))
    .pipe(gp.sass({ style: 'expanded', sourceComments: 'map', errLogToConsole: true}))
    //.pipe(gp.autoprefixer('last 2 version', "> 1%", 'ie 8', 'ie 9'))
    .pipe(gulp.dest(path.join(buildDir, 'css')))
    .pipe(gp.notify({ message: 'LibSass files dropped!' }));
});

gulp.task('copy-textures', function () {
  return gulp.src(path.join(srcDir, 'textures/*'))
    .pipe(gulp.dest(path.join(buildDir, 'textures')));
});

gulp.task('copy-models', function () {
  return gulp.src(path.join(srcDir, 'blender/json/*'))
    .pipe(gulp.dest(path.join(buildDir, 'models')));
});

gulp.task('copy-images', function () {
  return gulp.src(path.join(srcDir, 'images/*'))
    .pipe(gulp.dest(path.join(buildDir, 'images')));
});

gulp.task('copy-examples', function () {
  return gulp.src('examples/**/*')
    .pipe(gulp.dest(buildDir));
});

gulp.task('clean', function() {
  return gulp.src('build/*', {read: false})
	 .pipe(gp.clean());
});




//  Default Gulp Task
//===========================================
gulp.task('default', ['build'], function() {

});
