# JaSMIn

Javascript Soccer Monitor Interface, a webgl based player for RoboCup Simulation replay files.

1. OVERVIEW
===================

JaSMIn is a webgl based player for displaying replay files from RoboCup soccer simulation leagues (2D and 3D) in a browser.
Inspired by the GIBBS player, the initial idea was to create a similar web player for the RoboCup 3D soccer simulation league, rendering a full 3D scene instead of a 2D top down view.
After some fun with threejs I decided to create a more general player for both, 2D and 3D soccer simulation leagues.

At the moment the player can play 2D and 3D replay files.
Such replay files are converted from the original server log files.
For 2D simulation log files you can use the converter shipped with GIBBS.
For 3D simulation log files you need to wait till I publish the converter project...


2. SETUP
===================

TODO


3. DEVELOPMENT
===================

TODO

